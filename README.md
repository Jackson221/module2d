# About

Module2D is an engine which was spawned in 2017 out of a desire to learn C++. Since then, it has ballooned into a multi-threaded, networking-capable optimized 2D game engine with support for a ridiculous number of concurrent physics objects without sacrificing simulation speed.

# Cloning

Module2D uses submodules. To clone, just use

`git clone --recurse-submodules https://gitlab.com/AI221/module2d`

Most likely you'll use it as a submodule in your game project directory.

# Building?

Module2D can't be built without a game. 

To create a Module2D game, first add Module2D as a git submodule, then put your source code somewhere. You will need to define a singleton that inherets from game_base (level2/engine.hpp) and assigns to `game`. Next, in your CMakeLists.txt, include Module2D's CMakeLists.txt in the second line, right after `cmake_minimum_required`. Then, include the Module2D library.
Now, build your project as you normally would.

## Windows builds

To generate windows builds, you'll need to install the following packages

`sudo apt install mingw-w64 mingw-w64-tools wine`

And then run 

`./_WININSTALLLIBS.sh`

# New style

Code in header files should *always* be in a namespace. Namespaces should provide context to names -- for instance, tripple_buffer::instance makes more sense with the namespace than without. Due to this, it is prefered not to use `using namespace` on internal modules.

Manual memory management should probably be avoided in favor of RAII alternatives. Additionally, inheritance and RTTI should be avoided, if possible, in favor of composition and templates.

Spacegame's new style is exactly the same as Module3D's - instead of camelCase, underscore_seperation is used. This is exactly the same as C++'s standard library's style.

Names should be clear and descriptive over short and concise. Often times the clearest names are short and concise. But in liue of understanding the problem space enough to craft such names, a tragically long descriptive name is preferable. Names should strive to explain not only what, but also *why*.


# Legacy Style

The legacy style is deprecated and is being slowly removed from the game engine. The styling difference originates because of the addition of Module3D modules, and the decision to refactor spacegame code to the new styling. A module being in legacy style strongly suggests that it is in general bad or needs updating.

New code written in legacy styled modules will still follow GE_ naming, but otherwise follows the new styling guides.

The syntax for functions is pretty much the same as SDL. GE_CreateStructName , GE_FreeStructName . 
new and delete are used. 

Callbacks are C_Name

Array counts are numArrayName even though C arrays shouldn't be used ever

.h / .cpp are used even though it forces your editor to guess the C/++.

typedef and defines are used even though using and constexpr constants are clearly better.

# Relationship to Module3D

Module3D is my up-and-coming 3D game engine. I still work on and use spacegame because it allows me to understand the bigger picture of engine design before I make decisions for Module3D. In essense, I prototype new features in spacegame and then, if they are good enough, refactor them into Module3D modules. Otherwise, I rewrite them. Some things are specific to 2D and would not work in 3D. In this case, the code is kept in spacegame and is different in Module3D

When I reimplement a spacegame feature in Module3D, I try to replace spacegame's version with Module3D's. However, spacegame is really quite a large project for me, and thus some legacy code remains.

