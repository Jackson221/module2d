/*!
 * Provides run-time options, saving & loading of options, etc.
 * 
 * Copyright 2019 Jackson Reed McNeill. Licensed under the GNU AGPL version 3 or any later version.
 */
#pragma once

#include "mathematics/primitives.hpp"
#include "synchro/enumeration.hpp"
#include "vkrender/init.hpp"

#include "render/state.hpp"

namespace options
{
	template<vkrender::res_mode_t choice> 
	struct special_res_option_t 
	{
		constexpr static auto value = choice;

		SYNCHRO_NOTHING()
	};
	template<vkrender::res_mode_t choice_a, vkrender::res_mode_t choice_b>
	bool operator==(options::special_res_option_t<choice_a> const, options::special_res_option_t<choice_b> const)
	{
		return choice_a == choice_b;
	}
	using resolution_option_selected_t = std::variant<mm::vec2, special_res_option_t<vkrender::res_mode_t::guess>, special_res_option_t<vkrender::res_mode_t::multimonitor>>;
	//Game-engine options
	class options
	{
		public:
			resolution_option_selected_t resolution;
			vkrender::fullscreen_mode_t fullscreen; 
			//TODO: Move
			std::string nickname;
			SYNCHRO_STRUCT(options, resolution,fullscreen,nickname)

			//An empty filename is a violation of the API model
			std::string filename;

			void apply(render::state* rstate);
			
	};
	options load_options(std::string path);
	void save_options(options const & _options);
}
