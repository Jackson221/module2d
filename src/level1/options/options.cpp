#include "options.hpp"

#include "synchro/synchro.hpp"
#include "simulation/netplay.hpp"
#include "filesystem/filesystem.hpp"

namespace options
{
	void options::apply(render::state* rstate)
	{
		auto res= resolution.index() == 0? std::get<mm::vec2>(resolution) : mm::vec2{0,0};
		//We have set things up so our enum aligns to this.
		//We could use for_each_iterator and grab the type value but nah
		rstate->change_resolution(res,static_cast<vkrender::res_mode_t>(resolution.index()),fullscreen);
		netplay::nickname = nickname;
	}
	options load_options(std::string path)
	{
		auto tape = synchro::serialization_tape{};
		tape.data = filesystem::read_all_from_file(path);
		auto snapshot = synchro::initial_snapshot<options>(tape);
		options result(snapshot);
		//TODO our synchro-created copy ctor won't actually copy this
		result.filename = path;
		return result;
	}
	void save_options(options const & _options)
	{
		printf("SAVE %s\n",_options.filename.c_str());
		auto snapshot = synchro::initial_snapshot<options>{_options};
		auto tape = synchro::serialization_tape{snapshot};
		filesystem::write_to_file(_options.filename, tape.data);
	}
}
