#include "audio.hpp"

#include <vector>

#include <SDL2/SDL.h>

namespace audio
{
	class backend_sound
	{
		std::vector<float> samples;
	};
	class backend_play_sound
	{
		public:
			backend_sound& the_sound;
			float volume;
			float LRbalance;
	};
	void default_audio_callback(void* userdata, Uint8* stream, int len);
	class backend_out
	{
		SDL_AudioSpec spec;
		SDL_AudioDeviceID id;
		public:
			backend_out()
			{
				SDL_AudioSpec want;
				want.freq = 48000;
				want.format = AUDIO_F32;
				want.channels = 1;
				want.samples = 256;
				//want.callback = default_audio_callback;

				id = SDL_OpenAudioDevice(nullptr, 0, &want, &spec, SDL_AUDIO_ALLOW_FORMAT_CHANGE);
			}

	};
	void default_audio_callback(void* userdata, Uint8* stream, int len)
	{

	}
	std::unique_ptr<backend_out> make_backend()
	{
		return std::make_unique<backend_out>();
	}
}
