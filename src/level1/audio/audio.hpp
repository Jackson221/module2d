#include <memory>
#include <functional>

namespace audio
{
	class backend_out;
	std::unique_ptr<backend_out> make_backend();
	class out
	{
		std::unique_ptr<backend_out> backend;
		public:
			out() : backend(make_backend())
			{
			}
	};

	class backend_sound;
	std::unique_ptr<backend_sound> make_backend_sound();

	/*!
	 * A sound effect or music; essentially something
	 * which is not real-time synthesized.
	 */
	class sound
	{

	};

	class backend_play_sound;
	std::unique_ptr<backend_play_sound> make_backend_play_sound();

	class play_sound
	{
		public:
			play_sound(sound& the_sound, float volume, float LRbalance)
			{
				
			}
	};

}
