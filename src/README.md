# Where is `main`?

`main` is located in engine.cpp


Originally, game/main.cpp was responsible for defining `main`. However, when input and rendering where split off into different threads, it was easier to have the engine define `main` for you. This is because SDL2 mandates that event processing be done on the same thread that `SDL_Init` is called on.

Thus, `main` now resides in `engine.cpp`. `game_main` is now defined by game/main.cpp, and the engine is already initialized as control enters `game_main`.

# Git submodules

Some of Module2D's code, which is intended to be re-used in Module3D, is in submodules.
