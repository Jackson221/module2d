//Copyright 2017-2019 Jackson Reed McNeill. Licensed under the GNU AGPL version 3 or any later version.<Paste>
#include "engine.hpp"

#include <SDL.h>
#include <thread>
//Static asserts
#include <limits>

#include "simulation/physics.hpp"
#include "render/sprite.hpp"
#include "render/object.hpp"
#include "UI/UI.hpp"
#include "UI/font.h"
#include "UI/chat.hpp"
#include "UI/minimap.h"
#include "algorithm/frame.hpp"
#include "render/debug.hpp"
#include "simulation/netplay.hpp"

#include "filesystem/filesystem.hpp"
#include "logger/logger.hpp"
#include "input/schedule_events.hpp"
#include "input/computer.hpp"
#include "input/interfaces.hpp"
#include "simulation/netplay.hpp"

//Included for running the unit test
#include "test/test.hpp"
#include "filesystem/filesystem.hpp"
#include "event/event.hpp"
#include "network/network.hpp"
#include "variadic_util/variadic_util.hpp"




//we need this for portability in serialization
static_assert(std::numeric_limits<double>::is_iec559,"Doubles must be implemented as IEEE 754!\n");
int init_sdl()
{
	if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
	{
		printf("Unable to initialize SDL: %s", SDL_GetError());
		return 1;
	}
	atexit(SDL_Quit);
	return 0;
}

//TODO: Remove all instances of int exceptions
engine_state initialize_engine(bool is_headless, options::options const & opts, std::string window_name)
{
	int error;

	std::unique_ptr<render::state> rstate;
    //TODO factor out legacy numerical error system and replace with exceptions
	if (!is_headless)
	{
		error = init_sdl();
		if (error != 0)
		{
			throw error;
		}


		auto const & resolution = opts.resolution;
		auto res= resolution.index() == 0? std::get<mm::vec2>(resolution) : mm::vec2{0,0};
		rstate = std::make_unique<render::state>(res,static_cast<vkrender::res_mode_t>(resolution.index()), opts.fullscreen, window_name);

		render::sprite_init(*rstate);
	}
	else
	{
		//ui::global_ui_properties = std::make_unique<ui::global_ui_properties_t>();
	}
	if (!is_headless)
	{
		error = GE_Font_Init();
		if (error != 0)
		{
			throw error+600;
		}
	}
	chat::init();
	
	srand(time(NULL)); //set random seed to time
	return engine_state{is_headless, std::move(rstate)};
}
inline engine_state::~engine_state()
{
	printf("----FULL ENGINE SHUTDOWN----\n");
	game->running = false;
	if (!is_headless)
	{
		printf("sprites\n");
		render::sprite_shutdown();
	}
	if (!is_headless)
	{
		printf("Font\n");
		GE_Font_Shutdown();
	}
}

namespace
{
	int game_thread_result;
}

struct _game_main_wrapper
{
	int operator()(int argc, char* argv[], engine_state & estate)
	{
		game_thread_result = game->game_main(argc,argv,estate);
		game->running = false;
		return game_thread_result;
	}

	//a bit of a hack to access protected members
	static std::string get_title()
	{
		return game->game_title;
	}
};


//static_assert(std::is_base_of_v<game_base, game>, "Your game class MUST be a base of `game_base`")

//TODO: At some point, command line flags should at least partially be interpreted here.
//While the game should be able to *extend* them, we need to know some things -- like if we're running headless.
//
namespace
{
	auto shorthands = flag_interpreter::shorthands_t{{"h","headless"}};
}

int main(int argc, char* argv[])
{
	printf("Hooray, I'm useful!\n");


	//Interpret flags
	
	shorthands.insert(game->shorthands.begin(),game->shorthands.end());
	
	flag_interpreter::results_t flags;
	try
	{
		flags = flag_interpreter::process(argc,argv,shorthands);
	}
	catch (std::exception const & e)
	{
		printf("Caught exception while processing flags %s\n",e.what());
		return 1;
	}	
	bool is_headless = flag_interpreter::flag_present(flags, "headless"); 
	bool do_unit_tests = 
#ifdef UNIT_TEST
		true;
#else
		flag_interpreter::flag_present(flags, "unittest"); 
#endif


	auto input_if = input::interfaces{};

	auto _ = input::event_watcher{input_if};

	filesystem::init(std::string(argv[0]));
	logger::init();
	
	options::options opts;
	auto filename = filesystem::concat_directories(filesystem::config_directory,game->game_title + "_module2dconfig.dat");
	try
	{
		opts = options::load_options(filename);
	}
	catch(std::exception const & e)
	{
		printf("Failed to load options\n");
		printf("%s\n",e.what());
		printf("Substituting default options instead.\n");
		//TODO sdl2 query screen res, fullscreen by default.
		opts = options::options(options::special_res_option_t<vkrender::res_mode_t::guess>{},vkrender::fullscreen_mode_t::fullscreen_desktop,"New Human");
	}

	auto estate = initialize_engine(is_headless,opts,_game_main_wrapper::get_title());
	estate.input_if = &input_if;
	estate.flags = flags;
	estate.options = opts;
	//TODO: Our "filename" doesn't get in the move constructor because we do not register filename in synchro.
	//	We should probably rearrange options a bit so that we have the synchronized part contained.
	estate.options.filename = filename; //can't construct non-synced things in the synchro-provided ctor
	//TODO I don't know how exactly this shouldn't be here but it shouldn't.
	netplay::nickname = estate.options.nickname;

	auto uistate_container = ui::state_container(is_headless? nullptr : &(*estate.rstate));
	estate.uistate = uistate_container.contained_state;

	auto wm = ui::window_manager{estate.uistate};
	estate.wm = &wm;

#define listen_for(type) auto type ## _listener = event::listener(input_if. type ,[&](auto in)\
	{\
		wm.dispatch_event(in);\
	}, {});

	listen_for(mouse_button)
	listen_for(mouse_motion)
	listen_for(text_input)
	listen_for(scan_code)
	listen_for(clipboard)
#undef listen_for
	///////
	
	//:/ 
	
	std::optional<render::resolution_change::types::interface_t> res_change_if;
	std::optional<event::listener<render::resolution_change::types::interface_t>> res_change_listener;
	
	if (!is_headless)
	{
		res_change_if.emplace(estate.rstate->resolution_change_manager);
		//std::make_unique<event::listener<render::resolution_change::types::manager_t>>
		res_change_listener = event::listener(*res_change_if, [&](auto in)
		{
			wm.positioning_rectangle.set_size(in.resolution);
		},{});
		wm.positioning_rectangle.set_size(estate.rstate->get_resolution());
	}
	//////
	input::remappable_buttons_t<ui::text_input_bindings> binds{input_if};
	//TODO: Relocate default bindings
	binds.hook_key_to_button(SDL_SCANCODE_LEFT, ui::text_input_bindings::left);
	binds.hook_key_to_button(SDL_SCANCODE_RIGHT, ui::text_input_bindings::right);
	binds.hook_key_to_button(SDL_SCANCODE_BACKSPACE, ui::text_input_bindings::backspace);
	binds.hook_key_to_button(SDL_SCANCODE_DELETE, ui::text_input_bindings::del);
	binds.hook_key_to_button(SDL_SCANCODE_HOME, ui::text_input_bindings::home);
	binds.hook_key_to_button(SDL_SCANCODE_END, ui::text_input_bindings::end);

	binds.hook_key_to_button(SDL_SCANCODE_RETURN, ui::text_input_bindings::finished_editing);
	binds.hook_key_to_button(SDL_SCANCODE_RETURN2, ui::text_input_bindings::finished_editing);
	binds.hook_key_to_button(SDL_SCANCODE_KP_ENTER, ui::text_input_bindings::finished_editing);

	binds.hook_joystick_button_to_button(1, ui::text_input_bindings::backspace);
	binds.hook_joystick_button_to_button(248, ui::text_input_bindings::left);
	binds.hook_joystick_button_to_button(247, ui::text_input_bindings::right);
	auto text_input_processor = ui::event_processor<ui::text_input_bindings, input::remappable_buttons_t<ui::text_input_bindings>>{binds};

	input::remappable_buttons_t<chat::keybinds> chat_binds{input_if};

	chat_binds.hook_key_to_button(SDL_SCANCODE_Y, chat::keybinds::start_chat);
	auto chat_input_processor = ui::event_processor<chat::keybinds, input::remappable_buttons_t<chat::keybinds>>{chat_binds};


	if (do_unit_tests)
	{
		auto results = test::run_tests(filesystem::unit_test,event::unit_test,network::unit_test, flag_interpreter::unit_test, variadic_util::unit_test,
				synchro::unit_test);
		int result = 0;
		for (auto test_instance : results)
			result += test_instance.get_fail_count();
		return result;
	}


	_game_main_wrapper game_main_wrapper_instance{};

	std::jthread game_main_thread{game_main_wrapper_instance, argc, argv, std::ref(estate)};

	//game_main_thread.detach();
	auto quit_listener = event::listener(input_if.quit,[&](auto)
	{
		game->running = false;
	}, {});

	if (is_headless)
	{
		while(game->running)
			std::this_thread::sleep_for(std::chrono::seconds(1)); 
	}
	else
	{
		while(game->running)
		{
			auto _ = algorithm::framerate_guard(std::chrono::nanoseconds(125000));
			SDL_Event event;
			while(SDL_PollEvent(&event))
			{
				if (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_SIZE_CHANGED)
				{
					auto new_res = mm::vec2{static_cast<double>(event.window.data1),static_cast<double>(event.window.data2)};
					if (new_res.floor() != estate.rstate->get_resolution().floor())
					{
						auto _ = std::lock_guard(estate.uistate.globallock->lock);
						estate.rstate->change_resolution(new_res,vkrender::res_mode_t::specified,estate.rstate->data.fullscreen);
					}
				}
			}
			{
				auto _ = std::lock_guard(estate.uistate.globallock->lock);
				input_if.dispatch_events();
				res_change_if->dispatch_events();
				text_input_processor.give_all_events_to(wm);
				chat_input_processor.give_all_events_to(wm);
			}
		}
	}

	printf("Awww\n");

	game_main_thread.join();
	return game_thread_result;
}
