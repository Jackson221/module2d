//Copyright 2017-2019 Jackson Reed McNeill. Licensed under the GNU AGPL version 3 or any later version.
#include "shapes.h"

#include "SDL.h"

#include <cmath>

#include "mathematics/common_ops.hpp"

render::backend_sprite create_sprite(render::state* rstate, ui::color color)
{
	SDL_Surface* LoadingSurface;

	LoadingSurface = SDL_CreateRGBSurface(0, 1, 1, 32, 0, 0, 0, 0);
	SDL_FillRect(LoadingSurface, NULL, SDL_MapRGBA(LoadingSurface->format, color.r,color.g,color.b,color.a));

	auto&& sprite = render::backend_sprite(rstate->api, LoadingSurface);
	//SDL_FreeSurface(LoadingSurface);

	return std::move(sprite);
}

GE_RectangleShape::GE_RectangleShape(render::state* rstate, ui::color color) :
	rstate(rstate),
	sprite(create_sprite(rstate, color))
{
}
const SDL_Rect renderAnimation = {0,0,1,1}; //this is constant for all rectangles
void GE_RectangleShape::render(mm::vec2r position, mm::vec2 size,	mm::vec2 axis)
{
	//TODO: Extensive testing to see if axis rotation implementation truly works
	
	//Translate(because our rstate renders centered, but this takes top-left coords)
	auto halfsize = size/2.0 - axis;
	halfsize = mm::rotation_ccw(halfsize, position.r);
	halfsize += axis;
	rstate->api.add_transient_object(sprite, size,{0,0,1,1}, position+halfsize);
}
void GE_RectangleShape::render(mm::vec2r position, mm::vec2 size)
{
	render(position,size,{0,0});
}
void GE_RectangleShape::render(mm::vec2 position, mm::vec2 size)
{
	render({position.x,position.y,0},size);
}
void GE_RectangleShape::render(mm::vec2 start, mm::vec2 end, double thickness)
{
	mm::vec2r finalposition;
	mm::vec2 finalsize;
	//the line is as big as the distance and as thick as thickness
	finalsize.x = mm::distance(start,end).get();
	finalsize.y = thickness;
	//the final starting position
	finalposition.x = start.x;
	finalposition.y = start.y-(finalsize.y/2);

	//find the angle between start and end 
	finalposition.r = -asin((end.y-start.y)/finalsize.x);
	if (end.x < start.x) //correct for sin 
	{
		finalposition.r += M_PI;
		finalposition.r *= -1;
	}
	finalposition.r = finalposition.r;
	//render the line. axis of rotation is at the far left in the center of the thickness. 
	render(finalposition,finalsize,{0,finalsize.y/2});
}

GE_HollowRectangleShape::GE_HollowRectangleShape(render::state* rstate, ui::color color,double thickness) :
	color (rstate,color)
{
	this->thickness = thickness;
}
GE_HollowRectangleShape::~GE_HollowRectangleShape()
{
}

mm::vec2 calculate_endpoint(mm::vec2 start, mm::vec2 end, double extra_thickness, double size_of_this_side)
{
	//the regular end plus the delta of start and end, multiplied by a ratio
	//this ratio brings the value from its normal length down to the size that's needed to be extended, so that the 
	//line fully coveres the corner of the rectangle (this is needed because we aren't 1px size, in fact,
	//that's why we enter extra_thickness instead of thickness; it is the difference in thickness from a 1px line.)
	return (end+((end-start)*(extra_thickness*0.5/size_of_this_side))) - mm::vec2{0.5,0.5};
}
void render_line(GE_RectangleShape& color, double thickness, mm::vec2 start, mm::vec2 end, double size_of_this_side)
{
	color.render(start,calculate_endpoint(start,end,thickness-1, size_of_this_side),thickness);
}
void GE_HollowRectangleShape::render(mm::vec2r position, mm::vec2 size)
{
	mm::vec2 points[4];
	GE_RectangleToPoints({0,0,0,size.x,size.y},{size.x/2,size.y/2},points,position-mm::vec2{size.x/2,size.y/2});

	render_line(color, thickness, points[0],points[1],size.x);
	render_line(color, thickness, points[1],points[3],size.y);
	render_line(color, thickness, points[2],points[3],size.x);
	render_line(color, thickness, points[2],points[0],size.y);
}

