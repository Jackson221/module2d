/*!
 * @file
 * @author Jackson McNeill
 * A minimap that you can register RenderedObject's positions to, and assign a color. 
 *
 * Copyright 2017-2019 Jackson Reed McNeill. Licensed under the GNU AGPL version 3 or any later version.
 */
#pragma once


//Local includes
#include "UI/UI.hpp"
#include "UI/color.hpp"
#include "render/object.hpp"
#include "render/camera.hpp"

/*!
 * Make an object show up on the minimap
 *
 * Can be called without initializing minimap (e.g. in a headless context)
 */
void GE_LinkMinimapToRenderedObject(render::object* subject, ui::color color);

/*!
 * Deleted a minimap target -- may only be used in the render thread, or whilst the render thread is locked.
 *
 * Can be called without initializing minimap (e.g. in a headless context)
 */
void GE_FreeMinimapTarget(render::object* linkedRenderedObject);

/*!
 * Schedules a minimap target to be deleted. Safe to call from any thread!
 *
 * Can be called without initializing minimap (e.g. in a headless context)
 */
void GE_ScheduleFreeMinimapTarget(render::object* linkedRenderedObject);

namespace ui
{
	class minimap : public ui::element
	{
		public:
			minimap(state uistate, double scale, ui::color background, ui::color crosshair, render::camera* camera);
			void render();
		private:
			ui::rectangle background;
			ui::rectangle crosshair_x;
			ui::rectangle crosshair_y;
			render::camera* camera;
			render::camera scaledcamera;
	};
}

