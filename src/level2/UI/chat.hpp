/*!
 * Provides multiplayer chat
 *
 * Copyright 2019 Jackson Reed McNeill. Licensed under the GNU AGPL version 3 or any later version.
 */
#pragma once

#include "UI/UI.hpp"
#include "event/event.hpp"

#include "simulation/netplay.hpp"

namespace chat
{
	namespace chat_event
	{
		struct event_data_t
		{
			std::string contents;
		};
		using net_data_t = netplay::networkable_event_data_t<event_data_t>;
		using event_configuration_types = netplay::event_configuration_types_plus_network_interface<event::event_configuration_types<net_data_t>>;
		using manager_t = event_configuration_types::manager_t;
		extern manager_t manager;
		using interface_t = event::endpoint_interface<event_configuration_types>;
		extern thread_local interface_t interface;
	}

	struct style_t
	{
		ui::text_input_style_t input_style;
		ui::font_style_t message_style;
	};

	class color_char;
	using color_string = std::basic_string<color_char>;

	enum class keybinds
	{
		start_chat,
		SIZE
	};

	class UI : public ui::element,
		public ui::has_event<input::computer::mouse_motion::event_data_t>,
		public ui::has_event<ui::binding_for_button<keybinds::start_chat>>
	{
		style_t my_style;
		public:
			ui::element_list<ui::element> text_list;
			ui::text_input input;

			void impl_dispatch_event(input::computer::mouse_motion::event_data_t motion) override;
			void impl_dispatch_event(ui::binding_for_button<keybinds::start_chat> button) override;

			inline bool is_in_bounds(mm::vec2 mouse)
			{
				return input.is_in_bounds(mouse);
			}
		private:
			event::listener<chat_event::interface_t> chat_msg_listener;
			void _clamp_scroll();
			void add_message(color_string msg);

		public:
			UI(ui::state uistate, style_t my_style);
			void impl_render() override;


	};

	void init();
}
