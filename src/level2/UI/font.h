/*!
 * @file
 * @author Jackson McNeill
 *
 * A font loading system
 *
 * Copyright 2017-2019 Jackson Reed McNeill. Licensed under the GNU AGPL version 3 or any later version.
 */
#pragma once

#include <SDL2/SDL_ttf.h>
#include <cassert>
#include <string>
#include <optional>

//TODO: Allow for setting of bold and italic, etc. options
//
//Right now, this is done through SDL_TTF and winds up globally setting that option for all fonts of that same size and name. 
//This hasn't been a problem currently but it will likely become a problem soon.
//
//And refactor this into a namespace





struct GE_Font
{
	TTF_Font* font;
	unsigned int size;
	GE_Font(TTF_Font* font, unsigned int size)
	{
		this->font = font;
		this->size = size;

		assert(font != NULL);
	}
	GE_Font(GE_Font const &font_struct)
	{
		this->font = font_struct.font;
		this->size = font_struct.size;

		assert(font != NULL);
	}
	GE_Font(GE_Font&& font_struct) noexcept
	{
		font = std::move(font_struct.font);
		size = std::move(font_struct.size);
	}
	GE_Font(){}
	GE_Font operator = (GE_Font other)
	{
		font = other.font;
		size = other.size;
		return *this;
	}

};


int GE_Font_Init();

void GE_Font_LoadFromDir(std::string dir);
std::optional<GE_Font> GE_Font_GetFont(std::string name, unsigned int size);
void GE_Font_Shutdown();

