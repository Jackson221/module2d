/*!
 * Color struct.
 */
#pragma once

#include <cstdint>
#include <functional>
#include <tuple>

namespace ui
{
	/*!
	 * Basically the same as an SDL_Color
	 */
	struct color
	{
		uint8_t r;
		uint8_t g;
		uint8_t b;
		uint8_t a;
		auto operator<=>(color const& rhs) const = default;
	};
}
template<> struct std::hash<ui::color>
{
	std::size_t operator()(ui::color const& c) const noexcept
	{
		std::size_t h1 = std::hash<int>{}(c.r);
		std::size_t h2 = std::hash<int>{}(c.g);
		std::size_t h3 = std::hash<int>{}(c.b);
		std::size_t h4 = std::hash<int>{}(c.a);
		return h1 ^ (h2 << 1) ^ (h3 << 2) ^ (h4 << 3);
	}
};
