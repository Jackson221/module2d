/*!
 * @file
 * @author Jackson McNeill
 *
 * Rendering shapes that are abstracted over SDL's, offering additional features
 *
 * Copyright 2017-2019 Jackson Reed McNeill. Licensed under the GNU AGPL version 3 or any later version.
 */
#pragma once

#include "UI/color.hpp"

#include "render/vkrender_backend.hpp"
#include "render/state.hpp"


//Local includes
#include "mathematics/primitives.hpp"


/*!
 * Base class for rendered shapes
 */
class GE_Shape
{
	public:
		virtual void render(mm::vec2r position, mm::vec2 size) = 0;
		GE_Shape(){}
		virtual ~GE_Shape(){}
};



/*!
 * A renderable rectangle, which may have transparency.
 */
class GE_RectangleShape : public GE_Shape
{
	public:
		GE_RectangleShape(render::state* rstate, ui::color color);

		GE_RectangleShape(GE_RectangleShape const & other) = delete;
		GE_RectangleShape& operator=(GE_RectangleShape const &) = delete;

		inline GE_RectangleShape(GE_RectangleShape && other) :
			rstate(std::move(other.rstate)),
			sprite(std::move(other.sprite))
		{
		}

		void render(mm::vec2r position, mm::vec2 size,mm::vec2 axis);
		void render(mm::vec2r position, mm::vec2 size);
		void render(mm::vec2 position, mm::vec2 size);
		void render(mm::vec2 start, mm::vec2 end, double thickness);
	private:
		render::state* rstate;

		render::backend_sprite sprite;



};

class GE_HollowRectangleShape : public GE_Shape
{
	public:
		GE_HollowRectangleShape(render::state* rstate, ui::color color,double thickness);
		~GE_HollowRectangleShape();
		void render(mm::vec2r position, mm::vec2 size);
		void render(mm::vec2 position, mm::vec2 size);
	private:
		GE_RectangleShape color;
		double thickness;
};



