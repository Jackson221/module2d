//Copyright 2017-2019 Jackson Reed McNeill. Licensed under the GNU AGPL version 3 or any later version.
#include "UI.hpp"

#include <cmath>
#include <iostream>
#include <math.h>
#include <string>
#include <functional>
#include <cstring>
#include <cmath>
#include <list>
#include <algorithm>
#include <stdexcept>

#include <SDL2/SDL_ttf.h>

//Local includes

#include "mathematics/common_ops.hpp"

#include "UI/shapes.h"
#include "render/sprite.hpp"
#include "render/debug.hpp"

namespace ui
{

	font_style_t::font_style_t(ui::color color, std::string font_name, unsigned int size) :
		color(color)
	{
		auto maybe_font = (GE_Font_GetFont(font_name,size));
		if (!maybe_font.has_value())
		{
			throw std::runtime_error("Invalid font ("+font_name+")");
		}
		font = maybe_font.value();
	}
	double font_style_t::get_font_height() const
	{
		return TTF_FontLineSkip(font.font);
	}

	void text::set_text(std::string text)
	{
		size_t text_length = text.length();
		if (text_length == 0) //this statement prevents `surface_msg` from being NULL.
		{
			text = " ";
		}
		if (text == current_text) //don't re-render text we've already rendered
		{
			//note: since the constructor sets current_text to "", but text cannot be "" (if it is, it is set to " ")
			//so this branch never gets visited during construction
			return; 
		}
		current_text = text;

		SDL_Surface* surface_msg = TTF_RenderUTF8_Blended(font.font, text.c_str(), SDL_Color{color.r,color.g,color.b,color.a});
		if (surface_msg == NULL) //extra protection
		{
			throw failed_to_raster_text{};
		}

		text_raster_size = {static_cast<double>(surface_msg->w), static_cast<double>(surface_msg->h)};
		positioning_rectangle.set_max_size(text_raster_size);

		positioning_rectangle.set_expanded_size(text_raster_size);
		
		sprite = render::backend_sprite(uistate.rstate->api, surface_msg);
		//SDL_FreeSurface(surface_msg); //TODO currently render::backend_sprite constructor already frees the surface
	}
	std::string text::get_text()
	{
		return current_text;
	}

	text::text(state uistate, std::string text, ui::color color, GE_Font font) : 
		element(uistate),
		font(font)
	{
		//sanity checks because you don't want errors on font rendering(hard to trace)
		if (this->font.font == NULL)
		{
			throw std::runtime_error("Your font was NULL.");
		}

		this->color = color;
		current_text = ""; 
		set_text(text); //Fills Message variable belonging to this class. May throw `failed_to_raster_text`
		scroll_position = 0;

	}
	void text::impl_render()
	{
		hud_render({0,0,0});
	}
	void text::hud_render(mm::vec2r parent_position)
	{
		mm::vec2r position = parent_position + get_position_after_confining_to_painting_boundries();
		mm::vec2 size = get_size_after_confining_to_painting_boundries();

		mm::vec2r relative_position = position - (parent_position + positioning_rectangle.get_position());
		relative_position.x = std::max(0.0, relative_position.x);
		relative_position.y = std::max(0.0, relative_position.y);

		//relative_position.round();

		mm::rect selection = {relative_position.x+scroll_position,relative_position.y,size.x,size.y};
		//selection.floor();

		mm::vec2 halfsize = (size/2.0);//.floor();

		uistate.rstate->api.add_transient_object(*sprite, size/*.floor()*/,selection, position+halfsize);
	}
	void text::set_scroll(double ammount)
	{
		scroll_position = std::max<double>(0,ammount);
	}
	double text::get_scroll()
	{
		return scroll_position;
	}

	double text::get_text_length(char const * const in_text, GE_Font font)
	{
		int size; 
		TTF_SizeUTF8(font.font,in_text,&size,NULL);
		return static_cast<double>(size);
	}
	double text::get_text_height(char const* const in_text, GE_Font font)
	{
		int size; 
		TTF_SizeUTF8(font.font,in_text,NULL,&size);
		return static_cast<double>(size);
	}
	
	
	////////

	std::string text_cursor::get_text_before_cursor()
	{
		return (cursor == 0)? "" : text.substr(0,std::min(cursor-1,text.length()));
	}
	std::string text_cursor::get_text_before_cursor_and_at_cursor()
	{
		return get_text_before_cursor()+get_text_at_cursor();
	}
	std::string text_cursor::get_text_after_cursor()
	{
		return (cursor > text.length())? "" : text.substr(cursor);
	}
	std::string text_cursor::get_text_after_cursor_and_at_cursor()
	{
		return get_text_at_cursor()+get_text_after_cursor();
	}
	std::string text_cursor::get_text_at_cursor()
	{
		return (cursor==0 || cursor > text.length())? "" : text.substr(cursor-1,1);
	}
	void text_cursor::set_cursor(size_t pos)
	{
		cursor = pos;
		cursor = std::max((size_t)0,cursor);
		cursor = std::min(text.length(),cursor);
	}
	text_input::text_input(state uistate, color cursor_color, color text_color, color color_focused, color color_unfocused, GE_Font font) :
		element(uistate),
		font(font),
		background(uistate,color_focused, color_unfocused),
		ui_text(uistate,"",text_color,font),
		str_text(""),
		cursor_rect(uistate,cursor_color),
		cursor{0,str_text}
	{
		positioning_rectangle.set_expanded_size({0,static_cast<double>(TTF_FontLineSkip(font.font))});

		add_child(background);
		background.positioning_rectangle.set_origin_size_calculator([this](){return positioning_rectangle.get_size();});
		add_child(ui_text);
		ui_text.positioning_rectangle.set_origin_size_calculator([this](){return positioning_rectangle.get_size();});
		add_child(cursor_rect);
		
		cursor_rect.positioning_rectangle.set_origin_position_calculator([this](){return positioning_rectangle.get_position()+mm::vec2{get_cursor_rect_position(),0};});
		cursor_rect.positioning_rectangle.set_origin_size_calculator([this](){return mm::vec2{2,positioning_rectangle.get_size().y};});
	}
	text_input::text_input(state uistate, text_input_style_t style) :
		text_input(uistate,style.cursor_color,style.text_color,style.color_focused, style.color_unfocused,style.font.font)
	{
	}
	void text_input::set_text(std::string _text)
	{
		ui_text.set_text(_text);
		this->str_text = _text;
		scroll_text();
	}
	void text_input::set_cursor_position(size_t position)
	{
		cursor.set_cursor(position);
		scroll_text();
	}
	void text_input::impl_dispatch_event(input::computer::mouse_motion::event_data_t motion)
	{
		if (lmouse_held)
		{
			reposition_text_cursor_to_mouse_cursor(motion.x);
		}
	}
	void text_input::impl_dispatch_event(input::computer::clipboard::event_data_t txt)
	{
		set_text(cursor.get_text_before_cursor() + txt);
		cursor.set_cursor(cursor.cursor+txt.length());
		scroll_text();
	}
	void text_input::impl_dispatch_event(on_focus)
	{
		input::computer::text_input::start_input();
		printf("foc\n");
	}
	void text_input::impl_dispatch_event(on_unfocus)
	{
		lmouse_held = false;
		input::computer::text_input::stop_input();
		printf("ufoc\n");
	}
	void text_input::impl_dispatch_event(input::computer::mouse_button::event_data_t button)
	{
		using input::computer::mouse_button::button_t;
		if (button.button == button_t::left && in_focus)
		{
			if (button.is_down)
			{
				reposition_text_cursor_to_mouse_cursor(button.x);
			}
			lmouse_held = button.is_down;
		}
	}
	void text_input::impl_dispatch_event(binding_for_button<text_input_bindings::left> button)
	{
		if (button.is_down && cursor.cursor != 0)
		{
			cursor.set_cursor(cursor.cursor-1);
			scroll_text();
		}
	}
	void text_input::impl_dispatch_event(binding_for_button<text_input_bindings::right> button)
	{
		if (button.is_down)
		{
			cursor.set_cursor(cursor.cursor+1);
			scroll_text();
		}
	}
	void text_input::impl_dispatch_event(binding_for_button<text_input_bindings::backspace> button)
	{
		if (button.is_down && cursor.cursor > 0)
		{
			set_text(cursor.get_text_before_cursor()+cursor.get_text_after_cursor());
			cursor.set_cursor(cursor.cursor-1);
			scroll_text();
		}
	}
	void text_input::impl_dispatch_event(binding_for_button<text_input_bindings::del> button)
	{
		if (button.is_down)
		{
			auto aftertxt = cursor.get_text_after_cursor();
			if (!aftertxt.empty())
				aftertxt = aftertxt.substr(1);
			set_text(cursor.get_text_before_cursor_and_at_cursor()+aftertxt);
			scroll_text();
		}
	}
	void text_input::impl_dispatch_event(binding_for_button<text_input_bindings::home> button)
	{
		if (button.is_down)
		{
			cursor.set_cursor(0);
			scroll_text();
		}
	}
	void text_input::impl_dispatch_event(binding_for_button<text_input_bindings::end> button)
	{
		if (button.is_down)
		{
			cursor.set_cursor(str_text.length());
			scroll_text();
		}
	}
	void text_input::impl_dispatch_event(binding_for_button<text_input_bindings::finished_editing> button)
	{
		if (button.is_down)
		{
			//set_focused(false);
			if (callback.has_value())
			{
				(*callback)();
			}
		}
	}
	void text_input::impl_dispatch_event(input::computer::text_input::event_data_t text)
	{
		if (in_focus)
		{
			set_text(cursor.get_text_before_cursor_and_at_cursor()+text.text+cursor.get_text_after_cursor());
			cursor.set_cursor(cursor.cursor+text.text.size());
			scroll_text();
		}
	}
	
	void text_input::reposition_text_cursor_to_mouse_cursor(int rel_mousex)
	{
		double mx = rel_mousex+ui_text.get_scroll();
		double lastPosition = 0;
		double currentPositon = 0;

		for (unsigned int pos = 0; pos != str_text.length()+1;pos++)
		{
			lastPosition = currentPositon;
			double charSize = text::get_text_length(std::string(1,str_text[pos]),font);
			currentPositon += charSize;
			if (mx >= lastPosition && mx <= currentPositon) //is the cursor at the last char?
			{
				if (mx > lastPosition+charSize/2) //if it's over half of the character, move it to the next one. this feels more accurate.
				{
					pos++;
				}
				cursor.set_cursor(pos); //set it to the last char
				scroll_text();
				return;
			}

		}
		//they must've clicked too far right 
		cursor.set_cursor(str_text.length());
		scroll_text();
	}
	std::string text_input::get_text()
	{
		return str_text;
	}
	double text_input::get_cursor_rect_position()
	{
		return text::get_text_length(cursor.get_text_before_cursor_and_at_cursor(),font)-ui_text.get_scroll();
	}
	void text_input::scroll_text()
	{
		mm::vec2 size = positioning_rectangle.get_size();

		double sizeBefore = text::get_text_length(cursor.get_text_before_cursor_and_at_cursor(),font);
		double cutofftext = (sizeBefore-ui_text.get_scroll())-size.x;
		if ( cutofftext > -0.1) //Cursor is to the too far right forcing scroll
		{
			ui_text.set_scroll(sizeBefore-size.x);
		}
		else if (cutofftext < -size.x) //Cursor is to the too far left forcing scroll
		{
			ui_text.set_scroll(ui_text.get_scroll()+size.x+cutofftext);
		}
		ui_text.set_scroll((std::min(text::get_text_length(str_text,font),ui_text.get_scroll()+size.x))-size.x);

		cursor_rect.positioning_rectangle.recalculate();
	}
	positioning_rectangle_t::positioning_rectangle_t(mm::vec2 position, mm::vec2 size) :
		positioning_rectangle_t()
	{
		this->origin_position = position;
		this->origin_size = size;
	}
	positioning_rectangle_t::positioning_rectangle_t()
	{
		memset(modifiers_array,0,sizeof(modifiers_array));
		max_size.x = max_size.y = std::numeric_limits<double>::max();
	}

	mm::vec2 positioning_rectangle_t::get_position_impl()
	{
		mm::vec2 returnPosition = origin_position;
		if (get_modifier_value(modifiers::center_x))
		{
			returnPosition.x -= get_size().x/2;
		}
		if (get_modifier_value(modifiers::center_y))
		{
			returnPosition.y -= get_size().y/2;
		}
		if (get_modifier_value(modifiers::align_right))
		{
			returnPosition.x = origin_position.x-get_size().x;
		}
		if (get_modifier_value(modifiers::align_button))
		{
			returnPosition.y = origin_position.y-get_size().y;
		}


		return returnPosition;
	}
	mm::vec2 positioning_rectangle_t::get_position()
	{
#if DEBUG_RENDERS_ENABLED()
		render_debug();
#endif
		return get_position_impl();
	}
	mm::vec2 positioning_rectangle_t::get_size_impl()
	{
		if (get_modifier_value(modifiers::expand_size_to_fit))
		{
			return expanded_size;
		}
		else if (get_modifier_value(modifiers::expand_size_to_fit_with_regular_size_as_padding))
		{
			return expanded_size+origin_size;
		}
		mm::vec2 result = origin_size;
		if(get_modifier_value(modifiers::size_x_is_min_size))
		{
			result.x = std::max(result.x,expanded_size.x);
		}
		if(get_modifier_value(modifiers::size_y_is_min_size))
		{
			result.y = std::max(result.y,expanded_size.y);
		}
		result.x = std::min(max_size.x, result.x);
		result.y = std::min(max_size.y, result.y);
		return result;
	}
	mm::vec2 positioning_rectangle_t::get_size()
	{
		return get_size_impl();
	}

	void positioning_rectangle_t::set_position(mm::vec2 position)
	{
		origin_position_calculator = std::nullopt;
		origin_position = position;
		recalculate();
	}
	void positioning_rectangle_t::set_size(mm::vec2 size)
	{
		origin_size_calculator = std::nullopt;
		origin_size = size;
		recalculate();
	}
	void positioning_rectangle_t::set_max_size(mm::vec2 new_max_size)
	{
		max_size = {new_max_size};
	}
	void positioning_rectangle_t::set_modifier(modifiers modifier, bool value)
	{
		modifiers_array[static_cast<int>(modifier)] = value;
		recalculate(); //might change the value for children
	}
	bool positioning_rectangle_t::get_modifier_value(modifiers modifier)
	{
		return modifiers_array[static_cast<int>(modifier)];
	}
	mm::vec2 positioning_rectangle_t::get_origin_position()
	{
		return origin_position;
	}
	mm::vec2 positioning_rectangle_t::get_origin_size()
	{
		return origin_size;
	}
	void positioning_rectangle_t::set_origin_position_calculator(calculator_function funct)
	{
		origin_position_calculator = {funct};
		recalculate();
	}
	void positioning_rectangle_t::set_origin_size_calculator(calculator_function funct)
	{
		origin_size_calculator = {funct};
		recalculate();
	}
	void positioning_rectangle_t::set_update_callback(update_callback_t funct)
	{
		update_callback = funct;
	}

	void positioning_rectangle_t::set_origin_position_and_size_to_be_always_same_as(positioning_rectangle_t& other)
	{
		set_origin_position_calculator([&](){ return other.get_position(); });
		set_origin_size_calculator([&](){ return other.get_size(); });
		other.add_child_rectangle(*this);
	}

	void positioning_rectangle_t::recalculate()
	{
		if(origin_position_calculator.has_value())
		{
			origin_position = origin_position_calculator.value()(); //call the lambda inside the optional. this look weird.
		}
		if(origin_size_calculator.has_value())
		{
			origin_size = origin_size_calculator.value()();
		}
		for(positioning_rectangle_t* rectangle :child_rectangles)
		{
			rectangle->recalculate();
		}
		update_callback();
	}
	void positioning_rectangle_t::set_expanded_size(mm::vec2 size)
	{
		expanded_size = size;
	}

	void positioning_rectangle_t::add_child_rectangle(positioning_rectangle_t& rectangle)
	{
		child_rectangles.push_back(&rectangle);
	}
	void positioning_rectangle_t::print_debug_info()
	{
		for(int i = 0;i!=static_cast<int>(modifiers::SIZE);i++)
		{
			printf("Modifier #%d : %s\n",i,get_modifier_value(static_cast<modifiers>(i))? "true" : "false");
		}
		printf("Origin position: %f,%f get_position(): %f,%f\n",origin_position.x,origin_position.y,get_position().x,get_position().y);
		printf("Origin size: %f,%f get_size(): %f,%f\n",origin_size.x,origin_size.y,get_size().x,get_size().y);
		printf("Expanded size %f,%f\n",expanded_size.x,expanded_size.y);
		printf("# children rectangles: %d\n",static_cast<int>(child_rectangles.size()));
	}
	void positioning_rectangle_t::render_debug()
	{
		auto position = get_position_impl();
		auto size = get_size_impl();
#if DEBUG_RENDERS_ENABLED()
#if UI_DEBUG_RENDERS
		debug::draw_rect<debug::null_position_modifier>(debug::transient_object_pusher{},mm::rect{position.x,position.y,size.x,size.y},color{162,0,255,255});
		auto [x,y] = input::computer::mouse_motion::get_cursor();
		if (check_if_focused_for_box(x,y,position,size))
		{
			for (auto child : child_rectangles)
			{
				debug::draw_arrow<debug::null_position_modifier>(debug::transient_object_pusher{},position, child->get_position_impl(),color{255,0,0,255});
			}
		}
#endif
#endif
	}


	void button::impl_render()
	{
		//adjust our size in case the text size changed TODO add calculators for expanded size
		positioning_rectangle.set_expanded_size(ui_text->positioning_rectangle.get_size());

		//if m1 down and cursor over, or was pressed, render pressed_rectangle
		//If pressed but not over, render highlight_rectangle
		//if cursor over render highlight rectangle

		if ((pressed && mouse_is_hovering_me) || artificially_pressed)
		{
			children[0] = &(*pressed_rectangle);
		}
		else if (mouse_is_hovering_me || pressed)
		{
			children[0] = &(*highlight_rectangle);
		}
		else
		{
			children[0] = &(*normal_rectangle);
		}
	}
	element* button::impl_dispatch_event_maybe_delete_parent(input::computer::mouse_button::event_data_t button)
	{
		if(mouse_is_hovering_me) //check if the cursor is over the button
		{
			if (pressed && !button.is_down)
			{
				pressed = false;
				return callback();
			}
		}
		pressed = button.is_down;
		return nullptr;
	}
	element* button::impl_dispatch_event_maybe_delete_parent(on_hover)
	{
		return highlight_callback();
	}
	element* button::impl_dispatch_event_maybe_delete_parent(on_unhover)
	{
		return unhighlight_callback();
	}
	bool button::get_is_highlighted()
	{
		return mouse_is_hovering_me;
	}

	progress_bar::progress_bar(state uistate, ui::color color, ui::color background, bool show_progress_number) :
		element(uistate),
		background(uistate.rstate,background),
		bar(uistate.rstate,color),
		show_progress_number(show_progress_number),
		progress(0)
	{
	}

	//aligns two rotated rectangles -- parrent and child -- such that the parent and child's top left corner are the same.
	mm::vec2 align_child_rect_to_parrent(double parent_rotation, mm::vec2 parrentSize, double child_rotation, mm::vec2 childSize)
	{
		//find their top lefts
		
		//set the values equal to the center
		mm::vec2 parent_top_left = parrentSize/2.0;
		mm::vec2 child_top_left = childSize/2.0;

		//apply rotation to positions
		parent_top_left = mm::rotation_ccw(parent_top_left,parent_rotation);
		child_top_left = mm::rotation_ccw(child_top_left,child_rotation);

		//subtract the center to get the top left
		
		parent_top_left -= parrentSize/2.0;
		child_top_left -= childSize/2.0;

		//find the difference between the top lefts
		
		mm::vec2 difference = child_top_left-parent_top_left;

		return difference;
	}
	void progress_bar::hud_render(mm::vec2r parent_position)
	{
		background.render(parent_position+positioning_rectangle.get_position(),positioning_rectangle.get_size(),positioning_rectangle.get_size()/2.0);
		mm::vec2 barSize = positioning_rectangle.get_size();
		barSize.x *= progress;


		//Probably a more effecient way of doing this, but this works.

		if (parent_position.r != 0.0) //small micro-optmization, we don't need need to calculate all of this if we have no rotation because result.x and result.y will be 0
		{
			parent_position = parent_position+align_child_rect_to_parrent(parent_position.r,positioning_rectangle.get_size(),parent_position.r,barSize);
		}
		parent_position.x += positioning_rectangle.get_position().x;
		parent_position.y += positioning_rectangle.get_position().y;

		bar.render(parent_position,barSize,barSize/2.0);
	}
	void progress_bar::impl_render()
	{
		hud_render({0,0,0});
	}
	void progress_bar::set_progress(double progress)
	{
		this->progress = std::max(std::min(progress,1.0),0.0);
	}	
	double progress_bar::get_progress()
	{
		return progress;
	}
	void draggable_progress_bar::impl_dispatch_event(input::computer::mouse_button::event_data_t button)
	{
		if (button.button == input::computer::mouse_button::button_t::left)
		{
			poll_mouse = true;
			set_progress(button.x / positioning_rectangle.get_size().x);
		}
		//they could de-focus us by right-clicking on another thing while holding down left, so this should respond to any mouse-ups.
		if(!button.is_down)
		{
			poll_mouse = false;
		}
	}
	void draggable_progress_bar::impl_dispatch_event(input::computer::mouse_motion::event_data_t motion)
	{
		if (poll_mouse)
		{
			set_progress(motion.x / positioning_rectangle.get_size().x);
		}
	}
		
	auto generate_button_text(state uistate, window_title_style_t style)
	{
		auto button_text = std::make_shared<text>(uistate,"X",style.close_buttonFont);
		button_text->positioning_rectangle.set_modifier(positioning_rectangle_t::modifiers::expand_size_to_fit,true);
		button_text->positioning_rectangle.set_modifier(positioning_rectangle_t::modifiers::center_x,true);
		button_text->positioning_rectangle.set_modifier(positioning_rectangle_t::modifiers::center_y,true);
		return button_text;
	}

	title_bar::title_bar(state uistate, std::string name, window_title_style_t style) :
		element(uistate),
		title(uistate,name,style.font.color,style.font.font),
		//background(uistate,style.background),
		button_text(generate_button_text(uistate, style)),
		style(style),
		close_button(uistate,style.close_button,button_text)
	{
		add_child(&title);
		title.positioning_rectangle.set_modifier(positioning_rectangle_t::modifiers::expand_size_to_fit,true);
		title.positioning_rectangle.set_modifier(positioning_rectangle_t::modifiers::center_y,true);
		if (style.centerTitleText)
		{
			title.positioning_rectangle.set_modifier(positioning_rectangle_t::modifiers::center_x,true);
		}
		title.positioning_rectangle.set_origin_position_calculator([this,style]()
		{
			return positioning_rectangle.get_position()+mm::vec2{(style.centerTitleText? positioning_rectangle.get_size().x/2 : 0)+style.textOffset,style.height/2};
		});
		title.positioning_rectangle.set_size({0,style.height});
		/*background.positioning_rectangle.set_origin_position_calculator([this](){return positioning_rectangle.get_position();});
		background.positioning_rectangle.set_origin_size_calculator([this,style](){return mm::vec2{positioning_rectangle.get_size().x,style.height};});
		positioning_rectangle.add_child_rectangle(background.positioning_rectangle);
		add_child(&background);*/

		add_child(&close_button);
		close_button.positioning_rectangle.set_modifier(positioning_rectangle_t::modifiers::align_right,true);
		close_button.positioning_rectangle.set_origin_position_calculator([this,style](){return mm::vec2{positioning_rectangle.get_size().x-style.buttonDistanceFromRight,0}+positioning_rectangle.get_position();});
		close_button.positioning_rectangle.set_size({button_text->positioning_rectangle.get_size().x*2+style.close_buttonPadding,style.height});
		this->dragging = false;
	}
	void title_bar::impl_dispatch_event(input::computer::mouse_button::event_data_t button)
	{
		if (button.is_down && !close_button.get_is_highlighted())
		{
			dragging = true;
			initial_drag_position = {static_cast<double>(button.x),static_cast<double>(button.y)};
		}
		else if (!button.is_down)
		{
			dragging = false;
		}
	}
	void title_bar::impl_dispatch_event(input::computer::mouse_motion::event_data_t motion)
	{
		if (dragging)
		{
			positioning_rectangle.set_position(positioning_rectangle.get_origin_position()+mm::vec2{static_cast<double>(motion.x)-initial_drag_position.x,static_cast<double>(motion.y)-initial_drag_position.y});
		}
	}
	
	surface::surface(state uistate,color backgroundColor) :
		element(uistate),
		background_rect(uistate,backgroundColor)
	{
		background_rect.positioning_rectangle.set_origin_position_and_size_to_be_always_same_as(positioning_rectangle);
	}
	surface::~surface() 
	{
		for (element* element : children)
		{
			delete element;
		}
	}
	int surface::addelement(element* element)
	{
		add_child(element);
		return std::distance(children.begin(),children.end())-1;
	}
	element* surface::getelement(int elementID)
	{
		return children[elementID];
	}
	void surface::impl_render()
	{
		background_rect.render();
	}

	window::window(state uistate, std::string name, mm::vec2 position, mm::vec2 surfaceSize, style style) :
		element(uistate),
		ui_surface(uistate,style.window_style.background),
		ui_title_bar(uistate, name, style.window_style.title_style),
		border(uistate,style.window_style.border_color,style.window_style.border_color_unfocused)
	{
		title_barHeight = style.window_style.title_style.height;
		borderOffset = style.window_style.border_size;
		extra_dragging = style.window_style.extra_dragging;

		positioning_rectangle.set_position(position);

		border.positioning_rectangle.set_origin_position_and_size_to_be_always_same_as(positioning_rectangle);
		add_child(border);


		positioning_rectangle.set_origin_position_calculator([this]{return ui_title_bar.positioning_rectangle.get_position();});
		ui_title_bar.positioning_rectangle.add_child_rectangle(positioning_rectangle);
		//add_child(&ui_title_bar);
		//TODO fix this child inversion (window depends on its child title_bar position)
		children.push_back(&ui_title_bar);



		mm::vec2 additional_size_from_surface = mm::vec2{style.window_style.border_size*2,style.window_style.border_size+style.window_style.title_style.height};
		positioning_rectangle.set_size(surfaceSize+additional_size_from_surface);
		add_child(ui_surface);
		ui_surface.positioning_rectangle.set_origin_size_calculator([this, additional_size_from_surface](){return positioning_rectangle.get_size()-additional_size_from_surface;});
		auto const rendering_surface_offset_from_top_left = mm::vec2{borderOffset,style.window_style.title_style.height};
		ui_surface.positioning_rectangle.set_origin_position_calculator([=, this](){return positioning_rectangle.get_position()+rendering_surface_offset_from_top_left;});

		ui_title_bar.positioning_rectangle.set_size({surfaceSize.x+style.window_style.border_size, style.window_style.title_style.height});
		ui_title_bar.positioning_rectangle.set_position(position);

		/*mm::vec2 last_size = {0,0};
		painting_boundries_rectangle.set_update_callback([this,last_size]() mutable
		{
			if (painting_boundries_rectangle.get_size() == last_size)
			{
				return;
			}
			last_size = positioning_rectangle.get_size();
			//do not allow window to become impossible to drag (i.e. make sure title_bar is always visible)
			//<= 0 is commented out because it causes problems and is not possible as far as my testing goes
			const double minimum_visible_px = 50;
			if (positioning_rectangle.get_position().x+minimum_visible_px >= painting_boundries_rectangle.get_size().x)
			{
				ui_title_bar.positioning_rectangle.set_position(mm::vec2{painting_boundries_rectangle.get_size().x-minimum_visible_px,positioning_rectangle.get_position().y});
			}
			if (positioning_rectangle.get_position().y+minimum_visible_px >= painting_boundries_rectangle.get_size().y)
			{
				ui_title_bar.positioning_rectangle.set_position(mm::vec2{positioning_rectangle.get_position().x,painting_boundries_rectangle.get_size().y-minimum_visible_px});
			}
			else if(positioning_rectangle.get_position().y <= 0)
			{
				ui_title_bar.positioning_rectangle.set_position(mm::vec2{positioning_rectangle.get_position().x,0});
			}
		});*/

	}
	void window::impl_dispatch_event(on_focus)
	{
		dispatch_event(window_on_focus{});
	}
	void window::impl_dispatch_event(on_unfocus)
	{
		dispatch_event(window_on_unfocus{});
	}
	void window::impl_dispatch_event(input::computer::mouse_button::event_data_t button)
	{
		if (button.is_down)
		{
			double offset = borderOffset+extra_dragging;
			if ((button.y > ui_title_bar.positioning_rectangle.get_size().y) && //not clicking on the title_bar
					!( //not inside the window contents; actually on its border
						(
							button.x > offset && button.x < positioning_rectangle.get_size().x-offset
						) &&
						(
							button.y > offset && button.y < positioning_rectangle.get_size().y-offset
						)
					 )
				)
			{
				is_resizing = true;
				resizing_coeffecients = {0,0};
				if (button.x < offset) 
				{
					resizing_coeffecients.x = -1.0;	
				}
				else if (button.x > positioning_rectangle.get_size().x-offset)
				{
					resizing_coeffecients.x = 1.0;	
				}
				if (button.y > positioning_rectangle.get_size().y-offset)
				{
					resizing_coeffecients.y = 1.0;	
				}
			}
		}
		else
		{
			is_resizing = false;
		}
	}
	void window::impl_dispatch_event(input::computer::mouse_motion::event_data_t motion)
	{
		if (is_resizing)
		{
			auto move = (mm::vec2{static_cast<double>(motion.delta_x), static_cast<double>(motion.delta_y)}*resizing_coeffecients);
			auto new_size = positioning_rectangle.get_size() + move;
			new_size.x = std::max(1.0+(borderOffset*2),new_size.x);
			new_size.y = std::max(1.0+title_barHeight+borderOffset,new_size.y);
			new_size.max_between(min_size.get_size());

			positioning_rectangle.set_size(new_size);

			if (resizing_coeffecients.x < 0)
			{
				ui_title_bar.positioning_rectangle.set_position(ui_title_bar.positioning_rectangle.get_position() - mm::vec2{move.x,0});
			}
			if (resizing_coeffecients.y < 0)
			{
				ui_title_bar.positioning_rectangle.set_position(ui_title_bar.positioning_rectangle.get_position() - mm::vec2{0,move.y});
			}
			ui_title_bar.positioning_rectangle.set_size({positioning_rectangle.get_size().x,title_barHeight});
		}
	}
	



	rectangle::rectangle(state uistate, color color) :
		element(uistate),
		rectangle_shape(uistate.rstate,color)
	{
	}
	void rectangle::impl_render()
	{
		rectangle_shape.render(get_position_after_confining_to_painting_boundries(),get_size_after_confining_to_painting_boundries());
	}
	focus_color_change_rectangle::focus_color_change_rectangle(state uistate, color focus_color, color unfocused_color) :
		element(uistate),
		focus(uistate, focus_color),
		unfocused(uistate, unfocused_color)
	{
		add_child_without_modfying_positioning_rectangles(focus);

		focus.positioning_rectangle.set_origin_position_and_size_to_be_always_same_as(positioning_rectangle);
		unfocused.positioning_rectangle.set_origin_position_and_size_to_be_always_same_as(positioning_rectangle);

		focus.painting_boundries_rectangle.set_origin_position_and_size_to_be_always_same_as(painting_boundries_rectangle);
		unfocused.painting_boundries_rectangle.set_origin_position_and_size_to_be_always_same_as(painting_boundries_rectangle);
	}
	void focus_color_change_rectangle::impl_dispatch_event(window_on_focus)
	{
		remove_child(size_t(0));
		add_child_without_modfying_positioning_rectangles(focus);
	}
	void focus_color_change_rectangle::impl_dispatch_event(window_on_unfocus)
	{
		remove_child(size_t(0));
		add_child_without_modfying_positioning_rectangles(unfocused);
	}

	sprite::sprite(state uistate, sprite_style_t style) :
		element(uistate)
	{
		real_sprite = render::get_sprite(style.sprite_name);
		tint = style.tint;
		sprite_selection = style.selection;
	}
	void sprite::impl_render()
	{
		mm::vec2 position = get_position_after_confining_to_painting_boundries();
		mm::vec2 bounding_size = get_size_after_confining_to_painting_boundries();

		uistate.rstate->api.add_transient_object(*real_sprite, bounding_size, get_selection_rectangle_after_confining_to_painting_boundries(sprite_selection), static_cast<mm::vec2r>(position+bounding_size/2.0));
	}

	popup_ok::popup_ok(state uistate, mm::vec2 position, mm::vec2 size, element* displayelement,std::string name, std::string oktxt, std::string canceltxt, popup_ok_style_t style, std::function<element*()> ok_callback, std::function<element*()> cancel_callback, bool has_cancel) :
		element(uistate),
		ui_window(uistate,name,position,size,style.window)
	{
		children.push_back(&ui_window);
		positioning_rectangle.set_origin_position_and_size_to_be_always_same_as(ui_window.positioning_rectangle);

		ui_window.ui_title_bar.close_button.callback = cancel_callback;
		ui_window.ui_surface.addelement(displayelement);

		auto oktxtelement = std::make_shared<text>(uistate,oktxt,style.buttonFont);
		oktxtelement->positioning_rectangle.set_modifier(positioning_rectangle_t::modifiers::center_x,true);
		oktxtelement->positioning_rectangle.set_modifier(positioning_rectangle_t::modifiers::center_y,true);
		oktxtelement->positioning_rectangle.set_modifier(positioning_rectangle_t::modifiers::expand_size_to_fit,true);
		ok = new button(uistate,style.buttons,oktxtelement);
		ui_window.ui_surface.addelement(ok);
		ok->positioning_rectangle.set_modifier(positioning_rectangle_t::modifiers::align_button,true);
		ok->positioning_rectangle.set_modifier(positioning_rectangle_t::modifiers::align_right,true);
		if (style.useSizeAsPadding)
		{
			ok->positioning_rectangle.set_modifier(positioning_rectangle_t::modifiers::expand_size_to_fit_with_regular_size_as_padding,true);
		}
		else
		{
			ok->positioning_rectangle.set_modifier(positioning_rectangle_t::modifiers::size_x_is_min_size,true);
			ok->positioning_rectangle.set_modifier(positioning_rectangle_t::modifiers::size_y_is_min_size,true);
		}
		ok->positioning_rectangle.set_origin_position_calculator([=,this](){return ui_window.ui_surface.positioning_rectangle.get_position()+ui_window.ui_surface.positioning_rectangle.get_size()-mm::vec2{style.buttonSpacing,style.buttonDistanceFromBottom};});
		ok->positioning_rectangle.set_size(style.buttonSize);
		ok->callback = [this,ok_callback]()
		{
			return ok_callback(); 
		};


		if (has_cancel)
		{
			auto canceltxtelement = std::make_shared<text>(uistate,canceltxt,style.buttonFont);
			canceltxtelement->positioning_rectangle.set_modifier(positioning_rectangle_t::modifiers::center_x,true);
			canceltxtelement->positioning_rectangle.set_modifier(positioning_rectangle_t::modifiers::center_y,true);
			canceltxtelement->positioning_rectangle.set_modifier(positioning_rectangle_t::modifiers::expand_size_to_fit,true);
			cancel = new button(uistate,style.buttons,canceltxtelement);
			ui_window.ui_surface.addelement(cancel);
			cancel->positioning_rectangle.set_modifier(positioning_rectangle_t::modifiers::align_right,true);
			cancel->positioning_rectangle.set_origin_position_calculator([=,this](){return ok->positioning_rectangle.get_position()-mm::vec2{style.buttonSpacing,0};});
			cancel->positioning_rectangle.set_size(style.buttonSize);
			if (style.useSizeAsPadding)
			{
				cancel->positioning_rectangle.set_modifier(positioning_rectangle_t::modifiers::expand_size_to_fit_with_regular_size_as_padding,true);
			}
			else
			{
				cancel->positioning_rectangle.set_modifier(positioning_rectangle_t::modifiers::size_x_is_min_size,true);
				cancel->positioning_rectangle.set_modifier(positioning_rectangle_t::modifiers::size_y_is_min_size,true);
			}

			cancel->callback = ui_window.ui_title_bar.close_button.callback;
		}

		area_between_top_of_surface_and_top_of_buttons.set_origin_position_calculator([this](){return ui_window.ui_surface.positioning_rectangle.get_position();});
		area_between_top_of_surface_and_top_of_buttons.set_origin_size_calculator([this]()
		{
			return mm::vec2
			{
				ui_window.ui_surface.positioning_rectangle.get_size().x,
				ok->positioning_rectangle.get_position().y -  ui_window.ui_surface.positioning_rectangle.get_position().y
			};
		});
		ui_window.ui_surface.positioning_rectangle.add_child_rectangle(area_between_top_of_surface_and_top_of_buttons);
		ui_window.min_size.set_size(ui_window.positioning_rectangle.get_size());
	}
	window_manager::window_manager(state uistate) :
		element(uistate)
	{
		in_focus = true;
	}
	void window_manager::impl_child_changed_focus(size_t last_focused_child, size_t& new_focused_child)
	{
		auto new_focused_child_ptr = children[new_focused_child];
		if (new_focused_child_ptr != &(*background))
		{
			//elevate it to the top of render/input order
			children.erase(children.begin() + new_focused_child);

			//and now shift the id 
			new_focused_child = children.size();

			add_child_without_modfying_positioning_rectangles(new_focused_child_ptr);
		}
	}
	void window_manager::set_background(std::unique_ptr<element>&& new_background)
	{
		assert(new_background); //new_background shall not be an empty unique_ptr
		if (background)
		{
			children.front() = &(*new_background);
			if (focused_child == 0)
			{
				focused_child = element::NO_FOCUSED_CHILD_VAL;
			}
			positioning_rectangle.remove_child(background->positioning_rectangle);
		}
		else
		{
			if (focused_child < children.size())
			{
				++focused_child;
			}
			children.insert(children.begin(), &(*new_background));
		}
		new_background->positioning_rectangle.set_origin_position_and_size_to_be_always_same_as(positioning_rectangle);
		background.reset();
		background = std::move(new_background);
		if (focused_child == element::NO_FOCUSED_CHILD_VAL)
		{
			set_focused_element(*background);
		}
	}
	void window_manager::add_window(element* window, bool steal_focus)
	{
		add_child_without_modfying_positioning_rectangles(window);
		window->painting_boundries_rectangle.set_origin_position_and_size_to_be_always_same_as(painting_boundries_rectangle);
		if(steal_focus)
		{
			set_focused_element(children.size()-1);
		}
	}
	void window_manager::remove_window(element* window)
	{
		remove_child(window);
		painting_boundries_rectangle.remove_child(window->painting_boundries_rectangle);
	}

	void popup_message(state uistate, window_manager* wm, mm::vec2 position, mm::vec2 size, std::string title, std::string msg, font_style_t font_style, popup_ok_style_t style)
	{
		ui::text* text = new ui::text(uistate,msg,font_style);
		text->positioning_rectangle.set_modifier(ui::positioning_rectangle_t::modifiers::expand_size_to_fit,true);
		text->positioning_rectangle.set_modifier(ui::positioning_rectangle_t::modifiers::center_x,true);
		text->positioning_rectangle.set_modifier(ui::positioning_rectangle_t::modifiers::center_y,true);
		ui::popup_ok* popup;

		popup = static_cast<ui::popup_ok*>(::operator new (sizeof(ui::popup_ok)));
		auto ok_c = [popup,wm]()
		{
			wm->remove_window(popup);
			//put the pointer on the stack, cause this lambda's memory will get deleted.
			auto* popup_on_the_stack = popup;
			delete popup_on_the_stack;
			return popup_on_the_stack;
		};
		new (popup) ui::popup_ok(uistate, position-(size/2), size, text,title,  "OK", "",style,ok_c,ok_c,false);
		wm->add_window(popup);

		text->positioning_rectangle.set_origin_position_calculator([popup]()
		{
			return popup->ui_window.ui_surface.positioning_rectangle.get_position()+(popup->ui_window.ui_surface.positioning_rectangle.get_size()/2);
		});
		popup->ui_window.ui_surface.positioning_rectangle.add_child_rectangle(text->positioning_rectangle);
	}
}
