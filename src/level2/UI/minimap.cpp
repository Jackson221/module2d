//Copyright 2017-2019 Jackson Reed McNeill. Licensed under the GNU AGPL version 3 or any later version.
#include "minimap.h"

#include <mutex>
#include <map>
#include <stack>
#include "render/camera.hpp"
#include "mathematics/primitives.hpp"

struct GE_MinimapTarget //exists to allow for future expansion
{
	ui::color color;
};

std::unordered_map<render::object*,GE_MinimapTarget> minimapTargets; //this works PERFECTLY for what I need it for--it requires no additional variables for the user to store, just tell us what linked rendered object you're deleting before you delete it

std::stack<render::object*> scheduledToDelete;

std::mutex delete_stack_mutex;
std::mutex targets_access_mutex;

void GE_LinkMinimapToRenderedObject(render::object* subject, ui::color color)
{
	GE_MinimapTarget newTarget = GE_MinimapTarget{color};
	targets_access_mutex.lock();
	minimapTargets[subject] = newTarget;
	targets_access_mutex.unlock();
}
void GE_FreeMinimapTarget(render::object* linkedrenderedObject)
{
	targets_access_mutex.lock();
	minimapTargets.erase(linkedrenderedObject);
	targets_access_mutex.unlock();
}

void GE_ScheduleFreeMinimapTarget(render::object* linkedrenderedObject)
{
	delete_stack_mutex.lock();

	scheduledToDelete.push(linkedrenderedObject);	

	delete_stack_mutex.unlock();

}
//internal function - not exposed
void deleteTargetsScheduledForRemoval()
{
	delete_stack_mutex.lock();
	while (!scheduledToDelete.empty())
	{
		render::object* i = scheduledToDelete.top();
		scheduledToDelete.pop();

		GE_FreeMinimapTarget(i);
	}
	delete_stack_mutex.unlock();
}
namespace ui
{
	minimap::minimap(state uistate, double scale, ui::color background, ui::color crosshair, render::camera* camera) : 
		element(uistate),
		background(uistate,background), 
		crosshair_x(uistate,crosshair), 
		crosshair_y(uistate,crosshair)
	{
		this->background.positioning_rectangle.set_origin_position_and_size_to_be_always_same_as(positioning_rectangle);

		crosshair_x.positioning_rectangle.set_origin_position_calculator([this](){return mm::vec2{0,positioning_rectangle.get_size().y/2}+positioning_rectangle.get_position();});
		crosshair_x.positioning_rectangle.set_origin_size_calculator([this](){return mm::vec2{positioning_rectangle.get_size().x,1};});
		positioning_rectangle.add_child_rectangle(crosshair_x.positioning_rectangle);

		crosshair_y.positioning_rectangle.set_origin_position_calculator([this](){return mm::vec2{positioning_rectangle.get_size().x/2,0}+positioning_rectangle.get_position();});
		crosshair_y.positioning_rectangle.set_origin_size_calculator([this](){return mm::vec2{1,positioning_rectangle.get_size().y};});
		positioning_rectangle.add_child_rectangle(crosshair_y.positioning_rectangle);

		this->uistate = uistate;
		this->camera = camera;
		scaledcamera.scale = scale;

	}
	mm::vec2 const dot_size = {3,3};
	void minimap::render()
	{

		background.render();
		crosshair_x.render();
		crosshair_y.render();




		// anything scheduled for removal
		
		deleteTargetsScheduledForRemoval(); 

		mm::vec2 size = positioning_rectangle.get_size();
		targets_access_mutex.lock();
		for (auto i : minimapTargets)
		{
			scaledcamera.pos = camera->pos;
			scaledcamera.screen = size;

			GE_MinimapTarget subject_minimap = i.second;
			render::object* subject = i.first;
			ui::color color = subject_minimap.color;

			//translate to minimap space (0,0) to (size.x, size.y)


			//TODO: Needs refactoring for ECS
			mm::vec2r position;// = scaledcamera.apply_camera_offset(subject->position);
			position = position - dot_size/2.0;

			if ((position.x>=0)&&(position.y>=0)&&(position.x<=size.x)&&(position.y<=size.y)) //check if the object is within the bounds of the minimap
			{
				/*
				//translate to screen space
				position += positioning_rectangle.get_position();
				//render the object
				SDL_SetRenderDrawColor(uistate,color.r, color.g, color.b, color.a);
				temprect = SDL_Rect{static_cast<int>(position.x),static_cast<int>(position.y),static_cast<int>(dot_size.x),static_cast<int>(dot_size.y)};
				SDL_RenderFillRect(uistate,&temprect);
				*/ //TODO vkrender
			}
		}
		targets_access_mutex.unlock();
	}
}
