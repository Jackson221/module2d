//Copyright 2017-2019 Jackson Reed McNeill. Licensed under the GNU AGPL version 3 or any later version.
#include "UI/font.h"

#include <map>
#include <filesystem>

//Local includes
#include "filesystem/filesystem.hpp"

typedef std::string fontName_t;
typedef std::string fontPath_t;

typedef std::pair<fontName_t,unsigned int> fontPair_t;

typedef std::map<fontName_t,fontPath_t> fonts_t;
typedef std::map<fontPair_t,GE_Font> fontRenders_t;

fonts_t fonts; //font name:font path
fontRenders_t fontRenders; //font name, size: font


int GE_Font_Init()
{	
	int ttferror = TTF_Init();
	if (ttferror < 0) 
	{
		//TODO: Handle error...
		printf("TTF_Init error %d\n",ttferror);
		return ttferror;
	}
	return 0;
}
void GE_Font_LoadFromDir(std::string dir)
{
	auto path = std::filesystem::path(filesystem::host_os_path_format(dir));
	for (auto& d : std::filesystem::recursive_directory_iterator(path))
	{
		auto font_path = d.path();
		if (std::filesystem::is_regular_file(font_path) && font_path.extension() == ".ttf")
		{
			fonts.insert( std::make_pair(font_path.stem().string(), filesystem::generic_path_format(font_path.string())));
		}
	}
}
std::optional<GE_Font> GE_Font_GetFont(std::string name, unsigned int size)
{
	//fontPath_t font = *(fonts.find(name));
	fontRenders_t::iterator fontIt = fontRenders.find(fontPair_t(name,size));
	if (fontIt == fontRenders.end())
	{
		fonts_t::iterator pathIt = fonts.find(name);
		if (pathIt == fonts.end())
		{
			return {};
		}
		std::string path = filesystem::host_os_path_format(pathIt->second);
		GE_Font font = {TTF_OpenFont(path.c_str(), size),size};
		if (!(font.font))
		{
			printf("TTF_OpenFont: %s\n", TTF_GetError());
			return {};
		}
		fontRenders.insert(std::make_pair(fontPair_t(name,size),font));

		return {font};
	}
	return {fontIt->second};
}
void GE_Font_Shutdown()
{
	fontRenders_t::iterator it;
	while (true)
	{
		it = fontRenders.begin();
		if (it == fontRenders.end())
		{
			break;
		}
		TTF_CloseFont(it->second.font);
		fontRenders.erase(it);
	}
	TTF_Quit();
}
