#include "chat.hpp"

#include "UI/UI.hpp"
#include "UI/color.hpp"
#include "simulation/physics.hpp"

namespace chat
{
	ui::color constexpr name_color = {0x1e,0x88,0x1a,0xff};//{0x10,0x20,0x50,0xff};


	template<typename T>
	concept AcceptableChar = requires(T t, char y)
	{
		{t == y} -> std::same_as<bool>;
		{std::is_pod_v<T> == true};
	};

	template<typename T>
	concept AcceptableString = requires(T t)
	{
		{t.c_str()} -> std::same_as<const char*>;
		{t.begin()} -> std::same_as<typename T::iterator>;
		{t.end()} -> std::same_as<typename T::iterator>;
		{T(T::iterator, T::iterator)};
	};


	struct color_char
	{
		char underlying_char;
		ui::color color;

		inline bool operator==(char other_char)
		{
			return underlying_char == other_char;
		}
	};

	static_assert(AcceptableChar<color_char>);


	color_string create_monocolor_string_segment(std::string str, ui::color color)
	{
		color_string && return_val = {};
		return_val.reserve(str.size());
		for (char c : str)
		{
			return_val.push_back(color_char{c, color});
		}
		return std::move(return_val);
	}
	std::string convert_color_string_to_normal_string(color_string str)
	{
		std::string && return_val = {};
		return_val.reserve(str.size());
		for (color_char c : str)
		{
			return_val.push_back(c.underlying_char);
		}
		return std::move(return_val);
	}


	class color_text : public ui::element
	{
		mm::vec2 add_txt(ui::state uistate, std::string current_txt, ui::color current_color, GE_Font font, mm::vec2 current_offset)
		{
			auto this_color_text = new ui::text(uistate,current_txt, ui::font_style_t{current_color,font});
			add_child(this_color_text);
			this_color_text->positioning_rectangle.set_origin_position_calculator([current_offset,this](){return positioning_rectangle.get_position()+current_offset;});

			this_color_text->positioning_rectangle.set_modifier(ui::positioning_rectangle_t::modifiers::expand_size_to_fit,true);
			current_offset += mm::vec2{this_color_text->positioning_rectangle.get_size().x,0};

			positioning_rectangle.add_child_rectangle(this_color_text->positioning_rectangle);
			texts.emplace_back(this_color_text);

			return current_offset;

		}
		public:
			color_text(ui::state uistate, color_string str, GE_Font font) : 
				ui::element(uistate)
			{
				if (!str.empty())
				{
					mm::vec2 current_offset = {0,0};

					std::string current_txt;
					current_txt.reserve(str.size());

					ui::color current_color = str.front().color;
					for (auto this_char : str)
					{
						if (this_char.color != current_color)
						{
							printf("shift\n");
							current_offset = add_txt(uistate,current_txt,current_color,font,current_offset);	

							current_txt.clear();
							current_color = this_char.color;
						}
						current_txt += this_char.underlying_char;
					}
					current_offset = add_txt(uistate,current_txt,current_color,font,current_offset);	

					double max_size_y = 0;
					for( auto const & text_ptr : texts)
					{
						max_size_y = std::max( max_size_y, text_ptr->positioning_rectangle.get_size().y );
					}
					positioning_rectangle.set_expanded_size({current_offset.x, max_size_y});

				}
			}
		private:
			std::vector<std::unique_ptr<ui::text>> texts;
	};
	



	namespace chat_event
	{
		manager_t manager("chat_event");
		thread_local event::endpoint_interface<event_configuration_types> interface(manager);
		std::optional<netplay::network_interface<event_configuration_types>> netif;
	}
	UI::UI(ui::state uistate, style_t _my_style) :
		ui::element(uistate),
		my_style(_my_style),
		text_list(uistate),
		input(uistate,my_style.input_style),
		chat_msg_listener(chat_event::interface,[this](auto in)
		{
			std::string name = netplay::net_state == netplay::state::offline? netplay::nickname : netplay::client_names[in.client_id];
			color_string output_msg = create_monocolor_string_segment(name,name_color) + create_monocolor_string_segment(": "+in.event_data.contents,my_style.message_style.color);
			add_message(output_msg);
			if (netplay::net_state == netplay::state::offline)
			{
				add_message(create_monocolor_string_segment("In offline mode, no one can hear you scream.", ui::color{0xff,0x00,0x00,0xff}));
			}
		},{})
	{
		add_child(input);
		add_child(text_list);
		input.positioning_rectangle.set_modifier(ui::positioning_rectangle_t::modifiers::align_button,true);
		input.positioning_rectangle.set_modifier(ui::positioning_rectangle_t::modifiers::align_right,true);
		input.positioning_rectangle.set_modifier(ui::positioning_rectangle_t::modifiers::expand_size_to_fit_with_regular_size_as_padding,true);

		input.positioning_rectangle.set_origin_position_calculator([this]() {return positioning_rectangle.get_position()+positioning_rectangle.get_size();});
		text_list.positioning_rectangle.set_origin_position_calculator([this]() {return positioning_rectangle.get_position();});

		input.positioning_rectangle.set_origin_size_calculator([this](){ return mm::vec2{positioning_rectangle.get_size().x,0}; });
		text_list.positioning_rectangle.set_origin_size_calculator([this](){ return positioning_rectangle.get_size()-mm::vec2{0.0,input.positioning_rectangle.get_size().y}; });

		input.callback = [this]()
		{
			chat_event::interface.push_event({input.get_text()});
			chat_event::interface.send_events();
			chat_event::interface.dispatch_events();
			input.set_text("");

			defocus_any_element();
		};
	}
	void UI::add_message(color_string msg)
	{
		printf("chat{%s}\n",convert_color_string_to_normal_string(msg).c_str());

		double max_length = positioning_rectangle.get_size().x;

		auto font = my_style.message_style.font;
		auto text_too_big_function = [=](auto in){return ui::text::get_text_length(convert_color_string_to_normal_string(in), font) > max_length;};

		std::vector<color_string> lines = ui::text::split_text_into_lines(msg, text_too_big_function);
		for (auto const & line : lines)
		{
			auto insert_text = new color_text{uistate,line,my_style.message_style.font};
			insert_text->positioning_rectangle.set_modifier(ui::positioning_rectangle_t::modifiers::expand_size_to_fit,true);
			text_list.push_element(insert_text);
		}

		text_list.set_scroll(text_list.get_scroll() - (my_style.message_style.get_font_height() * lines.size()));
		_clamp_scroll();

		
		/* //IMAGE TEST
		auto _st = ui::sprite_style_t{"simple",{255,255,255,255},{0,0,8,9}};
		auto _ptr = new ui::sprite(uistate,_st);
		//_ptr->positioning_rectangle.set_modifier(ui::positioning_rectangle_t::modifiers::expand_size_to_fit,true);
		_ptr->positioning_rectangle.set_size(mm::vec2{8,9}*5.0);
		auto siz = _ptr->positioning_rectangle.get_size();
		printf("size %f %f\n",siz.x,siz.y);
		text_list.push_element(_ptr);
		*/
	}
	void UI::impl_render()
	{
		chat_event::interface.dispatch_events();
	}
	void UI::_clamp_scroll()
	{
		text_list.set_scroll(std::clamp(text_list.get_scroll(), std::min(-text_list.total_list_y_size+text_list.positioning_rectangle.get_size().y,0.0),0.0));
	}
	void UI::impl_dispatch_event(input::computer::mouse_motion::event_data_t motion)
	{
		text_list.set_scroll(text_list.get_scroll()+15*motion.delta_wheel_y);
		_clamp_scroll();
	}
	void UI::impl_dispatch_event(ui::binding_for_button<keybinds::start_chat> button)
	{
		if (button.is_down)
		{
			set_focused_element(input);
		}
	}
	void physics_done_callback()
	{
		/*chat_event::netif->dispatch_events();
		chat_event::netif->send_events();*/
	}
	void init()
	{
		//chat_event::netif = netplay::network_interface<chat_event::event_configuration_types>(chat_event::manager);
		//TODO phys
		//add_tick_finished_callback(physics_done_callback);
	}
}
