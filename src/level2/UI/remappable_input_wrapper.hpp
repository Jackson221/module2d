/*!
 * Converts remappable input from the input module to be raw-struct form for the UI system
 */
#pragma once
#include "input/remappable.hpp"
#include "event/event.hpp"
#include "variadic_util/variadic_util.hpp"

namespace ui
{
	template<auto axis_type>
	struct binding_for_axis
	{
		float value;
	};
	template<auto button_type>
	struct binding_for_button
	{
		bool is_down;
	};
	template<typename types_enum, typename remappable_t>
	struct event_processor
	{
		static constexpr size_t types_size = static_cast<size_t>(types_enum::SIZE);
		std::array<event::direct_interface<typename remappable_t::event_configuration_types>, types_size> interfaces;
		event_processor(remappable_t & remappable) : 
			interfaces(variadic_util::for_each_iterator_make_array<types_size>([&remappable]<size_t i>()
			{
				auto& manager = remappable.managers.managers[i];
				return event::direct_interface<typename remappable_t::event_configuration_types>{manager};
			}))
		{
		}
		void give_all_events_to(auto& recipient)
		{
			variadic_util::for_each_iterator<static_cast<size_t>(types_enum::SIZE)>([this, &recipient]<size_t i>()
			{
				for (auto data_orig : interfaces[i].fetch_events())
				{
					auto data = static_cast<typename remappable_t::val_t>(data_orig);
					recipient.dispatch_event(binding_struct<static_cast<types_enum>(i), typename remappable_t::val_t>{data});
				}
			});
		}
		private:
			template<auto enum_val, typename data_t>
			using binding_struct = std::conditional_t<
				/*if  */ std::is_same_v<data_t, bool>,
				/*then*/ binding_for_button<enum_val>,
				/*else*/ binding_for_axis<enum_val>
			>;
	};

}
