# Why does Module2D handle events in such a strange manner?

Module2D is built with latency reduction in mind everywhere. This is because, despite many programmers' easily-disproven views, latency is extremely important for the experince and even latency less than 10 miliseconds matters.

With that said, it wouldn't make sense to design Module2D's input in the same manner as typical applications. For one thing, the way that SDL2's event polling works would add a frames of latency right off the bat if we only polled at the start of the frame. This means that, in the worst case, events might be generated a few microseconds after the application polled. That would already be nearly 1 frame of latency off the bat (and there is really no way to avoid that latency). Alright, so then at the next frame these events finally get to be processed -- still at only 1 frame of latency -- but now they have to be sent to the physics engine for them to actually do anything. If the physics engine also happened to have just polled for its events, then there's another 1 frame generated there. So, now we're at 2 frames of latency by polling before-rendering.

Module2D polls every 1/8th of a milisecond in a dedicated event thread. So they will be delivered to the physics engine as fast as possible.

(of course, this could be sort-of eliminated with rendering-side prediction, especially if rendering was delayed until the GPU barely had enough time to make VBLANK)
    
SDL2 wasn't particularly designed with this setup in mind, in fact it's not possible to do while using its built-in renderer, but it works just fine because Module2D uses Vulkan. I do wish that SDL2 had used event callbacks that would run in seperate threads, on-button-press inside of before-rendering.

## What about the event processing in the UI subsystem?

You may have also noticed that there is a global UI mutex seperate to the normal rendering mutex. That's because UI elements actually process their events in Module2D's event polling thread, and thus are mutually exclusive to Module2D rendering things.

So while Module2D can't process events and render at the same time, it still is able to poll events at a much higher rate than it renders. And since event processing happens practically instantly, there is no real delay to rendering.

It also means that you should not do anything intensive in event processing.
