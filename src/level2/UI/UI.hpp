/*!
 * @file
 * @author Jackson McNeill
 * 
 * THE ORDER IN WHICH THIS HEADER FILE IS INCLUDED MATTERS GREATLY; ALL EVENT HEADERS SHOULD BE INCLUDED FIRST!
 *
 * A simple object-oriented UI system. This is intended to remain as minimal as possible, while providing necasary features for game HUDs and UIs. 
 * Many objects do not support rotation; some do (usually if used in HUDs). This helps to keep this system minimal.
 *
 * Top level elements might not be rendered every frame--optimization is allowed to be performed.
 *
 * Copyright 2017-2019 Jackson Reed McNeill. Licensed under the GNU AGPL version 3 or any later version.
 */
#pragma once



#include <string>
#include <functional>
#include <memory>
#include <optional>
#include <map>
#include <vector>
#include <set>
#include <queue>
#include <mutex>
#include <algorithm>


#include "UI/font.h"
#include "UI/color.hpp"
#include "UI/shapes.h"
#include "UI/remappable_input_wrapper.hpp"
#include "render/state.hpp"
#include "input/computer.hpp"
#include "variadic_util/variadic_util.hpp"
#include "mathematics/primitives.hpp"


namespace ui
{
	/*!
	 * Determines the position and size of an element. 
	 *
	 * Allows the user to set flags to modify the position and size, like centering or expanding size to fit.
	 * It also allows the user to set a function to determine the position and size. Furthermore, it allows 
	 * you to create dependant rectangles that auto-update when your rectangle updates.
	 *
	 * In retrospect, this is probably the best idea to come out of the UI system. It has made my life much easier.
	 */
	using calculator_function = std::function<mm::vec2()>;

	using update_callback_t = std::function<void()>;
	class positioning_rectangle_t
	{
		public:
			enum class modifiers 
			{
				NONE,
				center_x,
				center_y,
				expand_size_to_fit,
				expand_size_to_fit_with_regular_size_as_padding,
				size_x_is_min_size,
				size_y_is_min_size,
				align_right,
				align_button,
				SIZE,
			};

			positioning_rectangle_t();
			positioning_rectangle_t(mm::vec2 position, mm::vec2 size);
			
			mm::vec2 get_position();
			mm::vec2 get_size();
			inline mm::vec2 get_top_left()
			{
				return get_position();
			}
			inline mm::vec2 get_top_right()
			{
				return get_position() + mm::vec2{get_size().x,0};
			}
			inline mm::vec2 get_bottom_left()
			{
				return get_position() + mm::vec2{0,get_size().y};
			}
			inline mm::vec2 get_bottom_right()
			{
				return get_position() + get_size();
			}

			void set_position(mm::vec2 position);
			void set_size(mm::vec2 size);
			void set_max_size(mm::vec2 new_max_size);

			void set_modifier(modifiers modifier, bool value);
			bool get_modifier_value(modifiers modifier);

			void set_origin_position_calculator(calculator_function funct);
			void set_origin_size_calculator(calculator_function funct);
			void set_update_callback(update_callback_t funct);
			inline void set_update_callback_and_run_it_now(update_callback_t funct)
			{
				set_update_callback(funct);
				funct();
			}

			void set_origin_position_and_size_to_be_always_same_as(positioning_rectangle_t& other);

			void recalculate();

			mm::vec2 get_origin_position();
			mm::vec2 get_origin_size();
			inline mm::vec2 get_expanded_size() { return expanded_size; }

			void add_child_rectangle(positioning_rectangle_t& rectangle);

			//Only the owning object should touch these
			void set_expanded_size(mm::vec2 size);

			void print_debug_info();

			void render_debug();

			inline void remove_child(positioning_rectangle_t& child)
			{
				auto it = std::find(child_rectangles.begin(), child_rectangles.end(), &child);
				child_rectangles.erase(it);
			}
		private:
			mm::vec2 origin_position = {0,0};
			mm::vec2 origin_size = {0,0};
			mm::vec2 max_size;

			bool modifiers_array[static_cast<int>(modifiers::SIZE)] = {0,}; //initialize array to 0
			std::optional<calculator_function> origin_position_calculator = {};
			std::optional<calculator_function> origin_size_calculator = {};
			update_callback_t update_callback = [](){};

			std::vector<positioning_rectangle_t*> child_rectangles;

			mm::vec2 expanded_size = {0,0};

			mm::vec2 get_position_impl();
			mm::vec2 get_size_impl();
	};
	class element;

	struct gc_state
	{
		std::mutex gc_mutex;
		std::vector<std::function<void()>> gc_list;
	};
	struct global_lock
	{
		std::mutex lock;	
	};
	struct state
	{
		render::state* rstate;
		gc_state* gcstate;
		global_lock* globallock;
	};
	//manages the memory of ui::state, is used in engine.cpp
	struct state_container
	{
		state contained_state;
		//inlined for code readability purposes
		inline state_container(render::state* rstate)
		{
			contained_state.rstate = rstate;
			contained_state.gcstate = new gc_state{};
			contained_state.globallock = new global_lock{};
		}
		inline ~state_container()
		{
			delete contained_state.gcstate;
			delete contained_state.globallock;
		}
	};

	struct on_hover {};
	struct on_unhover {};
	
	struct on_focus {};
	struct on_unfocus {};

	//if an event (like a mouse click) requires its coordinates translated into the coordinate space of the element
	inline void translate_coords(element& child, input::computer::mouse_motion::event_data_t& motion);
	inline void translate_coords(element& child, input::computer::mouse_button::event_data_t& button);

	//all of these below will receive pre-translated events
	inline bool may_invoke_focus_change(input::computer::mouse_button::event_data_t button);
	inline bool focus_change(element& child, input::computer::mouse_button::event_data_t button);
	inline bool extra_requirements(element& child, input::computer::mouse_motion::event_data_t motion);



	/* The following is a language exploit.
	 * I am using partial template deduction specialization in order to mark event types which require focus.
	 *
	 * So, basically, to mark an event as requiring focus, just specialize requires_focus for a template argument
	 * containing that template. See the two examples (doesn't have to be a template class you're marking, obviously.)
	 */
	template<typename T>
	struct requires_focus
	{
		//all partial template specializations shall be empty and thus can be distinguished by their lack of a canary.
		static constexpr bool canary = true;
	};
	template<auto val>
	struct requires_focus<binding_for_button<val>>
	{
	};
	template<auto val>
	struct requires_focus<binding_for_axis<val>>
	{
	};
	template<typename T>
	concept _helper_does_not_require_focus = requires_focus<T>::canary;
	template<typename T>
	concept requires_focus_event = !_helper_does_not_require_focus<T>;


	template<typename T>
	concept translatable_event = requires(element& child, T& event)
	{
		{ translate_coords(child, event) };
	};
	template<typename T>
	concept focus_change_event = requires(element& child, T event)
	{
		{ may_invoke_focus_change(event) } -> std::same_as<bool>;
		{ focus_change(child, event) } -> std::same_as<bool>;
	};
	template<typename T>
	concept extra_requirements_event = requires(element& child, T event)
	{
		{ extra_requirements(child, event) } -> std::same_as<bool>;
	};
	/*
	//Current use for this is to send hover/unhover events 
	//inline bool post_event_processing(element& child, input::computer::mouse_motion::event_data_t motion);
	template<typename T>
	concept post_event_processing_event = requires(element& child, T event)
	{
		{ post_event_processing(child, event) } -> std::same_as<bool>;
	};*/
	template<typename event_data_t>
	class has_event_and_can_delete_parent_from_it
	{
		protected:
			//If you delete your parent in this event, then you should return its pointer.
			//Otherwise, if you delete a non-parent element or no elements at all, return nullptr.
			virtual element* impl_dispatch_event_maybe_delete_parent(event_data_t event) = 0;

			friend class element;
	};
	template<typename event_data_t>
	class has_event : public has_event_and_can_delete_parent_from_it<event_data_t>
	{
		protected:
			virtual void impl_dispatch_event(event_data_t event) = 0;
			inline element* impl_dispatch_event_maybe_delete_parent(event_data_t event) override final
			{
				impl_dispatch_event(event);
				return nullptr;
			}

			friend class element;
	};

	/*!
	 * An element which is intended to be added to a element
	 */
	class element
	{
		virtual inline void impl_render() {}

		//Called when the focus of this element's children changes.
		//new_focused_child is a reference and you are able to change it.
		virtual inline void impl_child_changed_focus(size_t last_focused_child, size_t& new_focused_child) {}

		//If this element also inherits from has_event<event_t>, then give it the event.
		template<typename event_t>
		element* give_event_to_self(event_t event)
		{
			auto* maybe_null_ptr_to_has_event_interf = dynamic_cast<has_event_and_can_delete_parent_from_it<std::decay_t<event_t>>*>(this);
			if (maybe_null_ptr_to_has_event_interf != nullptr)
			{
				return maybe_null_ptr_to_has_event_interf->impl_dispatch_event_maybe_delete_parent(event);
			}
			return nullptr;
		}

		//Unhover myself and my children
		inline void intern_element_unhover()
		{
			mouse_is_hovering_me = false;
			for (auto child : children)
			{
				child->intern_element_unhover();
			}
		}
		//Focus events are special in that they don't recurse for all children -- only the focused child
		//Since we need to give a focus change to both ourselves and a focused child, it's broken off into a 
		//function.
		inline void intern_element_give_focus_event(bool is_focused_now)
		{
			if (is_focused_now)
			{
				give_event_to_self(on_focus{});
			}
			else
			{
				give_event_to_self(on_unfocus{});
			}
		}
		//Focus change myself and my originally focused child.
		inline void intern_element_focus_change(bool is_focused_now)
		{
			in_focus = is_focused_now;
			//See comment on this function about focus events.
			intern_element_give_focus_event(is_focused_now);
				
			if (focused_child < children.size())
			{
				children[focused_child]->intern_element_focus_change(is_focused_now);
			}
		}

		public:
			state uistate;

			virtual inline void render()
			{
				mm::vec2 top_left = positioning_rectangle.get_position();
				mm::vec2 bottom_right = top_left + positioning_rectangle.get_size();

				mm::vec2 boundry_top_left = painting_boundries_rectangle.get_position();
				mm::vec2 boundry_bottom_right = boundry_top_left + painting_boundries_rectangle.get_size();

				if (top_left <= boundry_bottom_right && bottom_right >= boundry_top_left)
				{
					impl_render();
				}

				for (auto child : children)
				{
					child->render();
				}
			}
			virtual void render(mm::vec2 screen_limit_lower, mm::vec2 screen_limit_higher)
			{
				throw std::runtime_error("This event does not support rendering with screen limits");
			}

			inline void defocus_any_element()
			{
				if (focused_child < children.size())
				{
					children[focused_child]->intern_element_focus_change(false);
				}
				focused_child = NO_FOCUSED_CHILD_VAL;
			}
			inline void set_focused_element(size_t new_focused_child)
			{
				if (focused_child == new_focused_child)
					return;
				if (focused_child < children.size())
				{
					children[focused_child]->intern_element_focus_change(false);
					impl_child_changed_focus(focused_child, new_focused_child); //i is passed by ref and may be changed but that's okay since this is the end of the loop
				}
				focused_child = new_focused_child;
				if (in_focus)
				{
					children[new_focused_child]->intern_element_focus_change(true);
				}
			}
			inline void set_focused_element(element* child)
			{
				set_focused_element(std::distance(children.begin(), find_child(child)));
			}
			inline void set_focused_element(element& child)
			{
				return set_focused_element(&child);
			}


			//Used when the called event processor may delete this element -- we then need to return.
			//So the calling of this macro indicates a potential self-deletion point.
			//
			//This is put in place to allow us to - delete the parent - of the event caller!
			//Yes, that means part of processing this event could delete a parent object of the element
			//that processes the event.
			//
			//Think of a popup UI box's OK button -- clicking it deletes the popup box. So we return up
			//the callstack until `this == (what was returned)` and then return nullptr just once more. 
			//
			//The main question is, how do UI elements create more events while they're processing an event?
			#define _return_if_deleted(instatement)\
			element* deleted = instatement;\
			if (deleted != nullptr)\
			{   /* break the chain if they deleted us.*/\
				return (deleted == this)? nullptr : deleted;\
			}
			//We use the nullptr property, so this is an optional pointer.
			inline element* dispatch_event(auto this_event)
			{
				//hovering stuff -- special logic to emit mouse hovering events
				if constexpr(std::is_same_v<decltype(this_event), input::computer::mouse_motion::event_data_t>)
				{
					bool is_hovering_now = is_in_bounds({static_cast<double>(this_event.x), static_cast<double>(this_event.y)});
					bool was_hovering_before = mouse_is_hovering_me;

					//Hover events are special in that they do not recurse to our children (since we may not actually be hovering over them)
					//They will only get the hover event if the cursor actually hovers over them as well.
					if (is_hovering_now && !was_hovering_before)
					{
						mouse_is_hovering_me = true;
						_return_if_deleted(give_event_to_self(on_hover{}));
					}
					else if (was_hovering_before && !is_hovering_now)
					{
						intern_element_unhover();
						_return_if_deleted(give_event_to_self(on_unhover{}));
					}
				}
				bool do_dispatch = false;
				if constexpr(extra_requirements_event<decltype(this_event)>)
				{
					do_dispatch = extra_requirements(*this, this_event);
				}
				//first give us the event, then give it to our children
				if (in_focus || do_dispatch)
				{
					auto translated_event = this_event;
					if constexpr(translatable_event<decltype(this_event)>)
					{
						translate_coords(*this, translated_event);
					}
					_return_if_deleted(give_event_to_self(translated_event));
				}
				if constexpr(focus_change_event<decltype(this_event)>)
				{
					//while this may be a focus changing event sometimes, there may be additional qualifications (e.x. for mouse button events, the mouse button must be down)
					if (may_invoke_focus_change(this_event))
					{
						//try to find a new child to focus.
						for (size_t i = children.size(); i-- != 0;)
						{
							if (focus_change(*children[i], this_event))
							{
								//printf("foc %lu(%s), i am %s\n",i,typeid(*children[i]).name(), typeid(*this).name());
								set_focused_element(i);
								//give them the event
								_return_if_deleted(children[focused_child]->dispatch_event(this_event));
								break;
							}
						}
					} //if it doesn't meet extra requirements, give it to the focused child
					else if (focused_child < children.size())
					{
						_return_if_deleted(children[focused_child]->dispatch_event(this_event));
					}
				}
				else if constexpr(requires_focus_event<decltype(this_event)>)
				{
					if (focused_child < children.size())
					{
						_return_if_deleted(children[focused_child]->dispatch_event(this_event));
					}
				}
				else
				{
					for (size_t i = children.size(); i-- != 0;)
					{
						_return_if_deleted(children[i]->dispatch_event(this_event));
					}
				}
				return nullptr;
			}
#undef _return_if_deleted
			
			inline element(state in_uistate) :
				uistate(in_uistate)
			{
				painting_boundries_rectangle.set_origin_position_and_size_to_be_always_same_as(positioning_rectangle);
			}
			inline virtual ~element() {};
			positioning_rectangle_t positioning_rectangle;
			
			//Defines, with coordinates (in absolute screen coordinates), the area in which the object is permitted to paint in.
			//By default, this is tied to being the exact same value as positioning_rectangle
			positioning_rectangle_t painting_boundries_rectangle;

			inline mm::vec2 get_relative_painting_boundries_position()
			{
				return painting_boundries_rectangle.get_position() - positioning_rectangle.get_position();
			}
			inline mm::vec2 get_position_after_confining_to_painting_boundries()
			{
				mm::vec2 position = painting_boundries_rectangle.get_position();
				position.x = std::max(position.x, positioning_rectangle.get_position().x);
				position.y = std::max(position.y, positioning_rectangle.get_position().y);
				return position;
			}
			inline mm::vec2 get_size_after_confining_to_painting_boundries()
			{
				//bounding size can also be thought of as the maximum bottom right corner, in relative coordinate space
				mm::vec2 relative_position_increase = get_relative_painting_boundries_position();
				mm::vec2 bounding_size = painting_boundries_rectangle.get_size() + relative_position_increase;
				bounding_size.x = std::min(positioning_rectangle.get_size().x, bounding_size.x);
				bounding_size.x -= std::max(0.0,relative_position_increase.x);
				bounding_size.y = std::min(positioning_rectangle.get_size().y, bounding_size.y);
				bounding_size.y -= std::max(0.0,relative_position_increase.y);
				
				return bounding_size;
			}
			inline mm::rect get_selection_rectangle_after_confining_to_painting_boundries(mm::rect this_selection)
			{
				//The coeffecient that the bounding size has been scaled
				mm::vec2 coeffecient_scaling = get_size_after_confining_to_painting_boundries() / positioning_rectangle.get_size();
				//The coeffecient of my size to the size of the sprite
				mm::vec2 coeffecient_size = mm::vec2{this_selection.w, this_selection.h} / positioning_rectangle.get_size();

				//Adjust the sprite selection accordingly. This basically is just adjusting it the same percentage that we did the primary position/size
				this_selection.x += std::max(0.0, get_relative_painting_boundries_position().x * coeffecient_size.x);
				this_selection.y += std::max(0.0, get_relative_painting_boundries_position().y * coeffecient_size.y);
				this_selection.w *= coeffecient_scaling.x;
				this_selection.h *= coeffecient_scaling.y;

				return this_selection;
			}

			std::vector<element*> children;

			inline void add_child(element* child)
			{
				children.push_back(child);
				child->positioning_rectangle.set_origin_position_calculator([this](){ return positioning_rectangle.get_position();});
				positioning_rectangle.add_child_rectangle(child->positioning_rectangle);

				child->painting_boundries_rectangle.set_origin_position_and_size_to_be_always_same_as(painting_boundries_rectangle);
			}
			inline void add_child(element& child)
			{
				add_child(&child);
			}
			inline void add_child_without_modfying_positioning_rectangles(element* child)
			{
				children.push_back(child);
			}
			inline void add_child_without_modfying_positioning_rectangles(element& child)
			{
				add_child_without_modfying_positioning_rectangles(&child);
			}
			inline std::vector<element*>::iterator find_child(element* child)
			{
				//Due to the small size of the children vector, and the fact that it may change a lot, 
				//linear search was chosen here.
				return std::find(children.begin(), children.end(), child);
			}
			inline auto find_child(element& child)
			{
				return find_child(&child);
			}
			inline std::vector<element*>::iterator remove_child(size_t child_id)
			{
				return children.erase(children.begin() + child_id);
			}
			inline void remove_child(element& child)
			{
				children.erase(find_child(child));
			}
			inline void remove_child(element* child)
			{
				children.erase(find_child(child));
			}
			
			
			static constexpr size_t NO_FOCUSED_CHILD_VAL = std::numeric_limits<size_t>::max();
			size_t focused_child = NO_FOCUSED_CHILD_VAL;

			bool mouse_is_hovering_me = false;
			bool in_focus = false;
			
			inline virtual bool is_in_bounds(mm::vec2 mouse)
			{
				auto top_left = positioning_rectangle.get_position().max_between(painting_boundries_rectangle.get_position());
				auto bottom_right = (positioning_rectangle.get_position()  + positioning_rectangle.get_size()).min_between(painting_boundries_rectangle.get_position() + painting_boundries_rectangle.get_size());
				if (mouse >= top_left && mouse < bottom_right)
				{
					return true;
				}
				for (element* child : children)
				{
					if (child->is_in_bounds(mouse))
					{
						return true;
					}
				}
				return false;
			}
			element(element const &) = delete;
			element(element &&) = delete;
			element& operator=(element const &) = delete;
			element& operator=(element &&) = delete;
	};
	
	

	inline void _impl_translate_coords(element& child, auto& event)
	{
		event.x -= static_cast<int32_t>(child.positioning_rectangle.get_position().x);
		event.y -= static_cast<int32_t>(child.positioning_rectangle.get_position().y);
	}
	inline void translate_coords(element& child, input::computer::mouse_motion::event_data_t& motion)
	{
		return _impl_translate_coords(child, motion);
	}
	inline void translate_coords(element& child, input::computer::mouse_button::event_data_t& button)
	{
		return _impl_translate_coords(child, button);
	}

	//The presence of this function (for any type) categorizes the event as an event which may invoke focus change,
	//and the defintion specifies when it may.
	//However, it is an optimization function and does not guarentee a given event will cause it. Just when it should be checked at all.
	inline bool may_invoke_focus_change(input::computer::mouse_button::event_data_t button)
	{
		using input::computer::mouse_button::button_t;
		return (button.button == button_t::left || button.button == button_t::right) && button.is_down;
	}
	//This function specifies when a given element will be focused by an event.
	inline bool focus_change(element& child, input::computer::mouse_button::event_data_t button)
	{
		return child.is_in_bounds({static_cast<double>(button.x), static_cast<double>(button.y)});
	}
	//The presence of this function (for any type) categorizes the event as one which must meet a requirement to be sent to an element.
	//The defintion specifies that requirement.
	//Here, we're making sure only mouse motion events which are within boundries will be sent.
	inline bool extra_requirements(element& child, input::computer::mouse_motion::event_data_t motion)
	{
		return child.is_in_bounds({static_cast<double>(motion.x), static_cast<double>(motion.y)});
	}
	
	inline bool check_if_focused_for_box(int mousex, int mousey, mm::vec2 position, mm::vec2 size) //inline convinience function for checking wheather the mouse clicked a box 
	{
		return (mousex >= position.x && mousex <= position.x+size.x && mousey >= position.y && mousey <= position.y+size.y);
	}


	//////////////////////////////////////////////////
	//Section: Styling structs
	//////////////////////////////////////////////////

	struct font_style_t
	{
		ui::color color;
		GE_Font font;
		font_style_t(ui::color color, std::string font_name, unsigned int size);
		inline font_style_t(ui::color color, GE_Font font) :
			color(color),
			font(font)
		{
		}
		inline font_style_t& operator=(font_style_t const & other)
		{
			color = other.color;
			font = other.font;
			return *this;
		}
		inline font_style_t& operator=(font_style_t && other)
		{
			color = std::move(other.color);
			font = std::move(other.font);
			return *this;
		}
		inline font_style_t()
		{
		}
		font_style_t(font_style_t const &) = default;
		font_style_t(font_style_t &&) = default;
		double get_font_height() const;
	};
	struct text_input_style_t
	{
		ui::color cursor_color;
		ui::color text_color;
		ui::color color_focused;
		ui::color color_unfocused;
		font_style_t font;
	};
	struct sprite_style_t
	{
		std::string sprite_name;
		color tint;
		mm::rect selection;
	};
	class rectangle;
	class focus_color_change_rectangle;
	struct button_style_t
	{
		color background;
		color pressed;
		color highlighted;

		inline std::tuple<rectangle*, rectangle*, rectangle*> new_shapes(state uistate);
	};
	struct button_style_with_focus_color_change_t
	{
		color background_focused;
		color background_unfocused;
		color pressed;
		color highlighted;

		inline std::tuple<focus_color_change_rectangle*, rectangle*, rectangle*> new_shapes(state uistate);
	};
	class sprite;
	struct button_style_sprite_t
	{
		sprite_style_t background_sprite_style_t;
		sprite_style_t highlight_sprite_style_t;
		sprite_style_t pressed_sprite_style_t;
		/*color highlighted_tint;
		color pressed_tint;*/

		inline std::tuple<sprite*, sprite*, sprite*> new_shapes(state uistate);
	};

	struct window_title_style_t
	{

		font_style_t font;
		double textOffset;
		button_style_t close_button;
		font_style_t close_buttonFont;
		double close_buttonPadding;
		double buttonDistanceFromRight;
		color background;
		double height;
		bool centerTitleText;

	};


	struct window_style_t
	{
		window_title_style_t title_style;
		color background;
		double border_size;
		color border_color;
		color border_color_unfocused;
		
		double extra_dragging = 10.0;
	};
	struct style
	{
		font_style_t standardFont;
		font_style_t buttonFont;

		window_style_t window_style;
	};
	struct popup_ok_style_t
	{
		style window;
		font_style_t buttonFont;
		button_style_t buttons;
		mm::vec2 buttonSize;
		bool useSizeAsPadding;
		double buttonSpacing;
		double buttonDistanceFromBottom;
	};
	struct spacer_style_t
	{
		ui::color color;
		double size;
		double spaceLeftAtEachEdge;
		double extraSpaceBetween;
	};
	struct menu_list_style
	{
		font_style_t font;
		color background_focused;
		color background_unfocused;
		color highlight;

		color spacer_color;
		double spacer_size;
		double coeffecient_spacer_width; //coeffecient of spacer width vs drop-down width
	};


	class text : public element
	{
		public:
			struct failed_to_raster_text : std::exception
			{
				inline const char* what() const noexcept
				{
					return "Failed to raster text";
				}
			};

			text(state uistate, std::string text, color color,GE_Font font);
			inline text(state uistate, std::string text, font_style_t style) : 
				ui::text(uistate,text,style.color,style.font)
			{
			}
			void impl_render() override;
			virtual void hud_render(mm::vec2r parent_position);

			/*!
			 * Note that this will only render fonts (which is somewhat expensive) if the text has actually changed.
			 */
			void set_text(std::string text);
			std::string get_text();
			 
			void set_scroll(double ammount);
			double get_scroll();

			double static get_text_length(char const* const in_text, GE_Font font);
			double static get_text_height(char const* const in_text, GE_Font font);

			double static get_text_length(auto const & in_text, GE_Font font)
			{
				return get_text_length(in_text.c_str(),font);
			}
			double static get_text_height(auto const & in_text, GE_Font font)
			{
				return get_text_height(in_text.c_str(),font);
			}
		private:

			//The following bits are for splitting text into multiple lines
			//
			//While the text class itself does not support multiple lines, these functions help
			//other classes that do.

			template<typename string_type, typename iterator>
			static void for_each_word(iterator begin, iterator end, auto & func)
			{
				if (std::distance(begin,end) <= 2)
				{
					func(string_type(begin,end));
					return;
				}
				auto last_spot = begin;
				for (auto it = begin+1; it != end-1; it++)
				{
					if (*it == ' ')
					{
						func(string_type(last_spot, it+1));
						last_spot = it+1;
					}
				}
				func(string_type(last_spot,end));
			}
			template<typename string_type, typename is_text_too_big_t>
			struct split_string_processor
			{
				is_text_too_big_t is_text_too_big;
				std::vector<string_type> & result_dump;

				string_type next_string;

				inline void _dump_str()
				{
					//if this single word is still too big for the line, then run this same function but for characters instead of words
					//(this won't ever be true if this is actually being ran in single-character mode)
					if (next_string.length() > 1 && is_text_too_big(next_string))
					{
						split_string_processor<string_type,is_text_too_big_t> single_word_processor = {is_text_too_big,result_dump,{}};

						//for_each will copy whatever you put into it (which is really annoying for us because we need to call .finish()) so give it a lambda to copy that
						//will actually call a reference of our single word processor.
						std::for_each(next_string.begin(),next_string.end(),[&](auto in){single_word_processor(in);});

						//we've processed what was in the next_string so clear it
						next_string.clear();
						//now continue to process the unfinished line
						operator()(single_word_processor.next_string);
					}
					else
					{
						result_dump.push_back(std::move(next_string));
						next_string.clear();
					}
				}
				inline void operator()(auto this_word)
				{
					auto measured_string = next_string + this_word;
					if (measured_string.length() > 1 && is_text_too_big(measured_string)) 
					{
						_dump_str();

						next_string += this_word;
					}
					else
					{
						next_string = measured_string;
					}
				}
				inline void finish()
				{
					_dump_str();
					if(!next_string.empty())
					{
						result_dump.push_back(std::move(next_string));
					}
				}
			};
		public:
			template<typename string_type, typename is_text_too_big_t>
			inline static std::vector<string_type> const split_text_into_lines(string_type in_text, is_text_too_big_t is_text_too_big)
			{
				std::vector<string_type> result;

				split_string_processor<string_type,is_text_too_big_t> whole_word_processor = {is_text_too_big,result,{}};
				for_each_word<string_type>(in_text.begin(), in_text.end(), whole_word_processor);
				whole_word_processor.finish();
				
				return result;
			}


		private:
			mm::vec2 text_raster_size;
			GE_Font font;
			ui::color color;
			std::optional<render::backend_sprite> sprite;
			std::string current_text;
			double scroll_position;
	};

	struct text_cursor
	{
		size_t cursor = 0;
		std::string& text;
		std::string get_text_before_cursor();
		std::string get_text_before_cursor_and_at_cursor();
		std::string get_text_after_cursor();
		std::string get_text_after_cursor_and_at_cursor();
		std::string get_text_at_cursor();
		void set_cursor(size_t pos);
	};
	/*!
	 * like RectangleShape, but with a static position
	 */
	class rectangle : public element
	{
		public:
			rectangle(state uistate, color color);
			void impl_render() override;
		private:
			GE_RectangleShape rectangle_shape;
	};

	struct window_on_focus {};
	struct window_on_unfocus {};

	/*!
	 * like rectangle, but with a color change on window focus events
	 */
	class focus_color_change_rectangle : public element,
		public has_event<window_on_focus>,
		public has_event<window_on_unfocus>
	{
		public:
			focus_color_change_rectangle(state uistate, color focus_color, color unfocused_color);

			void impl_dispatch_event(window_on_focus) override;
			void impl_dispatch_event(window_on_unfocus) override;
		private:
			rectangle focus;
			rectangle unfocused;
	};

	/*!
	 * like Rectangle, but with a (tinted) sprite instead
	 */
	class sprite : public element
	{
		public:
			sprite(state uistate, sprite_style_t style);
			void impl_render() override;
		private:
			render::backend_sprite* real_sprite;
			color tint;
			mm::rect sprite_selection;
	};

	enum text_input_bindings
	{
		left,
		right,
		backspace,
		del,
		home,
		end,
		finished_editing,
		SIZE
	};

	/*!
	 * The reason why this input system is so great is cause I can define a deriving class like a Torque package!
	 *     Only, it's only organized into a mod folder, and probably can't be unloaded. But basically, you can derive these
	 *     and then substitute them.
	 *
	 *     The only thing that would make this more modable is if we then had auto-generated factories that could be hooked.
	 *
	 *     But basically, you can re-override all of the events when you inheret from them. You could even this still call its parent.
	 *     		Really, I think we need an "!important" which overrides the last element's and blocks it.
	 *     			Like, I'm saying we should have dynamic dispatch on the constructor of "text_input" so that we can then change the class at runtime.
	 *
	 *     			That way we can mod it in C++.
	 *
	 *     			Because while we could just have global callbacks to add children to all text_input's, children would need to have a way of subsuming their
	 *     			parent's behavior in the event and rendering system. And any other system in the system. 
	 *
	 *     			If the text_input then spawned children (in its constructor), by std::applying its constructor's arguments tuple to a list of mod functions to make
	 *     			children, then mods could add children and even subsume behaviors.
	 *
	 *     			The last added mod would then win in each subsumed function.
	 *
	 *
	 * 				Like, if a child could subsume its parent as if derived from it and reallocating the class as it's stored to be a different thing. And this is done
	 * 				by actually giving events to the child nodes first, in addition order. So, those children event handlers might return as if they've deleted their
	 * 				parent, but in reality they've been given access to the instance of shared_ptr<ui::element> and have reconstructed it by transforming the original
	 * 				T, like text_input, to their modded T.
	 *
	 * 				That way, it could be merely defined that a UI element run, as the last part of their constructor, a list of functions which add children elements
	 * 				and are created by the public mod API surface.
	 *
	 * 				This is accessed just like the OS interface. So, the element is constructed in two steps, and one
	 * 				of them is running a generated static fluid array of functions who return lists of shared_ptr<ui::element> to be added to the child list.
	 *
	 * 				And potentially then these events are hitting triggers which change what the class actually is.
	 *
	 */
	class text_input : public element,
		public has_event<input::computer::mouse_motion::event_data_t>,
		public has_event<input::computer::mouse_button::event_data_t>,
		public has_event<input::computer::text_input::event_data_t>,
		public has_event<input::computer::clipboard::event_data_t>,
		public has_event<binding_for_button<text_input_bindings::left>>,
		public has_event<binding_for_button<text_input_bindings::right>>,
		public has_event<binding_for_button<text_input_bindings::backspace>>,
		public has_event<binding_for_button<text_input_bindings::del>>,
		public has_event<binding_for_button<text_input_bindings::home>>,
		public has_event<binding_for_button<text_input_bindings::end>>,
		public has_event<binding_for_button<text_input_bindings::finished_editing>>,
		public has_event<on_unfocus>,
		public has_event<on_focus>
	{
		public:
			text_input(state uistate, color cursor_color, color text_color, color color_focused, color color_unfocused, GE_Font font);
			text_input(state uistate, text_input_style_t style);

			//This is a fundamentally incorrect interface (exposed setters/getters).
			//This is because we're defining multiple data structures here, we shouldn't
			//have combined a thing that manages rasterizing text (the `font` value)
			//and UI text. This should be a bare exposed 'rasterized_text` class
			//whose operator= automatically re-rasterizes, and dereference 
			//operator returns std::string.
			//
			//And if you decided you can't expose your internal font rasterization,
			//then there should be another class which exposes the operator= and dereference
			//operators only.
			void set_text(std::string text);
			std::string get_text();	

			//Called when finished editing
			std::optional<std::function<void()>> callback;

			void impl_dispatch_event(input::computer::mouse_motion::event_data_t motion) override;
			void impl_dispatch_event(input::computer::mouse_button::event_data_t button) override;
			void impl_dispatch_event(input::computer::text_input::event_data_t text) override;
			void impl_dispatch_event(input::computer::clipboard::event_data_t text) override;
			void impl_dispatch_event(binding_for_button<text_input_bindings::left>) override;
			void impl_dispatch_event(binding_for_button<text_input_bindings::right>) override;
			void impl_dispatch_event(binding_for_button<text_input_bindings::backspace>) override;
			void impl_dispatch_event(binding_for_button<text_input_bindings::del>) override;
			void impl_dispatch_event(binding_for_button<text_input_bindings::home>) override;
			void impl_dispatch_event(binding_for_button<text_input_bindings::end>) override;
			void impl_dispatch_event(binding_for_button<text_input_bindings::finished_editing>) override;
			void impl_dispatch_event(on_unfocus) override;
			void impl_dispatch_event(on_focus) override;

			void set_cursor_position(size_t position);
		private:
			GE_Font font;
			focus_color_change_rectangle background;
			text ui_text;

			std::string str_text;

			rectangle cursor_rect;
			text_cursor cursor;

			double get_cursor_rect_position();
			void scroll_text();

			void reposition_text_cursor_to_mouse_cursor(int rel_mousex);

			bool lmouse_held = false;
	};

	template<typename listed_type>
	class element_list : public element
	{
		public:
			element_list(state uistate) :
				element(uistate)
			{
			}
			~element_list()
			{
				for (auto& pair : elements_in_order)
				{
					delete pair.second;
				}
			}
			unsigned int push_element(listed_type* new_element)
			{
				//TODO: Figure out a better way of doing this, this is awful and ineffecient.
				painting_boundries_rectangle.set_origin_position_and_size_to_be_always_same_as(positioning_rectangle);

				auto this_element_id = next_element_id++;
				elements_in_order.insert(std::make_pair(this_element_id, new_element));
				add_child(new_element);

				total_list_y_size += new_element->positioning_rectangle.get_size().y;

				new_element->positioning_rectangle.set_origin_position_calculator([this,this_element_id,new_element]()
				{
					//essentially, the first element sets the position and the rest of the elements chain off of it.
					auto my_iterator_in_map = elements_in_order.find(this_element_id);
					if (my_iterator_in_map == elements_in_order.begin())
					{
						return positioning_rectangle.get_position() + mm::vec2{0,get_scroll()};
					}
					else
					{
						--my_iterator_in_map;
						return my_iterator_in_map->second->positioning_rectangle.get_position() + mm::vec2{0,my_iterator_in_map->second->positioning_rectangle.get_size().y};
					}
				});
				new_element->painting_boundries_rectangle.set_origin_position_and_size_to_be_always_same_as(positioning_rectangle);
				printf("my boundry size %f %f", painting_boundries_rectangle.get_size().x,painting_boundries_rectangle.get_size().y);
				printf("their boundry size %f %f", new_element->painting_boundries_rectangle.get_size().x,new_element->painting_boundries_rectangle.get_size().y);

				return next_element_id;
			}
			inline void set_scroll(double new_scroll) 
			{
				scroll = std::clamp(new_scroll,-total_list_y_size,positioning_rectangle.get_size().y); 
				positioning_rectangle.recalculate();
			}
			inline double get_scroll() { return scroll; }

			double total_list_y_size = 0;
		private:
			std::map<unsigned int, listed_type*> elements_in_order;
			unsigned int next_element_id = 0;

			double scroll = 0;
	};

	class button : public element, 
		public has_event_and_can_delete_parent_from_it<input::computer::mouse_button::event_data_t>,
		public has_event_and_can_delete_parent_from_it<on_hover>,
		public has_event_and_can_delete_parent_from_it<on_unhover>
	{
		inline constexpr static auto null_cback = [](){return nullptr;};
		public:
			template<typename style_t>
			button(state uistate, style_t style, std::shared_ptr<text> ui_text, std::function<element*()> in_callback=null_cback) :
				element(uistate),
				callback(in_callback)
			{
				this->ui_text = ui_text;
				ui_text->painting_boundries_rectangle.set_origin_position_and_size_to_be_always_same_as(painting_boundries_rectangle);

				auto [normal,pressed,highlight] = style.new_shapes(uistate);
				
				normal->positioning_rectangle.set_origin_position_and_size_to_be_always_same_as(positioning_rectangle);
				pressed->positioning_rectangle.set_origin_position_and_size_to_be_always_same_as(positioning_rectangle);
				highlight->positioning_rectangle.set_origin_position_and_size_to_be_always_same_as(positioning_rectangle);

				normal->painting_boundries_rectangle.set_origin_position_and_size_to_be_always_same_as(painting_boundries_rectangle);
				pressed->painting_boundries_rectangle.set_origin_position_and_size_to_be_always_same_as(painting_boundries_rectangle);
				highlight->painting_boundries_rectangle.set_origin_position_and_size_to_be_always_same_as(painting_boundries_rectangle);

				ui_text->positioning_rectangle.set_origin_position_calculator([this](){return this->positioning_rectangle.get_position()+this->positioning_rectangle.get_size()/2;});
				ui_text->positioning_rectangle.set_origin_size_calculator([this](){return this->positioning_rectangle.get_size();});
				positioning_rectangle.add_child_rectangle(ui_text->positioning_rectangle);

				normal_rectangle = std::unique_ptr<element>(dynamic_cast<element*>(normal));
				pressed_rectangle = std::unique_ptr<element>(dynamic_cast<element*>(pressed));
				highlight_rectangle = std::unique_ptr<element>(dynamic_cast<element*>(highlight));

				positioning_rectangle.set_expanded_size(ui_text->positioning_rectangle.get_size());

				add_child_without_modfying_positioning_rectangles(normal);
				add_child_without_modfying_positioning_rectangles(*ui_text);
			}
			template<typename style_t>
			button(state uistate, style_t style, std::string str, ui::font_style_t button_font, std::function<element*()> in_callback=null_cback) :
				button(uistate, style, std::make_shared<text>(uistate,str,button_font), in_callback)
			{
				ui_text->positioning_rectangle.set_modifier(ui::positioning_rectangle_t::modifiers::center_x,true);
				ui_text->positioning_rectangle.set_modifier(ui::positioning_rectangle_t::modifiers::center_y,true);
				ui_text->positioning_rectangle.set_modifier(ui::positioning_rectangle_t::modifiers::expand_size_to_fit,true);
			}
			void impl_render() override;
			element* impl_dispatch_event_maybe_delete_parent(input::computer::mouse_button::event_data_t button) override;
			element* impl_dispatch_event_maybe_delete_parent(on_hover) override;
			element* impl_dispatch_event_maybe_delete_parent(on_unhover) override;
			bool get_is_highlighted();

			std::function<element*()> callback;
			std::function<element*()> highlight_callback = null_cback;
			std::function<element*()> unhighlight_callback = null_cback;

			bool artificially_pressed = false;

			std::shared_ptr<text> ui_text;
		private:
			mm::vec2 position;
			
			bool pressed = false;

			std::unique_ptr<element> normal_rectangle;
			std::unique_ptr<element> pressed_rectangle;
			std::unique_ptr<element> highlight_rectangle;
	};
	class progress_bar : public element
	{
		public:
			progress_bar(state uistate, ui::color color, ui::color background, bool show_progress_number);
			void impl_render() override;
			void hud_render(mm::vec2r parent_position); //due to its use in HUDs, this supports rotation
			void set_progress(double progress);
			double get_progress();
		private:
			GE_RectangleShape background;
			GE_RectangleShape bar;
			bool show_progress_number;

			double progress;
	};
	class draggable_progress_bar : public progress_bar,
		public has_event<input::computer::mouse_motion::event_data_t>,
		public has_event<input::computer::mouse_button::event_data_t>
	{
		public:
			inline draggable_progress_bar(state uistate, ui::color color, ui::color background, bool show_progress_number) : 
				progress_bar(uistate,color,background,show_progress_number)
			{
			}
			void impl_dispatch_event(input::computer::mouse_button::event_data_t button) override;
			void impl_dispatch_event(input::computer::mouse_motion::event_data_t motion) override;
		private:
			bool poll_mouse = false;
	};
	class window;
	class title_bar : public element,
		public has_event<input::computer::mouse_button::event_data_t>,
		public has_event<input::computer::mouse_motion::event_data_t>
	{
		public:
			title_bar(state uistate, std::string name, window_title_style_t style);

			void impl_dispatch_event(input::computer::mouse_button::event_data_t button) override;
			void impl_dispatch_event(input::computer::mouse_motion::event_data_t motion) override;

		private:
			text title;
			std::shared_ptr<text> button_text;
			window_title_style_t style;
			window* parrent;
		public:
			button close_button;
		private:
			bool dragging;
			mm::vec2 initial_drag_position;
	};
	/*!
	 * Easy container which holds other elements. The first element added to this will be in the background, while the last will be in the foreground.
	 * deprecated
	 */
	class surface : public element
	{
		public:
			surface(state uistate, color backgroundColor);
			~surface();
			void impl_render() override;
			int addelement(element* element);
			element* getelement(int elementID);

		private:
			rectangle background_rect;
			
	};

	class window: public element,
		public has_event<input::computer::mouse_button::event_data_t>,
		public has_event<input::computer::mouse_motion::event_data_t>,
		public has_event<on_focus>,
		public has_event<on_unfocus>
	{
		public: 
			window(state uistate, std::string name, mm::vec2 position, mm::vec2 surfaceSize, style style);

			void impl_dispatch_event(input::computer::mouse_motion::event_data_t motion) override;
			void impl_dispatch_event(input::computer::mouse_button::event_data_t button) override;
			void impl_dispatch_event(on_focus) override;
			void impl_dispatch_event(on_unfocus) override;

			//only the size of this is used; the window will never resize to be smaller than this.
			positioning_rectangle_t min_size;

			surface ui_surface;
			title_bar ui_title_bar;
		private:
			focus_color_change_rectangle border;

			double title_barHeight;
			double borderOffset;
			double extra_dragging;

			bool is_resizing = false;
			mm::vec2 resizing_coeffecients;
		

	};
	using selected_callback_t = std::function<void()>;


	struct regular_menu_item_descriptor
	{
		std::string name;
		selected_callback_t selected_callback;
	};
	struct divider_menu_item_descriptor {};
	template <typename submenu_container_t>
	struct submenu_menu_item_descriptor
	{
		std::string name;
		submenu_container_t submenu;
		bool is_drop_down;
	};
	template<typename T>
	concept submenu_menu_item = requires(T t)
	{
		std::is_same_v<decltype(t.name), std::string>;
		{ t.submenu };
	};
	static_assert(submenu_menu_item<submenu_menu_item_descriptor<std::tuple<>>>);
	template<typename T>
	concept regular_menu_item = requires (T t)
	{
		std::is_same_v<decltype(t.name), std::string>;
		std::is_same_v<decltype(t.selected_callback), selected_callback_t>;
	};
	static_assert(regular_menu_item<regular_menu_item_descriptor>);
	template<typename T>
	concept divider_menu_item = std::is_same_v<divider_menu_item_descriptor, T>;
	template<typename T>
	concept valid_menu_item = submenu_menu_item<T> || regular_menu_item<T> || divider_menu_item<T>;

	//necessary because value templates (and concepts, which are value templates) can't be passed as a template template parameter.
	template<typename T>
	struct cpp11_style_concept_valid_menu_item
	{
		static constexpr bool value = valid_menu_item<T>;
	};
	template<typename T>
	concept tuple_of_regular_menu_item_descriptor_concept = variadic_util::all_tuple_elements_meet_cpp11_concept<cpp11_style_concept_valid_menu_item, T>;

	/*!
	 * Provides a drop-down or left-to-right list.
	 *
	 * Useful for right click menus, file-edit-view menus, and more.
	 *
	 * Note that you will have to adjust this elements painting_boundries_rectangle if you have sub-menus,
	 * or else they will not render since they will be out-of-bounds.
	 */
	class menu_list : public element,
		public has_event<on_unhover>
	{

		std::vector<menu_list*> sub_lists;
		menu_list* open_sub = nullptr;
		button* open_subs_button = nullptr;

		menu_list* parent = nullptr;
		

		menu_list_style style;
		mm::vec2 min_size;
		mm::vec2 coeffecient;
		mm::vec2 reverse_coeffecient;


		template<typename item_t>
		inline void create_new_button(item_t item)
		{
			auto new_button = new button(uistate, button_style_t{style.background_focused, style.highlight, style.highlight}, item.name, style.font);
			new_button->positioning_rectangle.set_size(min_size);
			new_button->painting_boundries_rectangle.set_origin_position_and_size_to_be_always_same_as(new_button->positioning_rectangle);
			add_child(new_button);
			if constexpr(regular_menu_item<item_t>)
			{
				new_button->callback = [=,this]()
				{
					item.selected_callback();
					clicked_any_callback(item.name);
					return nullptr;
				};
			}
		}
		inline void create_new_submenu(submenu_menu_item auto item)
		{
			auto new_menu_list = new menu_list(uistate, style,min_size, item.is_drop_down, item.submenu);
			new_menu_list->parent = this;

			button* this_button = dynamic_cast<button*>(children.back());
			new_menu_list->positioning_rectangle.set_origin_position_calculator([=,this]()
			{
				auto default_position = this_button->positioning_rectangle.get_position() + this_button->positioning_rectangle.get_size() * reverse_coeffecient;
				//the max position is the maximum possible screen coordinates that the drop-down can occupy without falling off the painting boundries limit
				auto max_position = painting_boundries_rectangle.get_position() + painting_boundries_rectangle.get_size() - new_menu_list->positioning_rectangle.get_size();
				return default_position.min_between(max_position);
			});
			painting_boundries_rectangle.add_child_rectangle(new_menu_list->positioning_rectangle);
			new_menu_list->painting_boundries_rectangle.set_origin_position_and_size_to_be_always_same_as(painting_boundries_rectangle);

			this_button->positioning_rectangle.add_child_rectangle(new_menu_list->positioning_rectangle);
			this_button->highlight_callback = [=,this]()
			{
				if (!open_sub || !open_sub->mouse_is_hovering_me)
				{
					close();
					add_child_without_modfying_positioning_rectangles(new_menu_list);
					open_sub = new_menu_list;
					open_subs_button = this_button;
					this_button->artificially_pressed = true;
				}
				return nullptr;
			};
			this_button->unhighlight_callback = [=,this]()
			{
				if (open_sub == new_menu_list && !new_menu_list->mouse_is_hovering_me)
				{
					close();
				}
				return nullptr;
			};

			sub_lists.push_back(new_menu_list);
		}
		template<bool is_divider>
		inline void glue_child_to_previous()
		{
			if (children.size() > 1)
			{
				element* previous_item = *(children.end()-2);
				element* new_item = *(children.end()-1);

				new_item->positioning_rectangle.set_origin_position_calculator([this,previous_item]()
				{
					mm::vec2 center = {0,0};
					if constexpr(is_divider)
					{
						center = (min_size * reverse_coeffecient) + (coeffecient * style.spacer_size);
						center /= 2.0;
					}
					return previous_item->positioning_rectangle.get_position()*coeffecient + (previous_item->positioning_rectangle.get_size() * coeffecient)
						+ center + positioning_rectangle.get_position() * reverse_coeffecient;
				});
			}
		}
		inline void readjust_size()
		{
			if (!children.empty())
			{
				positioning_rectangle.set_size(children.back()->positioning_rectangle.get_bottom_right() - positioning_rectangle.get_position());
			}
		}

		rectangle background;
		public:
			std::function<void(std::string)> clicked_any_callback = [](std::string){};

			inline void set_clicked_any_callback(std::function<void(std::string)> callback)
			{
				clicked_any_callback = callback;
				for (menu_list* sub : sub_lists)
				{
					sub->set_clicked_any_callback(callback);
				}
			}
			inline void close()
			{
				if (open_sub)
				{
					open_sub->close();
					remove_child(open_sub);
					open_subs_button->artificially_pressed = false;
					open_sub = nullptr;
					open_subs_button = nullptr;
				}
			}
			inline void add_regular_item(regular_menu_item_descriptor item)
			{
				create_new_button(item);
				glue_child_to_previous<false>();
				readjust_size();
			}
			inline void add_submenu_item(std::string name, bool is_drop_down)
			{
				auto item = submenu_menu_item_descriptor{name, std::tuple{}, is_drop_down};
				create_new_button(item);
				create_new_submenu(item);
				glue_child_to_previous<false>();
				readjust_size();
			}
			std::vector<menu_list*> const & get_sub_menus()
			{
				return sub_lists;
			}

			//coeffecient_x and coeffecient_y are used for spacing on the menu.
			//
			//so, a drop down menu may have a coeff. x = 0, and coeff. y = 1
			//that would put all the elements one-after-another
			//
			//a file-edit-view menu might have a coeff. x = 1.01
			//which would put a space of 1% extra padding.
			menu_list(state uistate, menu_list_style style, mm::vec2 min_size, bool is_drop_down, tuple_of_regular_menu_item_descriptor_concept auto descriptor_tuple) :
				element(uistate),
				style(style),
				min_size(min_size),
				coeffecient(is_drop_down? mm::vec2{0.0,1.0} : mm::vec2{1.0,0.0}),
				reverse_coeffecient({coeffecient.y, coeffecient.x}),
				background(uistate, style.background_focused)
			{
				background.painting_boundries_rectangle.set_origin_position_and_size_to_be_always_same_as(painting_boundries_rectangle);
				variadic_util::for_each_element_in_variadic(descriptor_tuple, [=,this]<typename item_t>(item_t item)
				{
					//process the individual item type
					if constexpr(divider_menu_item<item_t>)
					{
						auto new_divider = new rectangle(uistate, style.spacer_color);
						add_child(new_divider);
						new_divider->positioning_rectangle.set_size(mm::vec2{style.spacer_size,style.spacer_size}*coeffecient + (min_size * style.coeffecient_spacer_width) * reverse_coeffecient);
						new_divider->positioning_rectangle.set_modifier(ui::positioning_rectangle_t::modifiers::center_x,true);
						new_divider->positioning_rectangle.set_modifier(ui::positioning_rectangle_t::modifiers::center_y,true);
					}
					else //if it's a regular item or a submenu
					{
						create_new_button(item);
					}
					if constexpr(submenu_menu_item<item_t>)
					{
						create_new_submenu(item);
					}
					
					glue_child_to_previous<divider_menu_item<item_t>>();
				});

				readjust_size();
				background.positioning_rectangle.set_origin_position_and_size_to_be_always_same_as(positioning_rectangle);
			}
			inline ~menu_list()
			{
				close(); //to ensure that no sub_list is also in `children`, which would cause a double-delete.
				for (element* sub_list : sub_lists)
				{
					delete sub_list;
				}
				for (element* child : children)
				{
					delete child;
				}
			}
			inline void impl_render() override
			{
				background.render();
			}
			
			inline void impl_dispatch_event(on_unhover) override
			{
				bool any_subs_hovered = false;
				for(menu_list* sub : sub_lists)
				{
					if (sub->mouse_is_hovering_me)
					{
						any_subs_hovered = true;
						break;
					}
				}
				if (!any_subs_hovered)
				{
					if (parent && !parent->open_subs_button->mouse_is_hovering_me)
					{
						parent->close();
					}
					close();
				}
			}
			inline bool is_in_bounds(mm::vec2 mouse) override
			{
				return element::is_in_bounds(mouse) || (open_sub? open_sub->is_in_bounds(mouse) : false);
			}
	};


	/*!
	 * The way this should be coded, is a list of items which are sometimes "drops" which drop to the right
	 * and ask the windows manager for a new painting boundries.
	 *
	 * But the way it should be created for the UI designer is as it is currently.
	 *
	 * Getting those two things right at the same time is tricky.
	 */

	class menu_list_button : public element,
		public has_event<on_unfocus>
	{
		button list_button;
		menu_list list;

		inline void close()
		{
			if (children.size() == 2)
			{
				remove_child(1);
				list.close();
			}
		}
		public:
			inline void add_regular_item(regular_menu_item_descriptor item)
			{
				list.add_regular_item(item);
			}
			menu_list_button(state uistate, menu_list_style style, mm::vec2 min_size, std::string txt, bool show_selected_item, tuple_of_regular_menu_item_descriptor_concept auto descriptor_tuple) :
				element(uistate),
				list_button(uistate,button_style_with_focus_color_change_t{style.background_focused,style.background_unfocused, style.highlight, style.highlight}, txt, style.font, [this]()
				{
					if (children.size() == 2)
					{
						close();
					}
					else
					{
						add_child_without_modfying_positioning_rectangles(list);
					}
					return nullptr;
				}),
				list(uistate,style,min_size,true, descriptor_tuple)
			{
				add_child(list_button);
				positioning_rectangle.set_size(min_size);
				list_button.positioning_rectangle.set_origin_size_calculator([this](){return positioning_rectangle.get_size();});

				list.set_clicked_any_callback([this,show_selected_item](std::string name)
				{
					close();
					if (show_selected_item)
					{
						list_button.ui_text->set_text(name);
					}
				});
				list.positioning_rectangle.set_origin_position_calculator([this]()
				{
					return list_button.positioning_rectangle.get_bottom_left();
				});
				list_button.positioning_rectangle.add_child_rectangle(list.positioning_rectangle);
				list.painting_boundries_rectangle.set_origin_position_and_size_to_be_always_same_as(painting_boundries_rectangle);
			}
			inline void impl_dispatch_event(on_unfocus) override
			{
				close();
			}
			inline bool is_in_bounds(mm::vec2 mouse) override
			{
				return list_button.is_in_bounds(mouse) || (children.size() == 2 && list.is_in_bounds(mouse));
			}
	};
	
	class popup_ok : public element
	{
		public:
			popup_ok(state uistate, mm::vec2 position, mm::vec2 size, element* displayelement,std::string name, std::string oktxt, std::string canceltxt, popup_ok_style_t style, std::function<element*()> ok_callback, std::function<element*()> cancel_callback,bool has_cancel = true);
			window ui_window;
			positioning_rectangle_t area_between_top_of_surface_and_top_of_buttons;

			button* ok;
			button* cancel = nullptr;
	};

	class window_manager : public element
	{
		void impl_child_changed_focus(size_t last_focused_child, size_t& new_focused_child) override;

		std::unique_ptr<element> background;
		public:
			window_manager(state uistate);

			void set_background(std::unique_ptr<element>&& new_background);
			void add_window(element* window, bool steal_focus = true);
			void remove_window(element* window);
	};

	void popup_message(state uistate, window_manager* wm, mm::vec2 position, mm::vec2 size, std::string title, std::string msg, font_style_t font_style, popup_ok_style_t style);

	inline std::tuple<rectangle*, rectangle*, rectangle*> button_style_t::new_shapes(state uistate)
	{
		return std::make_tuple(
			new rectangle(uistate,background),
			new rectangle(uistate,pressed),
			new rectangle(uistate,highlighted)
		);
	}
	inline std::tuple<focus_color_change_rectangle*, rectangle*, rectangle*> button_style_with_focus_color_change_t::new_shapes(state uistate)
	{
		return std::make_tuple(
			new focus_color_change_rectangle(uistate,background_focused, background_unfocused),
			new rectangle(uistate,pressed),
			new rectangle(uistate,highlighted)
		);
	}
	inline std::tuple<sprite*, sprite*, sprite*> button_style_sprite_t::new_shapes(state uistate)
	{
		return std::make_tuple(
			new sprite(uistate,background_sprite_style_t),
			new sprite(uistate,pressed_sprite_style_t),
			new sprite(uistate,highlight_sprite_style_t)
		);
	}
}
