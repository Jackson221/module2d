//Copyright 2017-2019 Jackson Reed McNeill. Licensed under the GNU AGPL version 3 or any later version.
#include "debug.hpp"

#if DEBUG_RENDERS_ENABLED()
#include <cstdio>
#include <string>
#include <iostream>
#include <cmath>
#include <vector>
#include <stdexcept>
#include <functional>
#include <mutex>

#include <sys/time.h>



namespace debug
{
	double get_unix_time()
	{
		struct timeval tv;
		gettimeofday(&tv,NULL);
		
		return static_cast<double>(tv.tv_sec)+(static_cast<double>(tv.tv_usec)/(static_cast<double>(1e7))); //Add the microseconds divded by 1e7 to end up with seconds.microseconds
		//Static casting intensifies
	}


	thread_local instance this_thread_instance;


	mm::vec2r physics_position_to_screen(mm::vec2r position)
	{
		if (!has_been_passed)
		{
			return position;
		}
		return camera->apply_camera_offset(position);
	}
	mm::vec2 physics_position_to_screen(mm::vec2 position)
	{
		auto result = physics_position_to_screen(static_cast<mm::vec2r>(position));
		return {result.x,result.y};
	}

	ui::state uistate;
	render::camera* camera; 
	bool has_been_passed = false;
	GE_Font debug_sans; 
	bool isInit = false;
	void init()
	{
		auto debug_sans_opt = GE_Font_GetFont("FreeSans",15);
		if(!debug_sans_opt.has_value()) 
		{
			throw std::runtime_error("Couldn't get debug sans font");
		}
		debug_sans = debug_sans_opt.value();
	}
#define init_if_needed() if(!isInit) {isInit = true; init();} //glorious inline initialization

	void pass_uistate(ui::state in_uistate, render::camera* your_camera)
	{
		init_if_needed();
		uistate = in_uistate;
		camera = your_camera;
		has_been_passed = true;
	}

	void cursor_text_at_cursor()
	{
		int x,y;
		SDL_GetMouseState(&x,&y);
		text_at<null_position_modifier>(transient_object_pusher{},"  x "+std::to_string(x)+" y "+std::to_string(y),mm::vec2r{static_cast<double>(x),static_cast<double>(y),0.0});
	}

	instance::instance() : 
		buffer()
	{
	}
	instance::~instance()
	{
	}
	void instance::done_drawing()
	{
		buffer.push_in_to_middle();
		buffer_contiguous.push_in_to_middle();
	}
	constexpr size_t max_objects_to_render = 5000;
	constexpr size_t clear_everything_if_more_than_objects = 40000;
	template<typename buffer_t>
	void instance_render_for_buffer(buffer_t& buffer)
	{
		buffer.push_middle_to_out();
		buffer.accessing_out_mutex.lock();

		typename decltype(buffer.out)::iterator start_object;
		typename decltype(buffer.out)::iterator end_object;
		if (buffer.out.size() > max_objects_to_render)
		{
			start_object = buffer.out.begin()+(rand() % (buffer.out.size() - max_objects_to_render));
			end_object = start_object + max_objects_to_render;
		}
		else
		{
			start_object =  buffer.out.begin();
			end_object = buffer.out.end();
		}
		for(auto it = start_object; it != end_object; it++)
		{
			auto & object = *it;
			object.render();
		}
		buffer.accessing_out_mutex.unlock();
	}
	void instance::render()
	{
		instance_render_for_buffer(buffer);
		instance_render_for_buffer(buffer_contiguous);
	}
	std::vector<_internal_sticky_object_struct> _internal_sticky_object_vector;
	class on_screen_message_object
	{
		friend void render_sticky_objects();
		static constexpr double font_vertical_spacing = 20;
		public:
			ui::color color;
			std::string msg;
			on_screen_message_object(ui::color color, std::string msg) : 
				color(color),
				msg(msg)
			{
			}
			void render()
			{
				if (!has_been_passed)
				{
					return;
				}
				auto mytext = ui::text(uistate, msg, color,debug_sans); 
				mytext.positioning_rectangle.set_position({0,current_y});
				mytext.positioning_rectangle.set_modifier(ui::positioning_rectangle_t::modifiers::expand_size_to_fit,true);
				mytext.render();

				current_y += font_vertical_spacing;
			}
		protected:
			static void reset_y()
			{
				current_y = 0;	
			}
		private:
			static double current_y;

	};
	double on_screen_message_object::current_y = 0;

	size_t _on_screen_message_internal(ui::color color, double duration, std::string msg)
	{
		return sticky_object_pusher{duration}.push_object(on_screen_message_object{color,msg});
	}

	void render_sticky_objects()
	{
		auto time = get_unix_time(); //slightly less accurate but better than hammering system calls
		size_t rendered_objects_so_far = 0;
		size_t objects_to_render = _internal_sticky_object_vector.size();

		bool too_many_objects = objects_to_render > max_objects_to_render;

		if (objects_to_render > clear_everything_if_more_than_objects)
		{
			_internal_sticky_object_vector.clear();
		}
		for (auto it = _internal_sticky_object_vector.begin(); it != _internal_sticky_object_vector.end();)
		{
			if(it->end_time < time)
			{
				it = _internal_sticky_object_vector.erase(it);
			}
			else
			{
				/*if (rendered_objects_so_far < max_objects_to_render)
				{
					it->object.render();
				}*/
				if (too_many_objects)
				{
					if ( rand() % objects_to_render < max_objects_to_render)
					{
						it->object.render();
					}
				}
				else
				{
					it->object.render();
				}
				rendered_objects_so_far++;
				it++;	
			}
		}
		on_screen_message_object::reset_y(); //special case function for on screen messages
	}
	thread_local std::vector<std::pair<ui::color,GE_RectangleShape>> instantiated_colors = {};
}
#endif
