//Copyright 2017-2019 Jackson Reed McNeill. Licensed under the GNU AGPL version 3 or any later version.
#include "stars.hpp"

#include <cmath>
#include <array>
//Local includes
#include "UI/shapes.h"
#include "render/camera.hpp"
#include "mathematics/common_ops.hpp"


#include <chrono>
namespace render
{
	template<typename T, size_t size>
	struct circular_replacement_buffer
	{
		std::array<T,size> store;
		size_t it = std::numeric_limits<size_t>::max();
		void push_back(T t)
		{
			if (it++ == std::numeric_limits<size_t>::max())
				for (T& a : store)
					a = t;
			else
				store[it %= size] = t;
		}
	};
	struct single_star
	{
		mm::vec2 position;
		int size;
		unsigned int color; //color from the colorIDs in the starfield
		double distance;
		//circular_replacement_buffer<SDL_Rect, 10> last_positions;
	};
	struct rendering_surface
	{
		/*
		SDL_Surface* surface = nullptr;
		rendering_surface& operator=(rendering_surface const & o)
		{
			SDL_FreeSurface(surface);
			surface = o.surface;
			return *this;
		}*/
	};

	//TODO: ideas-
	/*Apply the same "repeat star field" thing but to a whole pre-rendered plate of stars
	 * Procedurally generate stars as you fly along
	 */

	//crap random function pulled from an old engine header
	double random_range_double(int low, int high)
	{
		return (static_cast<double>(  ((rand() % (high+1-low)))+low  ));
	}

	const auto nthreads = std::thread::hardware_concurrency();

	
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
		constexpr uint32_t rmask = 0xff000000;
		constexpr uint32_t gmask = 0x00ff0000;
		constexpr uint32_t bmask = 0x0000ff00;
		constexpr uint32_t amask = 0x000000ff;
#else
		constexpr uint32_t rmask = 0x000000ff;
		constexpr uint32_t gmask = 0x0000ff00;
		constexpr uint32_t bmask = 0x00ff0000;
		constexpr uint32_t amask = 0xff000000;
#endif

	size_t oneifzero(size_t a)
	{
		return a == 0? 1 : a;
	}

	size_t frame = 0;


	//Note: The star renderer has race conditions by design: in that there is potential for Z-fighting due to lack of depth buffer; it doesn't matter.

	stars::stars(ui::state _uistate, unsigned int number, unsigned int width, unsigned int height,std::vector<int> sizes, std::vector<ui::color> colors,render::camera* _camera, double high_dist, double low_dist, std::chrono::nanoseconds _ideal_tick_length) :
		ideal_tick_length(_ideal_tick_length),
		ui::element(_uistate),
		star_texture_creation(uistate.rstate->api.my_renderer.p_device,*uistate.rstate->api.my_renderer.device,*uistate.rstate->api.my_renderer.surface),
		submit_pool([this](std::monostate&, size_t)
		{
			auto _ = std::lock_guard(submit_surface_mutex);
			if (alive)
			{
				auto& my_renderer = uistate.rstate->api;
				auto rs = std::make_unique<vkrender::texture>(star_texture_creation,submit_surface,vkrender::texture::filter::nearest,true,false);
				{
					auto _ = std::lock_guard(rendered_sprite_mutex);
					rendered_sprite_working_space = std::move(rs);
				}
			}
		},1),
		tp(std::vector{decltype(tp)::worker_functions_t([this](rendering_surface&, size_t tnum)
		{
			auto r = algorithm::this_thread_work_range(nthreads, single_stars.size(), tnum);

			mm::vec2r position;
			position.r = 0; //the rotation is always 0
			for (size_t i = r.first; i < r.second; i++) //iterate over the stars assigned to this thread
			{
				//do math to get star position from camera
				auto svc = star_visible_camera;
				auto& star = single_stars[i];
				position.x = star.position.x-(svc.pos.x*star.distance);
				position.y = star.position.y-(svc.pos.y*star.distance);

				position = mm::vec2r{mm::fast_fmod(position.x,star_plate_size.x),mm::fast_fmod(position.y,star_plate_size.y),0};
				position -= star_plate_size/2;
				position *= svc.scale;
				position = mm::rotation_cw(position,svc.pos.r);
				position += last_size/2;

				double star_size = static_cast<double>(star.size)*svc.scale;

				if ( ( position.x+star_size >= 0) && (position.x-star_size <= svc.screen.x) && (position.y+star_size >= 0) && (position.y-star_size <= svc.screen.y))
				{
					auto color = star_colors[star.color];
					if (star_size < 1.0)
					{
						color.r *= star_size;
						color.g *= star_size;
						color.b *= star_size;
					}

					star_size = std::ceil(std::max(1.0,star_size));
					SDL_Rect x = {position.x,position.y,star_size,star_size};
					//star.last_positions.push_back(x);
					//SDL_FillRects(surface, star.last_positions.store.data(),star.last_positions.store.size(), SDL_MapRGB(surface->format, color.r,color.g,color.b));
					SDL_FillRect(surface, &x, SDL_MapRGB(surface->format, color.r,color.g,color.b));
				}
				else
				{
					//star.last_positions.it = std::numeric_limits<size_t>::max();
				}
			}
		}),
		decltype(tp)::worker_functions_t([this](rendering_surface&, size_t tnum)
		{
			//Why do we seem to have some kind of bizzarre situations with race conditions here?!?!?!
			//The fact that this works as a fix makes equally little sense.
			//Why does it seem like the first few times have race conditions but later doesn't?
			//Race conditions w.r.t. reading the pixels on the buffer (and logically there should be none but 
			//maybe SDL2 is tiling internally? Likely not).
			//should not cause crash issues but only visual errors.
			//TODO: Duct Tape Class C-1-2 star_weird_race
			auto y = algorithm::this_thread_work_range(nthreads, last_size.y, tnum);
			SDL_Rect r = {0,y.first,last_size.x,y.second-y.first};
			if (++first <= nthreads*10)
				tmpm.lock();
			if (first <= nthreads*10)
				SDL_Delay(1);
			SDL_BlitSurface(black_surface, &r,surface,&r); // The transparency gives us motion blur
			if (first <= nthreads*10)
				tmpm.unlock();
		})}, nthreads, [this](size_t)
		{
			return rendering_surface{};//SDL_CreateRGBSurface(0, oneifzero(positioning_rectangle.get_size().x), oneifzero(positioning_rectangle.get_size().y), 32, rmask, gmask, bmask, amask)};
		}, [this](std::vector<rendering_surface>&, size_t function_done)
		{
			if (function_done != 1) //only run right after star rendering; not after motion blurring. We're running below after both functions are done.
				return;
			auto start_time = std::chrono::steady_clock::now();

			if (first_time || submit_future.wait_for(std::chrono::milliseconds(0)) == std::future_status::ready)
			{
				SDL_BlitSurface(surface, nullptr,submit_surface,nullptr);
				submit_future = submit_pool.work();
				if (first_time)
					submit_future.wait();
				first_time = false;
			}


			{
				auto _ = std::lock_guard(star_visible_camera_in_mutex);
				star_visible_camera = star_visible_camera_in;
			}
			
			framerate_guard.reset();
			framerate_guard = algorithm::framerate_guard(ideal_tick_length);
			if (frame++ % 1000 == 0) //not a good fps counter
			{
				auto finish_time = std::chrono::steady_clock::now();
				auto time_took_to_tick = finish_time - start_time;
				float res = std::chrono::duration_cast<std::chrono::nanoseconds>(time_took_to_tick).count()/1000000.0;
				printf("stars at %f fps\n", 1000/res);
			}
		})
	{
		this->star_plate_size = {static_cast<double>(width),static_cast<double>(height)};

		this->camera = _camera;
		star_visible_camera = *camera;
		star_visible_camera_in = *camera;

		double accuracy = 10000.f; 
		single_stars.reserve(number);
		for (unsigned int i =0;i<=number;i++)
		{
			single_stars.push_back(single_star{
				mm::vec2{random_range_double(0,width),random_range_double(0,height)},
				sizes[  static_cast<int>(random_range_double(0,sizes.size()-1))  ],
				static_cast<unsigned int>(random_range_double(0,colors.size()-1)),
				1.f/pow(random_range_double(static_cast<int>(pow(high_dist,1)*accuracy), static_cast<int>(pow(low_dist,1)*accuracy))/accuracy,2),
			});
		}
		
		star_colors = colors;

		positioning_rectangle.set_update_callback_and_run_it_now([this]()
		{
			if ( surface == nullptr || (last_size - positioning_rectangle.get_size()).magnitude() > .1)
			{
				auto _ = std::lock_guard(submit_surface_mutex);
				bool brake = surface != nullptr;
				if (brake)
					tp.brake().wait();
				last_size = positioning_rectangle.get_size();
				last_size.x = std::max(1.0, last_size.x);
				last_size.y = std::max(1.0, last_size.y);
				SDL_FreeSurface(surface);
				SDL_FreeSurface(submit_surface);
				SDL_FreeSurface(black_surface);
				surface = SDL_CreateRGBSurface(0, last_size.x, last_size.y, 32, rmask, gmask, bmask, 0);
				submit_surface = SDL_CreateRGBSurface(0, last_size.x, last_size.y, 32, rmask, gmask, bmask, amask);
				black_surface = SDL_CreateRGBSurface(0, last_size.x, last_size.y, 32, rmask, gmask, bmask, amask);
				SDL_SetSurfaceRLE(surface, false);
				SDL_SetSurfaceRLE(submit_surface, false);
				SDL_SetSurfaceRLE(black_surface, false);
				printf("ml %d %d %d\n",SDL_MUSTLOCK(surface),SDL_MUSTLOCK(submit_surface),SDL_MUSTLOCK(black_surface));
				SDL_Rect x = {0,0,last_size.x,last_size.y};
				// 27 times how many fps over 144 it is
				// so 27 divided by how many doublings of 6.4ms it is
				// 27 times how many halvings of 6.4ms it is
				//SDL_FillRect(black_surface, &x, SDL_MapRGBA(black_surface->format, 0,0,0,27 * (6944444.0 / ideal_tick_length.count())));
				SDL_FillRect(black_surface, &x, SDL_MapRGBA(black_surface->format, 0,0,0,log(532048240601 * (6944444.0 / ideal_tick_length.count()))));
				//SDL_FillRect(black_surface, &x, SDL_MapRGB(black_surface->format, 0,0,0));
				if (brake)
					tp.release_brake();
			}
		});
		tp.start();
		while(true)
		{
			{
				auto _ = std::lock_guard(rendered_sprite_mutex);
				if (rendered_sprite_working_space)
					break;
			}
			std::this_thread::sleep_for(std::chrono::milliseconds(1));
		}
	}
	stars::~stars()
	{
		tp.brake().wait();
		auto _ = std::lock_guard(submit_surface_mutex);
		alive = false;
		SDL_FreeSurface(surface);
		SDL_FreeSurface(submit_surface);
		SDL_FreeSurface(black_surface);
	}



	void stars::impl_render() 
	{
		if (positioning_rectangle.get_size() == mm::vec2{0,0}) {return;}

		/*for (auto child_surface : tp.finished_work)
			SDL_BlitSurface(child_surface.surface, nullptr,surface,nullptr);*/
		ALGORITHM_THREAD_TRY_LOCK_GUARD(rendered_sprite_mutex) //is it even really necessary to try lock anymore?
		{
			if (rendered_sprite_working_space)
				rendered_sprite.emplace(uistate.rstate->api, last_size.x,last_size.y, std::move(*rendered_sprite_working_space.release()));
		}
		{
			auto _ = std::lock_guard(star_visible_camera_in_mutex);
			star_visible_camera_in = *camera;
		}
		uistate.rstate->api.add_transient_object(*rendered_sprite, positioning_rectangle.get_size(),{0,0,last_size.x,last_size.y}, mm::vec2r(positioning_rectangle.get_position()+last_size/2));
	}
}
