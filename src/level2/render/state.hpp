/*!
 * Holds the renderer state
 */
#pragma once

#include "render/vkrender_backend.hpp"

//welp, we are now official dependant on vkrender to provide the state.hpp file
//so a new rendering backend would have to rewrite all of this or be compatible
//with res_mode_t
#include "vkrender/init.hpp"

#include "mathematics/primitives.hpp"
#include "event/event.hpp"

namespace render
{
	//why does some of this exist when it's just "options.hpp"?
	namespace resolution_change
	{
		struct event_data_t
		{
			mm::vec2 resolution;
			vkrender::res_mode_t res_mode;
			float refresh;
			vkrender::fullscreen_mode_t fullscreen;
		};
		using types = event::regular_event_types<event_data_t>;
	}
	struct state
	{
		renderer api;

		resolution_change::event_data_t data;

		state(mm::vec2 resolution,vkrender::res_mode_t res_mode, vkrender::fullscreen_mode_t fullscreen, std::string window_name);
		void change_resolution(mm::vec2 resolution, vkrender::res_mode_t res_mode, vkrender::fullscreen_mode_t fullscreen);
		mm::vec2 get_resolution();
		
		resolution_change::types::manager_t resolution_change_manager;

	};
}
