//Copyright 2017-2019 Jackson Reed McNeill. Licensed under the GNU AGPL version 3 or any later version.
#include "camera.hpp"

#include "mathematics/common_ops.hpp"

namespace render
{
	mm::vec2r camera::apply_camera_offset(mm::vec2r subject)
	{
		//scale
		subject *= scale;

		//move their position to be at the camera
		subject -= pos*scale;

		//perform matrix transformation, rotating them arround what-is-currently 0,0
		subject = mm::rotation_cw(subject,pos.r);

		//adjust their position to be at the center of the screen
		subject += screen/2;
		subject.r = subject.r - pos.r;

		return subject;
	}
	mm::vec2r camera::negate_camera_offset(mm::vec2r subject)
	{
		subject.r = subject.r + pos.r;

		//adjust their position to be at the center of the screen
		subject -= screen/2;

		//perform matrix transformation, rotating them arround what-is-currently 0,0
		subject = mm::rotation_ccw(subject,pos.r);

		//move their position to be at the camera
		subject += pos*scale;

		//scale
		subject /= scale;
		return subject;
	}
}
