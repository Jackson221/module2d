/*!
 *
 * @file
 * @author Jackson McNeill
 *
 * A UI element which renders all renderedObjects
 *
 * Copyright 2017-2019 Jackson Reed McNeill. Licensed under the GNU AGPL version 3 or any later version.
 */
#pragma once

#include "render/camera.hpp"
#include "render/object.hpp"
#include "UI/UI.hpp"

namespace ui
{
	class game_render: public ui::element
	{
		public:
			game_render(state uistate, render::render_engine& in_engine, render::camera* camera);
			void impl_render() override;
		private:
			render::camera* camera;
			render::render_engine* engine;
	};
}
