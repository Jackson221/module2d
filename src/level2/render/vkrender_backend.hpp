#include "vkrender/object.hpp"
#include "vkrender/texture.hpp"
#include "vkrender/init.hpp"
#include "vkrender/camera/camera.hpp"

#include "algorithm/ecs.hpp"

#include "mathematics/primitives.hpp"

#include <memory>
#include <set>
#include <vector>

#pragma once

namespace render
{
	class renderer;

	class backend_sprite_impl
	{
		public: //don't access these
			uint32_t w;
			uint32_t h;
			vkrender::texture texture;
			//friend class transient_object_manager;

		public:
			backend_sprite_impl(renderer& my_renderer, SDL_Surface* surface, bool free=true);
			backend_sprite_impl(renderer& my_renderer, uint32_t w, uint32_t h, vkrender::texture texture);
	};

	//TODO: Consider adding a default constructor for better std::optional usage?
	class backend_sprite
	{
		static constexpr size_t invalid_id = std::numeric_limits<size_t>::max();
		renderer* renderer_ptr;
		protected:
			friend class renderer;

			size_t id;
		public:
			inline backend_sprite() :
				id(invalid_id)
			{}
			backend_sprite(renderer& my_renderer, SDL_Surface* surface, bool free=true);
			backend_sprite(renderer& my_renderer,uint32_t w, uint32_t h, vkrender::texture texture);
			~backend_sprite();

			inline backend_sprite(backend_sprite && other) :
				renderer_ptr(other.renderer_ptr),
				id(std::exchange(other.id,invalid_id))
			{
			}
			inline backend_sprite& operator=(backend_sprite && other)
			{
				this->~backend_sprite(); //TODO am I supposed to do this?
				renderer_ptr = other.renderer_ptr;
				id = std::exchange(other.id,invalid_id);

				return *this;
			}

			backend_sprite(backend_sprite const &) = delete;
			auto operator=(backend_sprite const &) = delete;
	};

	const auto NORMAL = glm::vec3{0,0,-1};
	const auto BOX_INDICES = std::vector<uint32_t>{2,1,0,0,3,2};
	constexpr auto BOX_INDICES_SIZE = 6;

	const inline auto get_box_vertices(float width, float height, mm::rect sprite_src = {0,0,1,1})
	{
		glm::vec2 start_src = {sprite_src.x,sprite_src.y+sprite_src.h};
		glm::vec2 end_src = start_src+glm::vec2{sprite_src.w,-sprite_src.h};
		return std::vector
		{ 
			vkrender::vertex{ {0,0,0}, {0,0,0}, start_src, NORMAL},
			vkrender::vertex{ {0,height,0}, {0,0,0}, {start_src.x,end_src.y}, NORMAL},
			vkrender::vertex{ {width,height,0}, {0,0,0}, end_src, NORMAL},
			vkrender::vertex{ {width,0,0}, {0,0,0}, {end_src.x,start_src.y}, NORMAL},
		};

	}
	constexpr auto BOX_VERTICES_SIZE = 4;



	struct std_uniform_buffer_object
	{
		glm::mat4 proj;
		glm::mat4 model;
	};

	template<typename uniform_buffer_object>
	class transient_object_manager
	{
		
		using uniform_buffer = vkrender::uniform_buffer<uniform_buffer_object>;


		static auto constexpr my_descriptor_layout_bindings = vkrender::descriptor_layout_bindings<std::tuple
		<
			vkrender::descriptor_layout_info<0,uniform_buffer,vk::ShaderStageFlagBits::eVertex>,
			vkrender::descriptor_layout_info<1,vkrender::texture,vk::ShaderStageFlagBits::eFragment>
		>>{};



		static constexpr size_t OBJ_PER_BUF = 2;//048;

		vk::PhysicalDevice p_device;
		vk::Device device;
		vkrender::command_pool& pool;
		uint32_t num_swapchain_images;

		vk::UniqueDescriptorSetLayout descriptor_layout;

		std::vector<std::vector<vkrender::vertex>> vertex_bufs;
		//std::vector<std::vector<uint32_t>> index_bufs;
		std::vector<vkrender::geometry_buffers> geo_bufs;
		std::vector<std::vector<vkrender::descriptor>> descriptors;
		std::vector<vkrender::descriptor_pool> descriptor_pools;

		std::vector<std::vector<vk::DescriptorPoolSize>> pool_sizes;

		std::vector<uniform_buffer> uniform_buffers;
		std::vector<glm::vec2> obj_size;
		std::vector<glm::vec3> obj_pos;
		size_t geo_bufs_obj_size() const
		{
			return geo_bufs.size() * OBJ_PER_BUF;
		}
		size_t next_object_id = 0;

		public:
			transient_object_manager(vkrender::command_pool& pool, uint32_t num_swapchain_images ) :
				p_device(pool.p_device),
				device(pool.device),
				pool(pool),
				num_swapchain_images(num_swapchain_images),
				descriptor_layout(my_descriptor_layout_bindings.get_descriptor_layout(device))
			{
				for (size_t i = 0; i < OBJ_PER_BUF; i++)
				{
					pool_sizes.push_back(my_descriptor_layout_bindings.get_pool_size_needed());
				}
			}

			auto get_descriptor_layouts()
			{
				return std::vector{descriptor_layout.get()};
			}
			void allocate_descriptor_pool()
			{
				descriptor_pools.emplace_back(device, pool_sizes, OBJ_PER_BUF);
				descriptors.emplace_back();
			}

			void new_frame()
			{
				next_object_id = 0;
			}
			void add_transient_object(backend_sprite_impl& sprite, mm::vec2 size,mm::rect sprite_src, mm::vec2r position)
			{
				auto& texture = sprite.texture;

				//convert the sprite src to normalized coordinates
				sprite_src.x /= sprite.w;
				sprite_src.w /= sprite.w;

				sprite_src.y /= sprite.h;
				sprite_src.h /= sprite.h;

				//and from here on out we're done with `sprite`.
				if (geo_bufs_obj_size() <= next_object_id ) //if we need to add a new pool:
				{
					//allocate vert/index bufs, set all to 0
					vertex_bufs.emplace_back(OBJ_PER_BUF * BOX_VERTICES_SIZE, vkrender::vertex{ {0,0,0}, {0,0,0}, {0,0}, {0,0,0} });
					//index_bufs.emplace_back(OBJ_PER_BUF * BOX_INDICES_SIZE, 0);

					//geo_bufs.emplace_back(pool, vertex_bufs.back(), index_bufs.back()); //box indices are always the same
					geo_bufs.emplace_back(pool, vertex_bufs.back(), BOX_INDICES);

					allocate_descriptor_pool();

				}
				size_t this_pool = next_object_id / OBJ_PER_BUF;

				/*{ //Not needed since we always have the same indices
					size_t start_pos = ((next_object_id % OBJ_PER_BUF) * BOX_INDICES_SIZE);
					for (auto i = 0; i < BOX_INDICES_SIZE; i++)
					{
						index_bufs[this_pool][start_pos+i] = BOX_INDICES[i];// + start_pos;
					}
				}*/
				{
					auto this_vertices = get_box_vertices(size.x,size.y, sprite_src); 
					size_t start_pos = ((next_object_id % OBJ_PER_BUF) * BOX_VERTICES_SIZE);

					for (auto i = 0; i < BOX_VERTICES_SIZE; i++)
					{
						vertex_bufs[this_pool][start_pos+i] = this_vertices[i];
					}
				}
				if (uniform_buffers.size() <= next_object_id)
				{
					uniform_buffers.emplace_back(p_device,device);
				}
				if (obj_size.size() <= next_object_id)
				{
					obj_size.push_back({});	
					obj_pos.push_back({});	
				}

				obj_size[next_object_id] = glm::vec2{size.x,size.y};
				obj_pos[next_object_id] = glm::vec3{position.x,position.y,position.r};

				{
					size_t descriptor_pos = next_object_id - (this_pool * OBJ_PER_BUF);

					if (descriptors[this_pool].size() <= descriptor_pos)
					{
						descriptors[this_pool].emplace_back(device, descriptor_layout.get(), descriptor_pools[this_pool]);
					}
					descriptors[this_pool][descriptor_pos].update(device, std::make_tuple //errors here probably mean the texture was destroyed too soon
					(
						vkrender::descriptor_info{0, uniform_buffers[next_object_id]}, 
						vkrender::descriptor_info{1, texture}
					));
				}

				next_object_id++;
			}
			size_t pool_count()
			{
				return ((next_object_id-1) / OBJ_PER_BUF)+1;
			}
			size_t objects_in_pool(size_t pool)
			{
				size_t max_id_this_pool = (pool+1)*OBJ_PER_BUF; 

				if ( next_object_id > max_id_this_pool)
				{
					return OBJ_PER_BUF;
				}
				return OBJ_PER_BUF - (max_id_this_pool - next_object_id);
			}
			void update_objs()
			{
				if (next_object_id != 0)
				{
					for (size_t i =0; i < pool_count(); i++)
					{
						geo_bufs[i].update_buffer(pool, vertex_bufs[i]);
						//geo_bufs[i].update_buffer(pool, index_bufs[i]);
					}
				}
			}
			inline auto get_bind_objects_fn()
			{
				return [this](vk::PipelineLayout& layout, vk::CommandBuffer& buffer, uint32_t /*this_swapchain_image*/)
				{
					update_objs();
					vk::DeviceSize no_offset = 0;
					if(next_object_id > 0)
					{
						buffer.bindIndexBuffer(geo_bufs[0].index_buffer.buffer.get(),0,vk::IndexType::eUint32);
						for (size_t this_pool = 0; this_pool < pool_count(); this_pool++)
						{
							buffer.bindVertexBuffers(0,1,&geo_bufs[this_pool].vertex_buffer.buffer.get(),&no_offset);
							for (size_t i = 0; i < objects_in_pool(this_pool); i++)
							{
								//                                                                 descriptorSetCount
								//                                                        firstset  |
								//                                                                | |                                        dynamicOffsetCount
								//                          pipelinebindpoint             layout  | |   pDescriptorSets                         |   pDynamicOffsets
								buffer.bindDescriptorSets(vk::PipelineBindPoint::eGraphics,layout,0,1,&descriptors[this_pool][i].descriptor_set,0,nullptr);
								//We don't add (BOX_INDICES_SIZE * i) for firstIndex since we just re-use the same indices.
								buffer.drawIndexed(BOX_INDICES_SIZE,1,0,(BOX_VERTICES_SIZE * i),0); 
								//               index cnt   instcnt| |firstindex |vertexOffset |firstInstance  
							}
						}
					}
				};
			}
			//TODO: This should really be on the uniform buffer type? Right?
			//	Our template is meaningless as it is though.
			inline auto get_update_objects_screen_position_fn(vkrender::viewport_info& info)
			{
				return [&](uint32_t /*current_image_index*/)
				{
					uniform_buffer_object ubo{};

					ubo.proj = glm::ortho( 0.0, static_cast<double>(info.get_extent().width), static_cast<double>(info.get_extent().height), 0.0, -1.0,1.0);

					for (size_t obj_id = 0; obj_id < next_object_id; obj_id++)
					{
						auto size = obj_size[obj_id];
						auto halfsize = glm::vec3{size.x/2.0,size.y/2.0,0};
						
						auto position3 = glm::vec3{obj_pos[obj_id].x, static_cast<double>(info.get_extent().height)-obj_pos[obj_id].y,0};
						ubo.model =  glm::translate(glm::rotate
						(
								glm::translate(glm::mat4(1.f),position3),
								obj_pos[obj_id].z,
								glm::vec3{0,0,1}
						),-halfsize);
						uniform_buffers[obj_id].buffer.transfer_c_data_into(&ubo,sizeof(ubo));
					}
				};
			}
	};

	struct tom_manager_t
	{
		vk::Device device;

		std::vector<transient_object_manager<std_uniform_buffer_object>> tom_for_swapchain_image;
		vkrender::swapchain& my_swapchain;
		vkrender::frame_drawer& drawer;

		inline tom_manager_t(vkrender::swapchain& my_swapchain, vkrender::frame_drawer& drawer, vkrender::command_pool& pool, uint32_t num_swapchain_images ) :
			device(pool.device),
			my_swapchain(my_swapchain),
			drawer(drawer)
		{
			for (uint32_t i = 0; i < num_swapchain_images; i++)
			{
				tom_for_swapchain_image.emplace_back(pool, num_swapchain_images);
			}
		}
		void add_transient_object(backend_sprite_impl& sprite, mm::vec2 size, mm::rect sprite_src, mm::vec2r position)
		{
			tom_for_swapchain_image[my_swapchain.current_image_index].add_transient_object(sprite,size,sprite_src,position);	
		}
		void new_frame()
		{
			device.waitForFences(1,&(drawer.drawing[my_swapchain.current_image_index].get()),true,std::numeric_limits<uint64_t>::max());

			tom_for_swapchain_image[my_swapchain.current_image_index].new_frame();
		}
		auto get_descriptor_layouts()
		{
			return tom_for_swapchain_image[0].get_descriptor_layouts();
		}
		inline auto get_bind_objects_fn()
		{
			return [this](vk::PipelineLayout& layout, vk::CommandBuffer& buffer, uint32_t this_swapchain_image)
			{
				tom_for_swapchain_image[my_swapchain.current_image_index].get_bind_objects_fn()(layout,buffer,this_swapchain_image);
			};
		}
		inline auto get_update_objects_screen_position_fn(vkrender::viewport_info& info)
		{
			return [&](uint32_t current_image_index)
			{
				tom_for_swapchain_image[my_swapchain.current_image_index].get_update_objects_screen_position_fn(info)(current_image_index);
			};
		}
	};


	class renderer
	{
		public:
			vkrender::renderer my_renderer;

			std::vector<vkrender::texture> my_textures;
			std::vector<vkrender::model> my_models;

			//std::unique_ptr<transient_object_manager<std_uniform_buffer_object>> tom;
			tom_manager_t tom_manager;
			algorithm::ecs_object<vkrender::full_object_pipeline> pipeline_transient;

		protected:

			friend class backend_sprite;

			std::mutex backend_mutex;

			std::unordered_map<size_t, backend_sprite_impl> backend_sprite_impls;
			size_t next_sprite_id = 0;

			//Each swapchain image has a list of doomed sprites. 
			//
			//If a sprite dies in a particular frame, it will be recorded inside the vector for that frame. 
			//The next time that frame is re-used (and all command buffers that might reference that sprite
			//are gone), the sprite will be deleted. So, they do tend to linger around for slightly longer
			//than neccessary.
			std::vector<std::vector<size_t>> doomed_sprites;
		private:
			void gc_sprites();
		public:
			renderer(mm::vec2 resolution, vkrender::res_mode_t res_mode, vkrender::fullscreen_mode_t fullscreen, std::string window_name);
			~renderer();
			void render();

			void change_resolution(mm::vec2 resolution, vkrender::res_mode_t res_mode, vkrender::fullscreen_mode_t fullscreen);
			mm::vec2 get_resolution();

			void begin_frame();

			void add_transient_object(backend_sprite& sprite, mm::vec2 size, mm::rect sprite_src, mm::vec2r position);
			

	};
}
