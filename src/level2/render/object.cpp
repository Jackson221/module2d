//Copyright 2017-2019 Jackson Reed McNeill. Licensed under the GNU AGPL version 3 or any later version.
#include "object.hpp"

#include <mutex>
#include <stack>

#include "render/sprite.hpp"
#include "render/camera.hpp"
#include "render/vkrender_backend.hpp"

#include "simulation/physics.hpp"

//TODO: De-globalize

#include "synchro/synchro.hpp"

namespace render
{
	
	object::object(engine_state& estate, std::string sprite_name, mm::vec2r position, mm::vec2 size, mm::rect animation) :
		rstate(estate.is_headless? nullptr : &*estate.rstate),
		sprite(estate.is_headless? nullptr : get_sprite(sprite_name)),
		size(size),
		animation(animation)
	{
	}

	void render_engine::render_obj(size_t id, object const & obj, camera* scene_camera)
	{
		mm::vec2r on_screen_position = scene_camera->apply_camera_offset(get_object_position(id));
		mm::vec2 on_screen_size = obj.size * scene_camera->scale;
		double diameter = std::max(on_screen_size.x,on_screen_size.y);

		mm::vec2 on_screen_position_point = static_cast<mm::vec2>(on_screen_position);

		if ( //is in view:
				on_screen_position_point + diameter >= mm::vec2{0,0}
				&& on_screen_position_point - diameter <= scene_camera->screen
		)
		{
			obj.rstate->api.add_transient_object(*obj.sprite, on_screen_size, obj.animation, on_screen_position);
		}
	}

	render_engine::render_engine(engine_state& estate, sim::physen& physen) :
		is_headless(estate.is_headless),
		render_object_lease(physen.type_monarch, "render_object"),
		render_accessible_synced_part
		(
			render_synchrothread_instance,
			render_synchrothread_instance.register_data_provider_object(physics_accessible_synced_part)
		)
	{
		if (!is_headless)
		{
			auto physics_tick_done_callback = [&physen, this]()
			{
				physics_accessible_synced_part= physen.position_component_lease.component_map;
				//push objects from physics accessible synced part -> render accessible synced part
				auto _ = std::lock_guard(render_mutex);
				render_synchrothread_instance.write_providers();
			};

			physen.add_tick_finished_callback(physics_tick_done_callback);
		}
	}


	
	mm::vec2r render_engine::get_object_position(size_t id)
	{
		//TODO: There really needs to be a better way of making sure that,
		//when a new render object is created, there will be the corresponding
		//position value already in render_accessible_synced_part. 
		//
		//This is essentially a temporary hack around assuring the above.
		auto it = render_accessible_synced_part->find(id);
		if (it != render_accessible_synced_part->end())
		{
			return it->position;
		}
		return {0,0,0};
	}
	void render_engine::render_objects(camera* scene_camera)
	{
		{
			auto _ = std::lock_guard(render_mutex);
			render_synchrothread_instance.write_readers();
		}
		//for (auto& object : render_object_lease.component_map)
		for (auto it = render_object_lease.component_map.begin(); it != render_object_lease.component_map.end(); it++)
		{
			render_obj(it.get_id(), *it, scene_camera);
		}
	}
}
