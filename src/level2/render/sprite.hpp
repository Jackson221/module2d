/*!
 *	@file
 *	@author Jackson McNeill
 *
 *	A sprite system
 *
 * Copyright 2017-2019 Jackson Reed McNeill. Licensed under the GNU AGPL version 3 or any later version.
 */
#pragma once


#include <string>
#include "render/state.hpp"

namespace render
{

	/*!
	 * Call before using the Sprite subsystem. Do not use the sprite subsystem without initializing it first.
	 *
	 * Interally, this loads the "ERR" sprite, from embedded memory. 
	 */
	void sprite_init(state& rstate);

	/*!
	 * Loads all sprites from the directory and the directories inside it.
	 */
	void load_all_sprites_in_dir(state& rstate, std::string directory);
	/*!
	 * Loads a sprite from a path
	 */
	void load_sprite(state& rstate, std::string path);

	backend_sprite* get_sprite(std::string name);

	/*!
	 * Frees all loaded sprites in the Sprite table (This is to be called on game shutdown)
	 */
	void sprite_shutdown();
}


