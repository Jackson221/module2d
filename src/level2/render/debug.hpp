/*!
 * @file
 * @author Jackson McNeill
 *
 * Debug-grade rstates for debugging. 
 * DO NOT USE WHILE PERFORMANCE TESTING! Debug renders have a tendancy to cause bad and inconsistant performance.
 		*
 * _PhysicsPosition functions do not account for rotation - they assume you will lock rotation during debugging.
 *
 * Copyright 2017-2019 Jackson Reed McNeill. Licensed under the GNU AGPL version 3 or any later version.
 */
#pragma once

#ifdef CONFIG_DEBUG_RENDERS_ENABLED
#define DEBUG_RENDERS_ENABLED() 1
#else
#define DEBUG_RENDERS_ENABLED() 0
#endif

#if DEBUG_RENDERS_ENABLED()

#include <string>
#include <optional>
#include <cstdio>
#include <mutex>
#include <memory>

//Local includes
#include "algorithm/tripple_buffer.hpp"
#include "UI/UI.hpp"
#include "UI/shapes.h"
#include "UI/font.h"
#include "render/camera.hpp"
#include "render/state.hpp"
#include "mathematics/primitives.hpp"
#include "mathematics/common_ops.hpp"
#include "mathematics/line.h"

namespace debug
{
	/*!
	 * Returns UNIX time in seconds.miliseconds
	 */
	double get_unix_time();

	const ui::color default_color = {0,255,255,255};//{0xff,0xff,0x99,0xFF};
	const ui::color default_message_color = {0,255,0,255};
	extern GE_Font debug_sans; 

	mm::vec2 physics_position_to_screen(mm::vec2 position);
	mm::vec2r physics_position_to_screen(mm::vec2r position);

	void cursor_text_at_cursor();

	void pass_uistate(ui::state uistate, render::camera* your_camera);

	class debug_renderable
	{
		class object_concept
		{
			public:
				virtual ~object_concept() {}
				virtual void render() = 0;
				virtual object_concept* _copy() =0;
		};
		template<class backend>
		class model : public object_concept
		{
			backend object;
			public:
				virtual ~model() {}
				void render()
				{
					object.render();	
				}
				model(backend my_backend) : 
					object(my_backend)
				{}
				object_concept* _copy()
				{
					return new model(*this);
				}

		};
		//std::unique_ptr<object_concept> my_held_object;
		object_concept* my_held_object;
		public:
			
			template<typename backend>
			debug_renderable(backend my_backend) :
				my_held_object(new model<backend>(my_backend))
			{
			}
			debug_renderable(debug_renderable && other) :
				my_held_object(other.my_held_object)
			{
				other.my_held_object = nullptr;
			}
			debug_renderable(debug_renderable const & other) :
				my_held_object(other.my_held_object->_copy())
			{
			}
			void render()
			{
				my_held_object->render();
			}
			~debug_renderable()
			{
				if (my_held_object != NULL)
				{
					delete my_held_object;
				}
			}
			debug_renderable & operator=(debug_renderable && other)
			{
				my_held_object = other.my_held_object;
				other.my_held_object = nullptr;

				return *this;
			}
			debug_renderable & operator=(debug_renderable const & other)
			{
				my_held_object = other.my_held_object->_copy();
				return *this;
			}
	};

	extern ui::state uistate;
	extern render::camera* camera; 
	extern bool has_been_passed;
	class sticky_object_pusher;
	class instance
	{
		algorithm::tripple_buffer<std::vector<debug_renderable>, std::mutex> buffer;
		algorithm::tripple_buffer_contiguous<std::vector<debug_renderable>, std::mutex> buffer_contiguous;
		size_t next_child_ID = 0;
		friend class sticky_object_pusher; 
		public:
			instance();
			~instance();

			void done_drawing();

			void render();
			template<typename backend>
			void push_object(backend object)
			{
				buffer.accessing_in_mutex.lock();
				buffer.in.emplace_back(object);
				buffer.accessing_in_mutex.unlock();
			}
			template<typename backend>
			void push_object_contiguous(backend object)
			{
				buffer_contiguous.accessing_in_mutex.lock();
				buffer_contiguous.in.emplace_back(object);
				buffer_contiguous.accessing_in_mutex.unlock();
			}
	};
	extern thread_local instance this_thread_instance;

	//sticky object stuff//////////////////////////////////////////////

	class _internal_sticky_object_struct
	{
		public:
			_internal_sticky_object_struct(debug_renderable && object, double duration, instance const * mother, size_t child_ID) :
				object(std::move(object)),
				end_time(get_unix_time()+duration),
				mother(mother),
				child_ID(child_ID)
			{
			}
			debug_renderable object;	
			double end_time;
			instance const * mother;
			size_t child_ID; //mother+child_ID pair is unique
	};
	extern std::vector<_internal_sticky_object_struct> _internal_sticky_object_vector;
	
	/*!
	 * This goes through the same process as non-sticky objects. However, instead of rendering something, it pushes its object
	 * into a buffer where it will persist until its time runs out. The holder object then dies immediatly after
	 */
	template<typename object_t>
	class _internal_sticky_object_holder
	{
		public:
			object_t object;
			double duration;
			instance const * mother;
			size_t child_ID; //mother+child_ID pair is unique

			/*!
			 * Does not actually render anything yet, but transfers the new object to the global buffer
			 */
			void render()
			{
				_internal_sticky_object_vector.push_back({std::move(object),duration,mother,child_ID});
			}
	};

	//position modifier stuff//////////////////////////////////////////////
	class null_position_modifier
	{
		public:
			template<typename T>
			static T modify(T const & in)
			{
				return in;
			}
	};
	class physics_position_modifier
	{
		public:
			template<typename T>
			static T modify(T const & in)
			{
				return physics_position_to_screen(in);
			}
	};

	//object pusher stuff//////////////////////////////////////////////
	class transient_object_pusher
	{
		public:
			template<typename object_t>
			void push_object(object_t && object)
			{
				this_thread_instance.push_object(std::move(object));
			}
	};
	class sticky_object_pusher
	{
		public:
			double duration;
			template<typename object_t>
			size_t push_object(object_t && object)
			{
				this_thread_instance.push_object_contiguous(_internal_sticky_object_holder<object_t>{std::move(object),duration,&this_thread_instance, this_thread_instance.next_child_ID});
				return this_thread_instance.next_child_ID++; //return the ID used and then increment
			}

	};

	//objects//////////////////////////////////////////////
	
	inline bool rotation_greater_than_180_deg(double rotation)
	{
		rotation = mm::canonicalize_rotation(rotation);
		return (rotation > M_PI) || (rotation < -M_PI);
	}
	enum class positioned_by_t
	{
		top_left,
		center
	};
	template<typename position_modifier>
	class text_at_object
	{
		public:
			std::string text;
			mm::vec2r position;
			ui::color color;
			positioned_by_t positioned_by;
			void render()
			{
				if (!has_been_passed)
				{
					return;
				}
				auto mytext = ui::text(uistate, text, color,debug_sans); //create a new text to render it once. debug-grade effeciency
				mytext.painting_boundries_rectangle.set_size({999999999,999999999});
				mytext.painting_boundries_rectangle.set_position({-9999,-9999});

				mytext.positioning_rectangle.set_modifier(ui::positioning_rectangle_t::modifiers::expand_size_to_fit,true);
				mytext.positioning_rectangle.set_modifier(ui::positioning_rectangle_t::modifiers::center_x,true);
				mytext.positioning_rectangle.set_modifier(ui::positioning_rectangle_t::modifiers::center_y,true);

				auto size = mytext.positioning_rectangle.get_size();
				auto final_position = position;
				if (positioned_by == positioned_by_t::top_left)
				{
					final_position += ((mm::vec2r{size.x,size.y,0}*0.5)/camera->scale);
				}
				auto pos_modified = position_modifier::modify(final_position);
				pos_modified.r *= 0.5; //dont know why this fixes things
				if (rotation_greater_than_180_deg(pos_modified.r+(M_PI/2.0)))
				{
					pos_modified.r -= M_PI;
				}
				mytext.hud_render(pos_modified);
			}
	};
	template<typename position_modifier_t, typename pusher_t>
	void text_at(pusher_t pusher, std::string text, mm::vec2r position, ui::color color = default_color, positioned_by_t positioned_by = positioned_by_t::top_left)
	{
		pusher.push_object(text_at_object<position_modifier_t>{text,position,color,positioned_by});
	}
	template<typename position_modifier_t, typename pusher_t>
	void text_at(pusher_t pusher, std::string text, mm::vec2 position, ui::color color = default_color, positioned_by_t positioned_by = positioned_by_t::top_left)
	{
		text_at<position_modifier_t>(pusher,text,static_cast<mm::vec2r>(position),color,positioned_by);
	}

////////

	extern thread_local std::vector<std::pair<ui::color,GE_RectangleShape>> instantiated_colors;
	
	template<typename position_modifier>
	class draw_line_object
	{
		public:
			mm::vec2 start;
			mm::vec2 end;
			ui::color color;
			void render()
			{
				if (!has_been_passed)
				{
					return;
				}
				mm::vec2 start_m = position_modifier::modify(start);
				mm::vec2 end_m = position_modifier::modify(end);

				if (start_m > mm::vec2{0,0} && end_m < camera->screen)
				{
					//really horrible linear search but it works, probably is ok for a debug UI
					auto color_it = std::find_if(instantiated_colors.begin(),instantiated_colors.end(), [this](std::pair<ui::color,GE_RectangleShape>& in){return in.first == color;});

					if (color_it == instantiated_colors.end())
					{
						instantiated_colors.push_back(std::make_pair(color, GE_RectangleShape{uistate.rstate, color}));
						color_it = instantiated_colors.end()-1;
					}
					auto& line = color_it->second;
					line.render(start_m,end_m,1);
				}
			}
	};
	template<typename position_modifier_t, typename pusher_t>
	void draw_line(pusher_t pusher, mm::vec2 start, mm::vec2 end, ui::color color = default_color)
	{
		pusher.push_object(draw_line_object<position_modifier_t>{start,end,color});
	}
	template<typename position_modifier_t, typename pusher_t>
	void draw_line_relative_position(pusher_t pusher, mm::vec2 start, mm::vec2 end, ui::color color = default_color)
	{
		draw_line<position_modifier_t>(pusher,start,start+end,color);
	}
	template<typename position_modifier_t, typename pusher_t>
	void draw_normal_line_relative_position(pusher_t pusher, mm::vec2 start, mm::vec2 end, ui::color color = default_color)
	{
		draw_line<position_modifier_t>(pusher,start,start+(mm::unit_vector(end)*mm::vec2{100,100}),color);
	}
////////
	template<typename pusher_t>
	inline void _draw_shape_internal(pusher_t pusher,GE_ShapePoints shape,ui::color color, std::function<void(pusher_t,mm::vec2,mm::vec2,ui::color)> draw_line_callback)
	{
		auto lastSpot = shape.begin();
		for (auto spot = shape.begin()+1;spot!= shape.end();spot++)
		{
			draw_line_callback(pusher, *lastSpot,*spot,color);
			lastSpot = spot;
		}
	}

	template<typename position_modifier_t, typename pusher_t>
	inline void draw_shape(pusher_t pusher, GE_ShapePoints shape, ui::color color = default_color)
	{
		_draw_shape_internal(pusher, shape, color, draw_line<position_modifier_t,pusher_t>);
	}
////////
	template<typename pusher_t>
	inline void _draw_rect_internal(pusher_t pusher, mm::rectr rect, ui::color color, std::function<void(pusher_t pusher, GE_ShapePoints,ui::color)> draw_shape_callback)
	{
		GE_ShapePoints pts(4);	
		GE_RectangleToPoints(rect, {0,0}, pts.data(), {0,0,0});
		GE_ShapePoints shapeLines = {};
		shapeLines.insert(shapeLines.end(),pts[0]);
		shapeLines.insert(shapeLines.end(),pts[1]);
		shapeLines.insert(shapeLines.end(),pts[3]);
		shapeLines.insert(shapeLines.end(),pts[2]);
		shapeLines.insert(shapeLines.end(),pts[0]); 
		draw_shape_callback(pusher, shapeLines,color);
	}
	template<typename position_modifier_t, typename pusher_t>
	inline void draw_rect(pusher_t pusher, mm::rectr rect, ui::color color = default_color)
	{
		_draw_rect_internal(pusher, rect, color, draw_shape<position_modifier_t,pusher_t>);
	}
	template<typename position_modifier_t, typename pusher_t>
	inline void draw_rect(pusher_t pusher, mm::rect rect, ui::color color = default_color)
	{
		draw_rect<position_modifier_t>(pusher,static_cast<mm::rectr>(rect),color);
	}
	template<typename position_modifier_t, typename pusher_t>
	inline void draw_rect(pusher_t pusher, mm::vec2 position, mm::vec2 size, ui::color color = default_color)
	{
		draw_rect<position_modifier_t>(pusher,mm::rectr{position.x,position.y,0,size.x,size.y},color);
	}
	template<typename position_modifier_t, typename pusher_t>
	inline void draw_rect(pusher_t pusher, mm::vec2r position, mm::vec2 size, ui::color color = default_color)
	{
		draw_rect<position_modifier_t>(pusher,mm::rectr{position.x,position.y,position.r,size.x,size.y},color);
	}
////////
	template<typename position_modifier_t, typename pusher_t>
	inline void draw_arrow(pusher_t pusher, mm::vec2 start, mm::vec2 end, ui::color color = default_color)
	{

		mm::vec2 const delta = end-start;
		double constexpr arrow_degrees = 35;
		double const arrow_angle = -std::atan2(delta.y,delta.x)+(M_PI/2.0);
		double const arrow_head_magnitude = 25.0;

		mm::vec2 arrow_head_normal_1 = {0,-1};
		mm::vec2 arrow_head_normal_2 = {0,-1};
		arrow_head_normal_1 = mm::rotation_ccw(arrow_head_normal_1, arrow_angle+(arrow_degrees / mm::RAD_TO_DEG));
		arrow_head_normal_2 = mm::rotation_ccw(arrow_head_normal_2, arrow_angle-(arrow_degrees / mm::RAD_TO_DEG));

		draw_line<position_modifier_t>(pusher,start,end,color);	
		draw_line<position_modifier_t>(pusher,end,end + (arrow_head_normal_1*arrow_head_magnitude),color);	
		draw_line<position_modifier_t>(pusher,end,end + (arrow_head_normal_2*arrow_head_magnitude),color);	
	}
////////
	size_t _on_screen_message_internal(ui::color color, double duration, std::string msg);

	template<typename ... T>
	size_t message(ui::color color, double duration, std::string formatstr, T ... print_objects)
	{
		//All messages are assumed to be originating from trusted strings -- so we will disable the warnings here.
		#pragma GCC diagnostic push
		#pragma GCC diagnostic ignored "-Wformat-security"
		size_t size_needed = snprintf(nullptr,0,formatstr.c_str(), print_objects...);	
		if (size_needed > 0)
		{
			std::string buffer(size_needed+1,'Q');
			snprintf(buffer.data(),buffer.size(),formatstr.c_str(),print_objects...);
			return _on_screen_message_internal(color,duration,buffer);
		}
		else
		{
			printf("on_screen_message - formatting error %lu\n",size_needed);
			printf("(formatstr: %s\n",formatstr.c_str());
			return 0;
		}
		#pragma GCC diagnostic pop
	}
	template<typename ... T>
	size_t message(double duration, std::string formatstr, T ... print_objects)
	{
		return message(default_message_color,duration,formatstr, print_objects...);
	}

	void render_sticky_objects();
}

#endif //DEBUG_RENDERS_ENABLED
