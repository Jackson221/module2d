/*!
 * @file
 * @author Jackson McNeill
 *
 * A simple object designed to be linked to a networkObject or physicsObject.
 *
 * Can only be created by the physics engine, and only rendered by the rendering engine.
 *
 * They are added inside the physics thread without any form of locking. This is because the list "end" position is not updated until the end of a physics tick 
 *
 * Copyright 2017-2019 Jackson Reed McNeill. Licensed under the GNU AGPL version 3 or any later version.
 */
#pragma once

#include <mutex>

#include "mathematics/primitives.hpp"
#include "synchro/synchro.hpp"
#include "render/vkrender_backend.hpp"
#include "render/state.hpp"
#include "engine.hpp"

#include "simulation/physics.hpp"
#include "synchrothread/synchrothread.hpp"
#include "container/idmap.hpp"



namespace render
{	


	struct camera;
	
	struct object
	{
		render::state* const rstate;
		backend_sprite* const sprite;
		mm::vec2 size;
		mm::rect animation;

		using enumeration = synchro::enumeration<synchro::treat_pointer_as_integral_type<&object::rstate>{},
			synchro::treat_pointer_as_integral_type<&object::sprite>{},
			&object::size,
			&object::animation
		>;
		SYNCHRO_OPERATOR_EQUALS_FOR_SNAPSHOT();
		
		object(engine_state& estate, std::string sprite_name, mm::vec2r position = {0,0,0}, mm::vec2 size = {0,0}, mm::rect animation = {0,0,0,0});

		SYNCHRO_ONLY_CONSTRUCTOR(object, rstate,sprite,size,animation)
	};

	/*!
	 * represents a render object in the queue to be synced
	class object_tracker
	{
		render_engine* engine;
		public:
			size_t id;

			object_tracker(render_engine& in_engine, render::state* rstate, std::string sprite_name, mm::vec2r position = {0,0,0}, mm::vec2 size = {0,0}, mm::rect animation = {0,0,0,0});
			~object_tracker();

			object& operator*();
			object* operator->();

			object_tracker& operator=(object_tracker const & ) = delete;
			object_tracker& operator=(object_tracker && other);

			object_tracker(object_tracker const & ) = delete;
			object_tracker(object_tracker && other);
			
	
	};
	 */
	//TODO list should sync by deltas, not by sending a new list every single time
	class render_engine
	{
		using object_list_t = container::idmap<sim::position_component>;
		bool is_headless;
		public:
			container::idmap<sim::position_component> physics_accessible_synced_part;
			synchrothread::instance< std::tuple< object_list_t > > render_synchrothread_instance;
			synchrothread::synced_object<object_list_t, decltype(render_synchrothread_instance) > render_accessible_synced_part;
			

			
			container::type_lease<object> render_object_lease;

			void render_obj(size_t id, object const & obj, camera* scene_camera);

			std::mutex render_mutex;

			render_engine(engine_state& estate, sim::physen& physen);
			void render_objects(camera* scene_camera);
			mm::vec2r get_object_position(size_t id);

			render_engine(render_engine &&) = delete;
			render_engine(render_engine const &) = delete;

			render_engine& operator=(render_engine &&) = delete;
			render_engine& operator=(render_engine const &) = delete;
	};
}
