#include "vkrender_backend.hpp"

#include "input/schedule_events.hpp"
#include "filesystem/filesystem.hpp"

namespace render
{
	backend_sprite_impl::backend_sprite_impl(renderer& my_renderer, SDL_Surface* surface, bool free) :
		w(surface->w),
		h(surface->h),
		texture(my_renderer.my_renderer.my_command_pool,surface, vkrender::texture::filter::nearest,true,free)
	{
	}
	backend_sprite_impl::backend_sprite_impl(renderer& my_renderer,uint32_t w, uint32_t h, vkrender::texture texture) :
		w(w),
		h(h),
		texture(std::move(texture))
	{
	}

	backend_sprite::backend_sprite(renderer& my_renderer, SDL_Surface* surface, bool free) : 
		renderer_ptr(&my_renderer)
	{
		//auto start_time = std::chrono::steady_clock::now();
		auto bsi = backend_sprite_impl(my_renderer,surface,free);
		/*auto finish_time = std::chrono::steady_clock::now();
		auto time_took_to_tick = finish_time - start_time;
		float res = std::chrono::duration_cast<std::chrono::nanoseconds>(time_took_to_tick).count()/1000000.0;
		printf("(text coeffecient %f of frame)\n", res/((1/144.0)*1000));*/
		auto _ = std::lock_guard(my_renderer.backend_mutex);
		id = my_renderer.next_sprite_id;
		my_renderer.backend_sprite_impls.insert(std::make_pair(my_renderer.next_sprite_id++, std::move(bsi)));
	}
	backend_sprite::backend_sprite(renderer& my_renderer, uint32_t w, uint32_t h, vkrender::texture texture) :
		renderer_ptr(&my_renderer)
	{
		auto _ = std::lock_guard(my_renderer.backend_mutex);
		id = my_renderer.next_sprite_id;
		my_renderer.backend_sprite_impls.insert(std::make_pair(my_renderer.next_sprite_id++, backend_sprite_impl(my_renderer, w, h, std::move(texture))));
	}
	backend_sprite::~backend_sprite()
	{
		if(id != invalid_id)
		{
			auto _ = std::lock_guard(renderer_ptr->backend_mutex);
			renderer_ptr->doomed_sprites[renderer_ptr->my_renderer.my_swapchain.current_image_index].push_back(id);
		}
	}

	



	auto pos = glm::vec2{0,0};
	auto vel = glm::vec2{3,3};

	renderer::renderer(mm::vec2 resolution, vkrender::res_mode_t res_mode, vkrender::fullscreen_mode_t fullscreen, std::string window_name):
		my_renderer({resolution.x, resolution.y}, res_mode, fullscreen, window_name, 0/*, {vkrender::pipeline_descriptor{ "bin/spirv/2dvert.spv" ,  "bin/spirv/2dfrag.spv" }}*/),
		tom_manager(my_renderer.my_swapchain, my_renderer.my_frame_drawer,  my_renderer.my_command_pool, my_renderer.num_swapchain_images),
		pipeline_transient(my_renderer.create_pipeline(vkrender::full_object_pipeline_descriptor{
					"spirv/transientvert.spv" ,  "spirv/transientfrag.spv", std::vector{tom_manager.get_descriptor_layouts()},
					tom_manager.get_bind_objects_fn(), tom_manager.get_update_objects_screen_position_fn(*(my_renderer.my_viewport_info))}))
	{
		doomed_sprites.resize(my_renderer.num_swapchain_images);	
	}
	renderer::~renderer()
	{
		my_renderer.device->waitIdle();
	}
	void renderer::change_resolution(mm::vec2 resolution, vkrender::res_mode_t res_mode, vkrender::fullscreen_mode_t fullscreen)
	{
		auto _ = std::lock_guard(backend_mutex);
		my_renderer.change_resolution({resolution.x,resolution.y}, res_mode,fullscreen);
	}
	mm::vec2 renderer::get_resolution()
	{
		auto v = my_renderer.get_resolution();
		return {v.x,v.y};
	}
	void renderer::begin_frame()
	{
		{
			auto _ = std::lock_guard(backend_mutex);
			my_renderer.start_frame();
			tom_manager.new_frame();
		}
		gc_sprites(); 
	}
	void renderer::render()
	{
		auto _ = std::lock_guard(backend_mutex);
		my_renderer.refresh_objects();
		my_renderer.end_frame();
	}
	/*
	algorithm::ecs_object<sprite> renderer::create_sprite(vkrender::texture& texture, mm::vec2 size, mm::vec2r position)
	{
		auto&& spr = sprite(my_renderer, my_object_support, pipeline2d_objects, size.x, size.y, texture);
		spr.set_position(position, size);
		return {*my_sprites, my_sprites->push_back(std::move(spr))};
	}
	*/
	void renderer::add_transient_object(backend_sprite& sprite, mm::vec2 size, mm::rect sprite_src, mm::vec2r position)
	{
		auto _ = std::lock_guard(backend_mutex);
		tom_manager.add_transient_object(backend_sprite_impls.at(sprite.id),size,sprite_src,position);
	}
	void renderer::gc_sprites()
	{
		auto _ = std::lock_guard(backend_mutex);
		for (size_t doomed_id : doomed_sprites[my_renderer.my_swapchain.current_image_index])
		{
			backend_sprite_impls.erase(doomed_id);
		}
		doomed_sprites[my_renderer.my_swapchain.current_image_index].clear();
	}
}
