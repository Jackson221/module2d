#include "render/state.hpp"
namespace render
{
	state::state(mm::vec2 resolution, vkrender::res_mode_t res_mode, vkrender::fullscreen_mode_t fullscreen, std::string window_name) : 
		api(resolution,res_mode,fullscreen,window_name),
		data(api.get_resolution(),res_mode,60,fullscreen)
	{
		//TODO
		SDL_DisplayMode dm;
		if (SDL_GetDesktopDisplayMode(0, &dm) == 0) 
			data.refresh = dm.refresh_rate;
	}
	void state::change_resolution(mm::vec2 resolution, vkrender::res_mode_t res_mode, vkrender::fullscreen_mode_t fullscreen)
	{
		api.change_resolution(resolution,res_mode,fullscreen);
		data = {api.get_resolution(),res_mode,60,fullscreen};

		resolution_change_manager.dump_events({data},nullptr);
		//
		//TODO
		SDL_DisplayMode dm;
		if (SDL_GetDesktopDisplayMode(0, &dm) == 0) 
			data.refresh = dm.refresh_rate;
	}
	mm::vec2 state::get_resolution()
	{
		return api.get_resolution();
	}
}
