//Copyright 2017-2019 Jackson Reed McNeill. Licensed under the GNU AGPL version 3 or any later version.
#include "sprite.hpp"


#include <SDL.h>
#include <SDL_image.h>
#include <unordered_map>
#include <filesystem>

#include "filesystem/filesystem.hpp"

#include "render/vkrender_backend.hpp"

namespace render
{

	std::unordered_map<std::string,backend_sprite> sprites;



	/*!
	 * This is an embedded monocolor BMP image, used in place of a missing file. This needs to be embedded, because it HAS to be available. 
	 */
	unsigned char ERR_bitmap[] = {66,77,82,0,0,0,0,0,0,0,62,0,0,0,40,0,0,0,11,0,0,0,5,0,0,0,1,0,1,0,0,0,0,0,20,0,0,0,196,14,0,0,196,14,0,0,2,0,0,0,2,0,0,0,255,255,255,0,0,0,255,0,23,96,0,0,119,96,0,0,53,64,182,0,113,10,0,0,23,118,226,219};

	void sprite_init(state& rstate)
	{
		IMG_Init(IMG_INIT_JPG | IMG_INIT_PNG | IMG_INIT_TIF);
		atexit(IMG_Quit);
		SDL_RWops* MissingImage_RWops = SDL_RWFromMem(ERR_bitmap, sizeof(ERR_bitmap));
		SDL_Surface* surface = IMG_Load_RW(MissingImage_RWops,0); 

		sprites.insert(std::make_pair("ERR",render::backend_sprite{rstate.api,surface}));
	}

	void load_all_sprites_in_dir(state& rstate, std::string directory)
	{
		std::filesystem::path path = filesystem::host_os_path_format(directory);

		for (auto& d : std::filesystem::recursive_directory_iterator(path))
		{
			auto sprite_path = d.path();
			if (std::filesystem::is_regular_file(sprite_path))
			{
				load_sprite(rstate, std::filesystem::absolute(sprite_path).string().c_str());
			}
		}
	}
	void load_sprite(state& rstate, std::string path)
	{
		path = filesystem::generic_path_format(path);
		auto new_sprite = backend_sprite{rstate.api,IMG_Load(filesystem::host_os_path_format(path).c_str())}; 

		path = filesystem::get_filename_without_extension(filesystem::get_base_filename(path));
		sprites.insert(std::make_pair(path,std::move(new_sprite)));
	}
	backend_sprite* get_sprite(std::string name)
	{
		try
		{
			return &sprites.at(name);
		}
		catch (const std::out_of_range& e)
		{
			return &sprites.at("ERR");
		}
	}

	void sprite_shutdown()
	{
		sprites.clear();
	}
}
