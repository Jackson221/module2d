/*!
 * @file
 * @author Jackson McNeill
 *
 * A small Camera stucture and a function to apply offset based upon it 
 *
 * Copyright 2017-2019 Jackson Reed McNeill. Licensed under the GNU AGPL version 3 or any later version.
 */
#pragma once

#include <math.h>


#include "mathematics/primitives.hpp"


namespace render
{
	/*!
	 * A camera, which contains the position(including rotation) of the camera, and the screen height and width
	 */
	struct camera
	{
		mm::vec2r pos = {0,0,0}; 
		double scale = 1.0;
		mm::vec2 screen;

		mm::vec2r apply_camera_offset(mm::vec2r subject);
		mm::vec2r negate_camera_offset(mm::vec2r subject);
	};
};


