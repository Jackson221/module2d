/*!
 * @file
 * @author Jackson McNeill
 *
 * A system that allows for repeating stars to be created, which aids the player in telling how fast they're moving in an outer space environment
 *
 * Copyright 2017-2019 Jackson Reed McNeill. Licensed under the GNU AGPL version 3 or any later version.
 */
#pragma once

#include <unordered_set>
#include <vector>
#include <future>
#include <memory>
#include <mutex>

#include "mathematics/primitives.hpp"
#include "algorithm/thread_pool.hpp"
#include "UI/UI.hpp"
#include "UI/color.hpp"
#include "render/camera.hpp"
#include "vkrender/texture.hpp"
#include "vkrender/command_pool.hpp"
#include "algorithm/frame.hpp"

struct SDL_Surface;
namespace render
{
	struct single_star;
	struct rendering_surface;

	class stars : public ui::element
	{
		public:
			stars(ui::state uistate, unsigned int number, unsigned int width, unsigned int height,std::vector<int> sizes, std::vector<ui::color> colors, render::camera* camera, double high_dist, double low_dist, std::chrono::nanoseconds ideal_tick_length);
			~stars();
			stars(stars const &) = delete;
			stars(stars &&) = delete;

			void impl_render() override;

		private:
			std::chrono::nanoseconds ideal_tick_length;
			std::optional<algorithm::framerate_guard> framerate_guard;

			vkrender::command_pool star_texture_creation;
			std::vector<single_star> single_stars;
			std::vector<ui::color> star_colors;
			mm::vec2 star_plate_size;
			render::camera* camera;

			mm::vec2 last_size = {0,0};

			SDL_Surface* surface = nullptr;
			SDL_Surface* submit_surface = nullptr;
			SDL_Surface* black_surface = nullptr;

			std::optional<render::backend_sprite> rendered_sprite = std::nullopt;
			std::unique_ptr<vkrender::texture> rendered_sprite_working_space = nullptr;

			std::mutex rendered_sprite_mutex;
			std::mutex submit_surface_mutex;

			render::camera star_visible_camera;
			render::camera star_visible_camera_in;
			std::mutex star_visible_camera_in_mutex;

			std::future<void> submit_future;
			bool first_time = true;

			bool alive = true;

			//TODO: Duct Tape Class C-1-2 star_weird_race
			std::mutex tmpm;
			std::atomic<int32_t> first = 0;
			/////

			algorithm::thread_pool<std::monostate> submit_pool;
			algorithm::thread_pool<rendering_surface,false,true> tp;
	};
}


