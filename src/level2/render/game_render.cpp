//Copyright 2017-2019 Jackson Reed McNeill. Licensed under the GNU AGPL version 3 or any later version.
#include "game_render.hpp"

//Local includes
#include "render/object.hpp"

namespace ui
{
	game_render::game_render(state uistate, render::render_engine& in_engine, render::camera* camera) :
		element(uistate),
		engine(&in_engine)
	{
		this->camera = camera;
		positioning_rectangle.set_update_callback([this,camera]()
		{
			camera->screen = positioning_rectangle.get_size();
		});
	}
	void game_render::impl_render() 
	{
		engine->render_objects(camera);
	}
}
