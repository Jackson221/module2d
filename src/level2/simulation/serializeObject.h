/*!
 * @file
 * @author Jackson McNeill
 * Allows for easy serialization of physics objects with multiple class types
 *
 * Copyright 2017-2019 Jackson Reed McNeill. Licensed under the GNU AGPL version 3 or any later version.
 */
#pragma once

//New code

#include "synchro/enumeration.hpp"
#include "synchro/journaling_container.hpp"
#include <memory>
#include <unordered_map>
#include <map>
namespace simulation
{
	struct synchronization_version;

	std::unique_ptr<synchronization_version> get_initial_version();

	class object_serialization_tracker_version_interface
	{
		virtual ~object_serialization_tracker_version_interface() {}
	};
	class object_serialization_tracker_interface
	{
		virtual synchro::serialization_tape serialize(object_serialization_tracker_version_interface* version) = 0;

		virtual void update(synchro::serialization_tape in_tape) = 0;
	};

	using object_id_t = synchro::portable_size_t;

	//one tracker per object type -- store them as their interface in a vector
	template<typename object_t>
	class object_serialization_tracker : public object_serialization_tracker_interface
	{
			using object_container_t = synchro::journaling_container<std::monostate, std::map, object_id_t, object_t>;

			using serialization_type = synchro::initial_snapshot<object_t>;
			using version_id_t = typename serialization_type::version_id_tuple_t;

			std::unordered_map<object_id_t, synchro::serialization_tape> object_tapes;
			std::unordered_map<object_id_t, version_id_t> last_saved_version;

			std::unordered_map<object_id_t, object_t*> objects;
			object_id_t next_object_id = 0;

		protected:
			
			object_id_t add_object(object_t* object)
			{
				objects.insert(std::make_pair(next_object_id, object));
				return next_object_id++;
			}
			void remove_object(object_id_t id)
			{
				objects.erase(id);
			}
		public:
			class tracker
			{
				object_serialization_tracker host_tracker;
				object_id_t id;
				public:
					tracker(object_serialization_tracker host_tracker, object_t* object) :
						host_tracker(host_tracker),
						id(host_tracker.add_object(object))
					{
					}
					~tracker()
					{
						host_tracker.remove_object(id);
					}
			};
			friend class tracker;
			struct version_t : public object_serialization_tracker_version_interface
			{
				version_id_t version_id;
			};

			synchro::serialization_tape serialize(object_serialization_tracker_version_interface* in_version) override
			{
				version_t* version = dynamic_cast<version_t>(in_version);

				//auto snapshot = 
			}

			void update(synchro::serialization_tape in_tape) override
			{

			}

	};

}
