/*!
 * Provides networking abilites to the simulation
 *
 * Copyright 2019 Jackson Reed McNeill. Licensed under the GNU AGPL version 3 or any later version.
 */
#pragma once

#include "event/event.hpp"
#include "network/network.hpp"
#include "container/idmap.hpp"

#include <string>
#include <vector>
#include <map>
#include <unordered_map>
namespace netplay
{

	using client_id_t = unsigned long long;
	using interface_id_t = unsigned long long;


	struct network_interface_event
	{
		interface_id_t event_id;
		//serialization::serialization_state serialized_event;
		std::vector<client_id_t> event_client_id;
	};

	//TODO allow de-registering handlers
	/*
	using networked_serialization_handler_uniform_id_t = std::function<void(client_id_t client_id_originating_from, serialization::unserialization_state &)>;

	using networked_serialization_handler_nonuniform_id_t = std::function<void(std::vector<client_id_t> client_id_originating_from, serialization::unserialization_state &)>;

	interface_id_t register_networked_serialization_handler(networked_serialization_handler_uniform_id_t && func_uniform, networked_serialization_handler_nonuniform_id_t && func_nonuniform, std::string const unique_id_string);

	void change_networked_serialization_handler(interface_id_t const id,networked_serialization_handler_uniform_id_t && func_uniform, networked_serialization_handler_nonuniform_id_t && func_nonuniform);
	*/

	void remove_networked_serialization_handler(interface_id_t const id, std::string const unique_id_string);

	extern std::vector<network_interface_event> queued_network_interface_events;


	extern client_id_t my_client_id;
	extern std::unordered_map<client_id_t,std::string> client_names;

	enum class state
	{
		offline,
		server,
		client
	};
	extern state net_state;


	template<typename _event_data_t>
	struct networkable_event_data_t
	{
		using event_data_t = _event_data_t;
		event_data_t event_data;
		client_id_t client_id = my_client_id;
	};

	//TODO: non bi-directionality options in template arguments for network_interface
	template<typename _event_event_configuration_types_t_t, typename _mutex_t = std::mutex>
	class network_interface
	{
		public:
			using mutex_t = _mutex_t;
			using event_types_t_t = _event_event_configuration_types_t_t;
			using event_types_t = typename _event_event_configuration_types_t_t::configuration_types_t;
			using queued_events_t = typename event_types_t::queued_events_t;
			using event_data_t = typename event_types_t::event_data_t;

			using manager_t = typename event_types_t_t::manager_t;

			queued_events_t events_queued_for_networking;
			mutex_t events_queued_for_networking_mutex;

			queued_events_t events_queued_for_event;

			/*!
			 * Adds a new event that will be sent out over the network, 
			 * but not sent to other event interface on this same
			 * computer.
			 */
			void push_event(event_data_t data)
			{
				events_queued_for_networking_mutex.lock();
				events_queued_for_networking.push_back(data);	
				events_queued_for_networking_mutex.unlock();
			}
			void dump_events(queued_events_t in)
			{
				events_queued_for_networking_mutex.lock();
				events_queued_for_networking.insert(events_queued_for_networking.end(),in.begin(),in.end());
				events_queued_for_networking_mutex.unlock();
			}
			/*!
			 * Send events to other computers
			 */
			void send_events()
			{
				//We can't send things to other computers if we are offline
				//Also, without this check a recourse leak will occur for all offline clients.
				if (net_state != state::offline)
				{
					/*
					serialization::serialization_state state(0);
					serialization::serialize(static_cast<serialization::size_tp>(events_queued_for_networking.size()),state);
					auto&& client_id_vector = std::vector<client_id_t>();
					client_id_vector.reserve(events_queued_for_networking.size());
					for (auto& data : events_queued_for_networking)
					{
						serialization::serialize(&data.event_data,state);
						client_id_vector.push_back(data.client_id);
					}
					queued_network_interface_events.push_back(network_interface_event{id,std::move(state),std::move(client_id_vector)});
					events_queued_for_networking.clear();
					*/
				}
			}
			/*!
			 * Take all the events received from other computers and send them to all other interfaces
			 */
			void dispatch_events()
			{
				if (!events_queued_for_event.empty())printf("dispatching %ld events\n",events_queued_for_event.size());
				pointer_to_manager->dump_events(events_queued_for_event, this);	
				events_queued_for_event.clear();
			}
		private:
			interface_id_t id;
			//event_t may be an incomplete type during some instantiations of this template (like the one that event_t itself uses)
			//However, it is okay for have a pointer to an incomplete type.
			manager_t* pointer_to_manager;

			//These two handlers exists because a of a need to both:
			// * Trust the server that messages are coming from a specific client
			// * Not trust a client to accurately submit out their ID and not someone else's
			struct _serialization_handler_uniform
			{
				network_interface* host;
				/*void operator()(client_id_t id_originating_from, serialization::unserialization_state & state)
				{
					serialization::size_tp number_of_events = serialization::unserialize<serialization::size_tp>(state);
					host->events_queued_for_event.reserve(static_cast<size_t>(number_of_events));

					if (number_of_events > 0) printf("got %llu events\n",number_of_events);

					for (serialization::size_tp i = 0; i < number_of_events; i++)
					{
						auto&& event_data = serialization::unserialize<typename event_data_t::event_data_t>(state);
						host->events_queued_for_networking.push_back({event_data,id_originating_from}); //TODO Temporary hack to relay client messages (client->server->other clients)
						host->events_queued_for_event.push_back({std::move(event_data),id_originating_from});
					}
				}*/
			};
			struct _serialization_handler_nonuniform
			{
				network_interface* host;
				/*void operator()(std::vector<client_id_t> id_originating_from, serialization::unserialization_state & state)
				{
					serialization::size_tp number_of_events = serialization::unserialize<serialization::size_tp>(state);
					host->events_queued_for_event.reserve(static_cast<size_t>(number_of_events));

					if (number_of_events > 0) printf("got %llu events\n",number_of_events);

					for (serialization::size_tp i = 0; i < number_of_events; i++)
					{
						auto&& event_data = serialization::unserialize<typename event_data_t::event_data_t>(state);
						host->events_queued_for_event.push_back({std::move(event_data),id_originating_from[i]});
					}
				}*/
			};
		public:
			network_interface(manager_t & manager) : 
				//id(register_networked_serialization_handler(_serialization_handler_uniform{this},_serialization_handler_nonuniform{this},manager.name)),
				pointer_to_manager(&manager)
			{
				manager.push_interface(this);
			}
			~network_interface()
			{
				pointer_to_manager->remove_interface(this);
			}
			network_interface & operator=(network_interface && other)
			{
				id = std::move(other.id);
				pointer_to_manager = std::move(other.pointer_to_manager);
				pointer_to_manager->push_interface(this);
				change_networked_serialization_handler(id,_serialization_handler_uniform{this},_serialization_handler_nonuniform{this});
				return *this;
			}
			network_interface(network_interface && other)
			{
				id = std::move(other.id);
				pointer_to_manager = std::move(other.pointer_to_manager);
				pointer_to_manager->push_interface(this);
				change_networked_serialization_handler(id,_serialization_handler_uniform{this},_serialization_handler_nonuniform{this});
			}
			network_interface & operator=(network_interface const &) = delete;
			network_interface(network_interface const &) = delete;
	};

	template<typename my_configuration_types_t>
	struct event_configuration_types_plus_network_interface
	{
		using manager_t = event::manager<event_configuration_types_plus_network_interface>;
		using configuration_types_t = my_configuration_types_t;
		using interface_types_tuple_t = std::tuple<event::endpoint_interface<event_configuration_types_plus_network_interface>,network_interface<event_configuration_types_plus_network_interface>>;
	};

	
		
	extern std::string nickname;

	/*!
	 * Stops all connections and clears the client list
	 *
	 * May also be called in offline mode upon game exit, to clear the client list (which will have 1 entry, for the offline mode player)
	 *
	 * Must be called with physics engine mutex locked
	 */
	void stop_connections();

	/*!
	 * Must be called with physics engine mutex locked
	 */
	void begin_offline_mode();

	void start_server(unsigned short port);
	void join_server(unsigned short port, std::string hostname);

	void _physics_pre_callback();
	void _physics_post_callback();

	void init();
	void shutdown();


	namespace new_name_event
	{
		struct event_data_t
		{
			client_id_t id;
			std::string name;
		};
		using net_data_t = netplay::networkable_event_data_t<event_data_t>;
		using event_configuration_types = netplay::event_configuration_types_plus_network_interface<event::event_configuration_types<net_data_t>>;
		using manager_t = event_configuration_types::manager_t;
		using interface_t = event::endpoint_interface<event_configuration_types>;

		extern manager_t manager;
		extern thread_local interface_t interface;
		extern interface_t * netplay_thread_interface;
	}
}
