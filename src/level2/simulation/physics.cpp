//Copyright 2017-2019 Jackson Reed McNeill. Licensed under the GNU AGPL version 3 or any later version.
#include "physics.hpp"

#include <cmath>
#include <unistd.h>
#include <cstdio>
#include <sys/time.h>
#include <chrono>
#include <thread>
#include <iostream>
#include <future>
#include <map>
#include <optional>
#include <unordered_set>

//Local includes

#include "mathematics/collision.h"
#include "network/network.hpp"
#include "serializeObject.h"
#include "input/schedule_events.hpp"
#include "simulation/netplay.hpp"
#include "algorithm/frame.hpp"
#include "engine.hpp"



namespace sim
{
	inline GE_ShapePoints convert_rectangle_to_shape_points(mm::vec2* rectangle)
	{
		GE_ShapePoints pointsVector;
		pointsVector.reserve(5);
		pointsVector.push_back(rectangle[0]);
		pointsVector.push_back(rectangle[1]);
		pointsVector.push_back(rectangle[3]);
		pointsVector.push_back(rectangle[2]);
		pointsVector.push_back(rectangle[0]);
		return pointsVector;
	}
#if DEBUG_RENDERS_ENABLED()
	void localdebug_worldsectors()
	{

#define limit 5
		for (physics_area_single_coord_t x=-limit;x<limit;x++)
		{
			for (physics_area_single_coord_t y = -limit; y<limit;y++)
			{
				mm::vec2 vec = {static_cast<double>(x*PHYSICS_AREA_SIZE),static_cast<double>(y*PHYSICS_AREA_SIZE)};
				debug::text_at<debug::physics_position_modifier>(debug::transient_object_pusher{},vec.to_string(),vec);
				debug::draw_rect<debug::physics_position_modifier>(debug::transient_object_pusher{},mm::rect{vec.x,vec.y,PHYSICS_AREA_SIZE,PHYSICS_AREA_SIZE});
			}
		}

	}

	void DEBUG_show_projected_rectangle(mm::rect_points points, ui::color color = debug::default_color)
	{
		auto shape_points = mm::convert_rect_points_to_simple_polygon_points(points);
		debug::draw_shape<debug::physics_position_modifier>(debug::transient_object_pusher{},shape_points,color);
	}

	void DEBUG_show_all_bounding_boxes(object* current_object)
	{
		current_object->collision_info.collisionRectangles.inorder_traverse([current_object](unsigned int myRectangleID ,mm::rectr myRectangle) -> bool
		{
			auto rect_points = mm::transform_rectr_to_rect_points_in_world_coords(myRectangle,current_object->collision_info.bounding_box/2.f,current_object->position()-(current_object->collision_info.bounding_box/2.f));
			DEBUG_show_projected_rectangle(rect_points,ui::color{0x00,0xff,0x00,0xff});
			return false;
		});
	}
#endif 

	void physen::create_physics_object(size_t id, mm::vec2r position, mm::vec2r velocity, double mass)
	{
		assert(type_monarch.is_in_mixture(minimal_physics_object, id));

		position_component_lease.component_map.push_back_at(id,{position});
		physics_object_lease.component_map.push_back_at(id, object(*this, id, position, velocity, mass));
	}

	object::object(physen& host, size_t id, mm::vec2r position, mm::vec2r velocity, double mass) : 
		host_ptr(&host),
		id(id)
	{
		this->velocity = velocity;
		this->collision_info.bounding_box = {0,0};
		this->mass = mass;

		lastGoodPosition = position;

		centerOfMass = {0,0}; //set to geometric center
	}
	double physen::get_delta_time_for_a_tick()
	{
		return static_cast<double>(physics_ideal_tick_length.count())/(1000000000.0);
	}
	void physen::physics_thread_main(physen& me)
	{
#if DEBUG_RENDERS_ENABLED()
		me.physics_debug_instance = &debug::this_thread_instance;
#endif
		netplay::init();
		me.physics_engine_initialized_promise.set_value();
		while(game->running)
		{
			auto _ = algorithm::framerate_guard(me.physics_ideal_tick_length);
			auto _l = std::lock_guard(me.physics_engine_mutex);

			me.physics_engine_input_if.dispatch_events();

			for (auto& this_system : me.systems)
			{
				this_system->tick();
			}

#if DEBUG_RENDERS_ENABLED()
			debug::this_thread_instance.done_drawing();
#endif
		}
		netplay::shutdown();
		printf("Physics Engine thread is shutting down.\n");
	}


	class physics_system : public system
	{
		physen& physics;
		public:
			physics_system(physen & physics) : physics(physics) {}
			~physics_system() override final {}
			void tick() override final
			{
				physics.tick();
			}
	};

	physen::physen() :
		//TODO: make death events actually work
		death_event(type_monarch, "death"),
		position_component_lease(type_monarch, "position_component"),
		physics_object_lease(type_monarch, "physics_object"),
		minimal_physics_object(type_monarch, std::vector<container::lease_info const *>
		{
			&position_component_lease,
			&physics_object_lease
		})
	{
		death_event.add_listener([&](size_t id)
		{
			object* obj = &physics_object_lease.component_map.at(id);

			//remove the object from the areas it's in
			//TODO refactor
			physics_object_area_list_t::iterator it;
			while (true)
			{
				it = obj->areas.begin();
				if (it == obj->areas.end())
				{
					break;
				}
				(*it)->objects_contained.remove(id);
				obj->areas.erase(it);
			}

			//TODO: check if the object is actually a physics object
			position_component_lease.component_map.erase(position_component_lease.component_map.find(id));
			physics_object_lease.component_map.erase(physics_object_lease.component_map.find(id));
		});

		systems.push_back(std::make_unique<physics_system>(*this));


		auto fut = physics_engine_initialized_promise.get_future();
		physics_engine_thread = std::jthread(physics_thread_main, std::ref(*this));
		fut.wait();
	}
	physen::~physen()
	{
		physics_engine_thread.join();
		reset();
	}
	void physen::reset()
	{
		for (object& o : physics_object_lease.component_map)
		{
			remove_object(&o);
		}
		garbage_collect_objects();
		while (!world.empty())
		{
			auto worldpair = world.begin();
			delete worldpair->second; //delete all areas
			world.erase(worldpair);
		}
		ticknum = 0;
	}
	void physen::reset_completely()
	{
		//netplay::stop_connections(); //Also puts state into offline, which will allow us to free server-owned physics objects
		reset();
	}

	void physen::add_tick_finished_callback(std::function<void ()> callback)
	{
		tick_finished_callbacks.push_back(callback);
	}
	void physen::add_tick_start_callback(std::function<void ()> callback)
	{
		tick_start_callbacks.push_back(callback);
	}
	void physen::transient_callback(std::function<void ()> callback)
	{
		auto _ = std::lock_guard(transient_callbacks_mutex);
		transient_callbacks.push_back(callback);
	}
	//Note: Does nothing to tracked objects while in client mode.
	void physen::remove_object(object* physicsObject)
	{
		if (!(netplay::net_state == netplay::state::client && physicsObject->is_tracked )) // object managed by remote servers; no touchie.
		{
			/* //TODO linear search if debug
			if (dead_objects.find(physicsObject) != std::end(dead_objects))
			{
				throw std::runtime_error("trying to kill already dead physics object!");
			}
			*/
			
			//dead_objects.insert(physicsObject);
		}
		else
		{
			printf("Did not delete physics object (because this simulation does not own it)\n");
		}
	}
	bool physen::is_point_in_object(mm::vec2 point, object* obj)
	{
		mm::vec2 rectPoints[4];
		bool returnval = false;
		//for (mm::rectr rectangle : obj->collision_info.collisionRectangles)
		obj->collision_info.collisionRectangles.inorder_traverse([&](unsigned int i, mm::rectr rectangle)
		{
			
			GE_RectangleToPoints(rectangle,obj->collision_info.bounding_box/2.f,rectPoints,obj->position()-(obj->collision_info.bounding_box/2.f));

			//Algorithm credits: Raymond Manzoni (https://math.stackexchange.com/users/21783/raymond-manzoni), How to check if a point is inside a rectangle?, URL (version: 2012-09-03): https://math.stackexchange.com/q/190373
			mm::vec2 AM = point-rectPoints[0]; //M-A
			mm::vec2 AB = rectPoints[1]-rectPoints[0];
			mm::vec2 AD = rectPoints[2]-rectPoints[0]; //bottom-left minus top left

			double AMAB = mm::dot(AM,AB);
			double AMAD = mm::dot(AM,AD);
			double ABAB = mm::dot(AB,AB);
			double ADAD = mm::dot(AD,AD);

			if( ( ( 0.0 <= AMAB ) && ( AMAB <= ABAB ) && ( 0.0 <= AMAD ) && ( AMAD <= ADAD ) ) )
			{
				returnval = true;
				return true;
			}
			return false;
		});
		return returnval;
	}
	std::set<size_t> physen::list_objects_in_radius(mm::vec2 position, double radius)
	{
		std::set<size_t> objects;
		//search the map, searching through each sector that could possibly contain an object with a distance less than "radius" to "position"
		for (physics_area_single_coord_t x = std::floor<physics_area_single_coord_t >((position.x-radius)/PHYSICS_AREA_SIZE); x<= std::ceil<physics_area_single_coord_t >((position.x+radius)/PHYSICS_AREA_SIZE);x++)
		{
			for (physics_area_single_coord_t y = std::floor<physics_area_single_coord_t >((position.y-radius)/PHYSICS_AREA_SIZE); y<= std::ceil<physics_area_single_coord_t >((position.y+radius)/PHYSICS_AREA_SIZE);y++)
			{
				physics_area_coord_t coord = physics_area_coord_t(x,y);
				auto area_it = world.find(coord);
				if (area_it != world.end())
				{
					physics_area_t* area = area_it->second;
					for (size_t id : area->objects_contained)
					{
						if (mm::distance(position,position_component_lease.component_map.at(id).position) <= radius)
						{
							objects.insert(id);
						}
					}
				}
			}
		}
		return objects;
	}
	//TODO gc redo
#define check_not_dead(object) true//(dead_objects.find(object) == dead_objects.end())

	namespace
	{
		void do_callback(auto list)
		{
			for (auto& callback : list)
			{
				callback();
			}
		}
	}
	
	void physen::tick()
	{
		netplay::_physics_pre_callback();
		do_callback(tick_start_callbacks);

		std::vector<std::function< void ()>> tcstore;
		{
			auto _ = std::lock_guard(transient_callbacks_mutex);
			std::swap(tcstore, transient_callbacks);
			transient_callbacks.clear();
		}
		do_callback(tcstore);

		//do main tick logic:
		{
			ticknum++;
			
			for (object& object_ref : physics_object_lease.component_map)
			{
				object* object = &object_ref; //TODO: refactor this loop
				if (check_not_dead(object))
				{
					tick_for_object(object_ref.id);
				}
#if DEBUG_RENDERS_ENABLED()
				DEBUG_show_all_bounding_boxes(object);
#endif
			}
#if DEBUG_RENDERS_ENABLED()	
			localdebug_worldsectors();
#endif
		}
		garbage_collect_objects();

		netplay::_physics_post_callback();
		do_callback(tick_finished_callbacks);
		{
			auto _ = std::lock_guard(transient_callbacks_mutex);
			std::swap(tcstore, transient_callbacks);
			transient_callbacks.clear();
		}
		do_callback(tcstore);
	}
	void physen::garbage_collect_objects()
	{
		/*
		std::unordered_set<object*>::iterator it;
		while(!dead_objects.empty()) //erase stuff like a queue, cause we're constantly removing things and shifting the iterators arround, this works best
		{
			it = dead_objects.begin();
			if (it == dead_objects.end())
			{
				break;
			}

			delete *it;
			dead_objects.erase(it);
		}
		*/
		for (size_t id : dead_objects)
		{
			
		}
	}

	/*
	mm::vec2 get_object_velocity_at(size_t id, mm::vec2 relativeToObjectMomentumPoint)
	{
		object& subject = physics_object_lease.component_map.at(id);
		mm::vec2 velocity = {subject.velocity.x,subject.velocity.y};
		//double radius = mm::distance(subject->centerOfMass,relativeToObjectMomentumPoint);

		//mm::vec2 relativeToCOMCollisionPoint = collisionPoint-centerOfMass;



		return velocity;
	}
	mm::vec2 get_object_velocity_at_with_world_coordinates(size_t id, mm::vec2 momentum_point_in_world)
	{
		return get_object_velocity_at(id, momentum_point_in_world-subject->position());
	}
	*/


	double MINERT = 1000000.0; //TODO 

	double get_impulse_scalar_to_normal(mm::vec2 normal, double coeffecient_resitution, double mass_A, double mass_B, mm::vec2 velocity, mm::vec2 collision_point_relative_to_center_of_mass_A, mm::vec2 collision_point_relative_to_center_of_mass_B, double moment_inertia_A, double moment_inertia_B)
	{
		double top = (-(1+coeffecient_resitution))*mm::dot(velocity,normal);
		double bottom = mm::dot(normal,normal)*((1.0/mass_A + 1.0/mass_B));
		//printf("bottom %f\n",bottom);
		//printf("cpr %s inert %f\n",collision_point_relative_to_center_of_mass_A.to_string().c_str(),moment_inertia_A);
		bottom += pow(mm::dot(collision_point_relative_to_center_of_mass_A, normal),2)/moment_inertia_A;
		//printf("bottom1 %f\n",bottom);
		bottom += pow(mm::dot(collision_point_relative_to_center_of_mass_B, normal),2)/moment_inertia_B;
		//printf("bottom2 %f\n",bottom);
		return top/bottom;
	}
	void apply_impulse_scalar_to_normal(mm::vec2 normal, double impulse, mm::vec2 collision_point, mm::vec2 perpendicular_radius, object* subject)
	{
		subject->velocity += static_cast<mm::vec2r>(normal * (impulse/subject->mass));
		subject->velocity.r -= mm::dot(perpendicular_radius, normal*impulse)/MINERT;
	}




	
	physen::collision_check_result physen::full_collision_check(size_t id, object* victim_object)
	{
		//TODO: This entire function is probably the worst thing in the entire engine, in desperate need of refactor
		mm::vec2r& position = position_component_lease.component_map.at(id).position;
		object& current_object = physics_object_lease.component_map.at(id);

		mm::vec2 theirPoints[4] = {};
		mm::vec2 myPoints[4] = {};

		bool doReturn = false;
		physen::collision_check_result returnval = {false,false};


		current_object.collision_info.collisionRectangles.inorder_traverse([&myPoints,&theirPoints,&doReturn,&returnval,&position,&current_object,victim_object,id,this](unsigned int myRectangleID ,mm::rectr myRectangle) -> bool
		{
			//iterate through each of our rectangles...

			GE_RectangleToPoints(myRectangle,current_object.collision_info.bounding_box/2.0,myPoints,position-(current_object.collision_info.bounding_box/2.f));

			size_t victim_id = victim_object->id;


			victim_object->collision_info.collisionRectangles.inorder_traverse([&myPoints,&theirPoints,&doReturn,&returnval,&position,&current_object,victim_object,myRectangleID,id,victim_id,this](unsigned int victimRectangleID,mm::rectr victimRectangle)
			{

				GE_RectangleToPoints(victimRectangle,victim_object->collision_info.bounding_box/2.0,theirPoints,victim_object->position()-(victim_object->collision_info.bounding_box/2.f));

				GE_ShapePoints myPointsVector = convert_rectangle_to_shape_points(myPoints);
				GE_ShapePoints theirPointsVector = convert_rectangle_to_shape_points(theirPoints);
				auto collision_points = collision::shape_collision(myPointsVector,theirPointsVector);
				if (!collision_points.empty())
				{
#if DEBUG_RENDERS_ENABLED()
					double collision_info_stay_time = 5.0;
					debug::draw_shape<debug::physics_position_modifier>(debug::sticky_object_pusher{collision_info_stay_time},myPointsVector);
					debug::draw_shape<debug::physics_position_modifier>(debug::sticky_object_pusher{collision_info_stay_time},theirPointsVector);
					for (auto& pt : collision_points)
					{
						debug::draw_rect<debug::physics_position_modifier>(debug::sticky_object_pusher{collision_info_stay_time},mm::rect{pt.point.x,pt.point.y,4,4},{255,0,0,255});	
					}
#endif
					position = current_object.lastGoodPosition;

					victim_object->position() = victim_object->lastGoodPosition;

#if DEBUG_RENDERS_ENABLED()
					auto clr = ui::color{255,0,0,255};
					debug::message(clr, collision_info_stay_time, "collision");
					debug::draw_rect<debug::physics_position_modifier>(debug::sticky_object_pusher{collision_info_stay_time},mm::rect{position.x,position.y,5,5},{0,0,255,255});
#endif

					auto get_object_velocity_at_with_world_coordinates = [&](size_t _id, auto)
					{
						object& subject = physics_object_lease.component_map.at(_id);
						return mm::vec2{subject.velocity.x,subject.velocity.y};
					};


					mm::vec2 collision_normal = mm::unit_vector(collision::collision_normal(collision_points)*-1.0);
					mm::vec2 selected_collision_point = collision_points[0].point;
					mm::vec2 collision_velocity = get_object_velocity_at_with_world_coordinates(id,selected_collision_point)+get_object_velocity_at_with_world_coordinates(victim_id,selected_collision_point);
					mm::vec2 current_object_perpendicular_radius = mm::normal_vector(selected_collision_point-position);
					mm::vec2 victim_object_perpendicular_radius = mm::normal_vector(selected_collision_point-victim_object->position());

					double impulse_scalar_to_normal = get_impulse_scalar_to_normal(collision_normal, 0.9, current_object.mass, victim_object->mass, collision_velocity, current_object_perpendicular_radius, victim_object_perpendicular_radius, MINERT,MINERT);

#if DEBUG_RENDERS_ENABLED()
					debug::message(clr,collision_info_stay_time, "collision velocity "+collision_velocity.to_string());
					debug::draw_line<debug::physics_position_modifier>(debug::sticky_object_pusher{1000},collision_points[0].point,collision_points[0].point+mm::vec2{100,100}*mm::unit_vector(collision_normal),{25,255,25,255});
					debug::message(clr, collision_info_stay_time, "impulse scalar to normal %f",impulse_scalar_to_normal);
					auto rs = collision_normal*impulse_scalar_to_normal;
					debug::message(clr,collision_info_stay_time,"    vectorized %f %f",rs.x,rs.y);
#endif

					apply_impulse_scalar_to_normal(collision_normal, impulse_scalar_to_normal, selected_collision_point,current_object_perpendicular_radius,&current_object);
					apply_impulse_scalar_to_normal(collision_normal, -impulse_scalar_to_normal, selected_collision_point,victim_object_perpendicular_radius,victim_object);

					bool killMe = false;
					if (killMe)
					{
						remove_object(&current_object);
						returnval = {true,true};
						doReturn = true;
						return true;
					}
					returnval = {false,true};
					doReturn = true;
					return true;
				}
				return false;
			});
			return doReturn;
		});
		return returnval;
	}
	physen::collision_check_result physen::tick_for_object_stage_2(size_t id, mm::vec2r* velocity)
	{
		mm::vec2r& position = position_component_lease.component_map.at(id).position;
		object& current_object = physics_object_lease.component_map.at(id);

		//move ourselves forward
		position = position+(*velocity);


		//remove us from the old areas first
		

		//TODO: I'm imaging a syste where this happens in tick_for_object and predicts where it will be if it doesn't have a collision, and puts the object in those areas. 
		
		physics_object_area_list_t::iterator it;
		while (true)
		{
			it = current_object.areas.begin();
			if (it == current_object.areas.end())
			{
				break;
			}
			(*it)->objects_contained.remove(id);
			current_object.areas.erase(it);
		}
		//printf("po position %f %f collision_info.bounding_box %f %f\n",position.x,position.y,current_object.boundingBox.x,current_object.boundingBox.y);
		physics_area_single_coord_t xMin = (position.x/PHYSICS_AREA_SIZE);
		physics_area_single_coord_t yMin = (position.y/PHYSICS_AREA_SIZE); 

		physics_area_single_coord_t xMax = std::ceil<int>((position.x+current_object.collision_info.diameter)/PHYSICS_AREA_SIZE);
		physics_area_single_coord_t yMax = std::ceil<int>((position.y+current_object.collision_info.diameter)/PHYSICS_AREA_SIZE);


		for(physics_area_single_coord_t x=xMin;x<=xMax;x++)
		{
			for (physics_area_single_coord_t y=yMin;y<=yMax;y++)
			{
#if DEBUG_RENDERS_ENABLED()
				debug::text_at<debug::physics_position_modifier>(debug::transient_object_pusher{},std::to_string(x)+", y "+std::to_string(y)+"( x "+std::to_string(position.x)+" y "+std::to_string(position.y)+")",position,debug::default_color,debug::positioned_by_t::center);
#endif
				physics_area_coord_t coord = physics_area_coord_t(x,y);
				auto area_it = world.find(coord);
				physics_area_t* area;
				if (area_it == world.end())
				{
					world[coord]  = new physics_area_t();
					area = world[coord];
					//printf("New map spot!\n");
				}
				else
				{
					area = area_it->second;
				}
				

				area->objects_contained.push_front(id);

				current_object.areas.insert(current_object.areas.begin(),area); 
			}
		}


		

		if (collisions_enabled)
		{
			for (physics_area_t* area : current_object.areas)
			{
				for (auto it = area->objects_contained.begin(); it != area->objects_contained.end();it++)
				{
					size_t victim_id = *it;
					object& victim_object = physics_object_lease.component_map.at(victim_id);
					mm::vec2r& victim_position = position_component_lease.component_map.at(victim_id).position;


					if((victim_id != id) && check_not_dead(&victim_object))
					{
						double maxSize = current_object.collision_info.radius;//fmax(current_object->collision_info.bounding_box.x, current_object->boundingBox.x); //fmax tested to be about 2x faster than std::max in this situation
						double theirMaxSize = victim_object.collision_info.radius;
						//take a sphere arround both objects which represents the greatest possible size they could take up. do they collide?
						if ( ( position.x+maxSize >= victim_position.x-theirMaxSize) && (position.x-maxSize <= victim_position.x+theirMaxSize) && (position.y+maxSize >= victim_position.y-theirMaxSize) && (position.y-maxSize <=victim_position.y+theirMaxSize)) //tested to help a lot as compared to just running a full check.
						{
							physen::collision_check_result result = full_collision_check(id, &victim_object);
							if (result.deleted_me || result.collided)
							{
								return result;
							}
						}
					}
				}
			}
		}
		current_object.lastGoodPosition = position;
		return {false,false};
	}
	inline mm::vec2r physen::calculate_minitick_velocity(mm::vec2r real_velocity, double mini_tickrate)
	{
		return real_velocity * (get_delta_time_for_a_tick()/static_cast<double>(mini_tickrate));
	}
	bool physen::tick_for_object(size_t id)
	{
		mm::vec2r& position = position_component_lease.component_map.at(id).position;
		object& current_object = physics_object_lease.component_map.at(id);

		//To avoid Clipping/"Bullet through paper" effect, we will slow down the physics simulation for a specific object by an unsigned nonzero integer and then slow down velocity accordingly. The user SHOULD NOT be able to tell any of this is happening. For performance, velocity is passed as a pointer to the internal function.
		
		unsigned short mini_tickrate = fmax(ceil(  ((abs(current_object.velocity.x)+abs(current_object.velocity.y))/PHYSICS_MAX_SPEED_BEFORE_BROKEN_INTO_MINI_TICKS ) ),1.0); //minimum of 1. //TODO adjust this to good value
		mini_tickrate = std::min(mini_tickrate,static_cast<unsigned short>(255));

		mm::vec2r velocity = calculate_minitick_velocity(current_object.velocity, static_cast<double>(mini_tickrate));

		for (unsigned short i = 0; i <= mini_tickrate; i++)
		{//TODO: Because of this mini-tick system, objects that collide with a i/mini_tickrate value of <1 will have non-up-to-date velocities being added. 
			physen::collision_check_result result = tick_for_object_stage_2(id, &velocity); 
			if (result.deleted_me)
			{
				return true;
			}
			else if (result.collided)
			{
				//break;
				velocity = calculate_minitick_velocity(current_object.velocity, static_cast<double>(mini_tickrate));
			}
		}
		position.r = mm::canonicalize_rotation(position.r); //prevent floating point imprecision from building up and causing issues in various places

		return false;

	}
}
