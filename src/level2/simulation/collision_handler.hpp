#pragma once
#include "mathematics/primitives.hpp"

#include <vector>
#include <unordered_map>

namespace sim
{
	using rect_id_t = unsigned short;

	class rectangle_collision_mesh;

	class collision_rect_editor
	{
		rectangle_collision_mesh* host;
		
		protected:
			collision_rect_editor(rectangle_collision_mesh* in_host) : 
				host(in_host)
			{
			}
			friend class rectangle_collision_mesh;

		public:
			std::unordered_map<rect_id_t, mm::rectr> const & get_rects();
			~collision_rect_editor();
			
			rect_id_t add_rect(mm::rectr);

			void remove_rect(rect_id_t id);
	};

	class rectangle_collision_mesh
	{
		mm::simple_polygon_points* host;

		std::unordered_map<rect_id_t, mm::rectr> rectangles;
		rect_id_t next_id = 0;
		protected:
			friend class collision_rect_editor;

			void update();
		
		public:
			inline std::unordered_map<rect_id_t, mm::rectr> const & get_rects()
			{
				return rectangles;
			}

			inline rectangle_collision_mesh(mm::simple_polygon_points* host) :
				host(host)
			{
			}
			inline rectangle_collision_mesh(rectangle_collision_mesh && other) : 
				host(other.host)
			{
			}
			inline rectangle_collision_mesh & operator=(rectangle_collision_mesh && other)
			{
				host = other.host;
				return *this;
			}

			inline collision_rect_editor get_editor()
			{
				return collision_rect_editor{this};
			}
	};

	template<typename mesh>
	class collision_handler
	{

	};

}
