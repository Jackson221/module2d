/*!
 *@file
 *@author Jackson McNeill
 *
 * An overkill 2D physics engine. Before checking "full" collision, there is a preliminatory check which involves sboundingBox.
 *
 * This preliminatory check works by setting elements in the 2D sboundingBox array to their physicsID. When overlap in this is detected, a full collision check happens. TODO: better explanation 
 *
 * Copyright 2017-2019 Jackson Reed McNeill. Licensed under the GNU AGPL version 3 or any later version.
 */
#pragma once


#include <mutex>
#include <thread>
#include <forward_list>
#include <list>
#include <set>
#include <unordered_map>
#include <functional>
#include <vector>
#include <atomic>
#include <chrono>

#include "mathematics/primitives.hpp"
#include "mathematics/common_ops.hpp"
#include "serializeObject.h"
#include "container/bst.h"
#include "mathematics/collisionRectangles.h"
#include "render/debug.hpp"
#include "event/event.hpp"
#include "input/interfaces.hpp"
#include <unordered_set>
#include <future>
#include "container/idmap.hpp"

//CONFIGS

//Optimization
#define PHYSICS_AREA_SIZE 500
#define PHYSICS_MAX_SPEED_BEFORE_BROKEN_INTO_MINI_TICKS 1200 //e.g if set to 20, an object moving at 100 units/tick would be broken up into 5 ticks of 20 units/tick

//RUNTIME CONFIG

namespace sim
{
	class object;
	using physics_object_id_t = unsigned long;

	using physics_area_single_coord_t = long long int;
	using physics_area_coord_t = std::pair<physics_area_single_coord_t,physics_area_single_coord_t>;
	//using physics_area_t = std::forward_list<object*>;
	struct physics_area_t
	{
		std::forward_list<size_t> objects_contained;
	};
	using physics_object_area_list_t = std::vector<physics_area_t*>;

	class physen;

	
	class object;

	struct position_component
	{
		mm::vec2r position;

		SYNCHRO_STRUCT(position_component,position)
	};

	class system
	{
		public:
			virtual void tick() = 0;
			
			virtual inline ~system() {} 
	};

	class physics_system;
	namespace {using namespace std::chrono_literals;}
	class physen
	{
		public:
			container::type_monarch type_monarch;

			container::event_type<void> death_event;

			container::type_lease<position_component> position_component_lease;
			container::type_lease<object> physics_object_lease;

			container::idmap<std::unique_ptr<system>> systems;

			void create_physics_object(size_t id, mm::vec2r position, mm::vec2r velocity, double mass);

			container::type_mixture minimal_physics_object;

			/*!
			 * The TARGET tick duration of each tick. If the engine
			 * fails to meet this target, ticks per second will drop
			 * and things will appear to slow down.
			 *
			 * This value should NOT be adjusted on the fly. It should match the 
			 * user's vsync framerate in order to deliver a smooth experince
			 */
			std::chrono::nanoseconds physics_ideal_tick_length = 6944444ns;//16666667ns;
			/*!
			 * Returns the IDEAL length, in seconds, for a tick.
			 *
			 * Note that a tick may actually take longer depending on the ammount of work to do and the processor speed.
			 * In most cases, the correct behavior is to have the simulation visibily slow down.
			 */
			double get_delta_time_for_a_tick();


#if DEBUG_RENDERS_ENABLED()
			debug::instance* physics_debug_instance;
#endif
			bool collisions_enabled = true;
			bool ticking_objects_enabled = true;

		
			std::mutex physics_engine_mutex; //TODO: should be private
		private:
			std::jthread physics_engine_thread;

			std::promise<void> physics_engine_initialized_promise;

			input::interfaces physics_engine_input_if = {};

			std::vector<std::function< void ()>> tick_finished_callbacks;
			std::vector<std::function< void ()>> tick_start_callbacks;

			std::mutex transient_callbacks_mutex;
			std::vector<std::function< void ()>> transient_callbacks;

		protected:
			friend class object;

			std::vector<size_t> dead_objects;

			std::map<physics_area_coord_t,physics_area_t*> world;  //TODO: Re-implemt the scollision_info.bounding_box idea but with each area having its own sboundingBox?

			friend class physics_system;
			/*! 
			 * The function called every tick.
			 */
			void tick();
		public:

			physen();
			~physen();
			unsigned long long ticknum = 0;

			/*!
			 * Must be called in the physics thread or with it locked. 
			 *
			 * Causes the current game to become a game host at the specified port.
			void GE_StartServer(unsigned short port);

			 * Must be called in the physics thread or with it locked. 
			 *
			 * Causes a complete physics engine reset, then it will connect to the remote
			 * host.
			void GE_JoinServer(unsigned short port, std::string hostname);
			 */

			/*!
			 * Frees all physics objects in memory.
			 */
			void reset();

			/*!
			 * Disconnects from any games and/or quits hosting a server
			void GE_StopConnections();
			 */

			/*!
			 * Resets all physics objects and disconnects from any connection
			 */
			void reset_completely();


			/*!
			 * Adds a callback which will be ran after each physics tick. Used by the engine itself for gluing things to the physics engine.
			 *
			 * @param callback A function that gives void output and takes no input
			 */
			void add_tick_finished_callback(std::function<void ()> callback);

			/*!
			 * Adds a callback which will be ran before each physics tick. Used by the engine itself for gluing things to the physics engine.
			 *
			 * @param callback A function that gives void output and takes no input
			 */
			void add_tick_start_callback(std::function<void ()> callback);

			/*!
			 * Adds a single-time callback that will be destroyed after calling it. Will be called either before or after a tick.
			 *
			 * DO NOT CALL with the physics engine locked. That is illegal.
			 */
			void transient_callback(std::function<void ()> callback);

			/*! 
			 * Frees the memory used by a physics object. 
			 * @param physicsObject The physics object which will be DELETED and you can no longer use afterwards
			 */
			void remove_object(object* physicsObject); //MUST be allocated with new

			/*!
			 * Finds wheather a point is inside a physics object's collision boxes.
			 */
			bool is_point_in_object(mm::vec2 point, object* obj);

			/*!
			 * Returns physics objects in a given radius.
			 *
			 * Due to the world being broken into sectors behind-the-scenes, this function is generally very fast.
			 */
			std::set<size_t> list_objects_in_radius(mm::vec2 position, double radius);

			/*!
			 * Takes a container of physics objects and sorts them into a vector, where the first element is the closest and last is the furthest away.
			 * NOTE: This isn't perfectly accurate, as it does NOT take into account collision rectangles. If you're, for example, raycasting, you want to
			 * keep checking elements beyond when you find a collision until you get to one that is definitely farther away(i.e. accounting for its rectangles and position)
			 */
			template <class container>
			std::vector<object*> sort_by_closest_centroid(mm::vec2 position, container to_sort_list);
	
		private:

		static void physics_thread_main(physen& me);

		

		/*!
		 * Delete the objects that got destroyed during simulation
		 */
		void garbage_collect_objects();

		struct collision_check_result
		{
			bool deleted_me;
			bool collided;
		};
		collision_check_result full_collision_check(size_t id, object* victimObj);
		collision_check_result tick_for_object_stage_2(size_t id, mm::vec2r* velocity);
		mm::vec2r calculate_minitick_velocity(mm::vec2r real_velocity, double mini_tickrate);
		bool tick_for_object(size_t id);
	};
	struct object 
	{
		inline constexpr mm::vec2r& position()
		{
			return host_ptr->position_component_lease.component_map[id].position;
		}
		inline constexpr mm::vec2r const & position() const
		{
			return host_ptr->position_component_lease.component_map[id].position;
		}
		size_t id;
		mm::vec2r velocity;
		double mass;
		/*!
		 * How offset the center-of-mass is from the geometric center.
		 * Default value is 0,0
		 */
		mm::vec2 centerOfMass; 
		
		collision_rectangle_manager collision_info;	

		mm::vec2r lastGoodPosition;
		/*!
		 * Which area(s) the physics object is in the world (basically, a cached value you don't have to worry about)
		 */
		physics_object_area_list_t areas;

		bool is_tracked = false;

		constexpr void add_relative_velocity(mm::vec2r moreVelocity)
		{
			moreVelocity = mm::rotation_ccw(moreVelocity,position().r);
			velocity.x += moreVelocity.x;
			velocity.y += moreVelocity.y;
			velocity.r += moreVelocity.r;
		}
		protected:
			friend void physen::create_physics_object(size_t id, mm::vec2r position, mm::vec2r velocity, double mass);
			object(physen& host, size_t id, mm::vec2r position, mm::vec2r velocity, double mass);
			physen* host_ptr;
	};

	template <class container>
	std::vector<object*> physen::sort_by_closest_centroid(mm::vec2 position, container to_sort_list)
	{
		//allocate & copy into sorted vector
		container new_vector; 
		for (object* obj : to_sort_list) //go through every object in the old list
		{
			auto distance = mm::distance(position,obj->position()).get();
			distance -= obj->collision_info.radius;
			for (auto it = new_vector.begin(); true; it++)
			{
				if ( it == new_vector.end() || (distance <= (mm::distance(position,(*it)->position()).get()-(*it)->collision_info.radius)  ))
				{
					new_vector.insert(it,obj);
					break;
				}
			}
		}
		return new_vector;
	}

}
