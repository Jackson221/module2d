#include "netplay.hpp"


//TODO:
//* Revamp serialization library and remove serialization implementations that have bled into netplay
//* Better differentiate client and server; perhaps seperate classes, namespaces, or something else. Code is to hard to navigate atm.

#include <optional>
#include <thread>
#include <mutex>
#include <deque>
#include <unordered_map>
#include <tuple>

#include "simulation/serializeObject.h"
#include "simulation/physics.hpp"


namespace netplay
{
	state net_state = state::offline;

	std::string nickname = "New Human"; 

	
	interface_id_t next_network_interface_id = 0;
	/*container::idvector<networked_serialization_handler_uniform_id_t> network_interface_callbacks_uniform;
	container::idvector<networked_serialization_handler_nonuniform_id_t> network_interface_callbacks_nonuniform;*/

	using interface_name_to_id_t = std::unordered_map<std::string, interface_id_t>;
	interface_name_to_id_t global_my_network_interface_name_to_id;

	/*
	interface_id_t register_networked_serialization_handler(networked_serialization_handler_uniform_id_t && func_uniform, networked_serialization_handler_nonuniform_id_t && func_nonuniform, std::string const unique_id_string)
	{
		network_interface_callbacks_uniform.push_back(std::move(func_uniform));	
		network_interface_callbacks_nonuniform.push_back(std::move(func_nonuniform));	
		global_my_network_interface_name_to_id.insert(std::make_pair(unique_id_string,next_network_interface_id));
		return next_network_interface_id++;
	}

	void change_networked_serialization_handler(interface_id_t const id,networked_serialization_handler_uniform_id_t && func_uniform, networked_serialization_handler_nonuniform_id_t && func_nonuniform)
	{
		network_interface_callbacks_uniform[id] = std::move(func_uniform);
		network_interface_callbacks_nonuniform[id] = std::move(func_nonuniform);
	}
	void remove_networked_serialization_handler(interface_id_t const id, std::string const unique_id_string)
	{
		network_interface_callbacks_nonuniform.erase(network_interface_callbacks_nonuniform.begin().move_by_id(id));
		network_interface_callbacks_uniform.erase(network_interface_callbacks_uniform.begin().move_by_id(id));
		global_my_network_interface_name_to_id.erase(global_my_network_interface_name_to_id.find(unique_id_string));
	}
	*/

	using id_translate_map_t = std::unordered_map<interface_id_t, interface_id_t>; 
	/*!
	 * Generates translations maps for, in order:
	 *
	 * theirID -> myID
	 *
	 * myID -> theirID
	 */
	std::tuple<id_translate_map_t,id_translate_map_t> generate_translate_map(interface_name_to_id_t const & my_interface_name_to_id, interface_name_to_id_t const & their_interface_name_to_id)
	{
		printf("genmap\n");
		id_translate_map_t&& return_val_0 = id_translate_map_t();
		id_translate_map_t&& return_val_1 = id_translate_map_t();
		for (auto&& [name, id] : their_interface_name_to_id)
		{
			auto my_interface_name_to_id_entry = my_interface_name_to_id.find(name);
			if (my_interface_name_to_id_entry != my_interface_name_to_id.end()) //if we have an ID on our side for that event name
			{
				printf("translate name %s with our id %llu and theirs %llu\n",name.c_str(), my_interface_name_to_id_entry->second, id);
				return_val_0.insert(std::make_pair(id, my_interface_name_to_id_entry->second)); // insert theirID -> myID
				return_val_1.insert(std::make_pair(my_interface_name_to_id_entry->second,id)); // insert myID -> theirID
			}
		}
		return std::make_tuple(std::move(return_val_0),std::move(return_val_1));
	}


	std::vector<network_interface_event> queued_network_interface_events;


	//TODO object-orient client & server to better seperate this stuff?
	
	//~~server stuff
	std::optional<network::socket<network::type::IPv6>> server_sock;
	struct client_info
	{
		client_id_t id;
		network::socket<network::type::server_view_of_client> socket;
	};
	container::idmap<client_info> clients;
	std::optional<std::future<network::socket<network::type::server_view_of_client>>> new_client;
	client_id_t next_client_id = 1;
	//networkable events management
	//std::unordered_map<client_id_t, id_translate_map_t> client_id_translate_maps;
	
	//~~both stuff

	std::unordered_map<client_id_t,std::string> client_names;

	//~~client stuff
	std::optional<network::socket<network::type::client_view_of_server>> remote_server_sock;
	std::mutex read_thread_mutex;
	std::thread client_read_thread;
	std::deque<std::string> client_unserialization_strings;
	//std::deque<serialization::unserialization_state> client_unserialization_states;
	client_id_t my_client_id = 0;
	//networkable events management
	id_translate_map_t server_id_to_my_id_translation_map;
	id_translate_map_t my_id_to_server_id_translation_map;


	//////////

	namespace new_name_event
	{
		manager_t manager("new_name_event");
		thread_local interface_t interface(manager);
		auto netif = netplay::network_interface<event_configuration_types>(manager);

		interface_t * netplay_thread_interface;
	}



	//////////

	/*!
	 * Process network interface events after they have been converted to use our IDs
	 *
	 * Use a single client ID for all of them
	 */
	void process_queued_network_interface_events_uniform(std::vector<network_interface_event> const & events, client_id_t client_id)
	{
		/*
		for (auto& the_event : events)
		{
			auto unser_state = serialization::unserialization_state(the_event.serialized_event.buffer);
			if (network_interface_callbacks_uniform.size() > the_event.event_id)
			{
				network_interface_callbacks_uniform[the_event.event_id](client_id,unser_state);
			}
			else
			{
				throw std::runtime_error("no network_interface_callbacks entry for this originating ID.");
			}
		}
		*/
	}
	void process_queued_network_interface_events_nonuniform(std::vector<network_interface_event> const & events)
	{
		/*
		for (auto& the_event : events)
		{
			auto unser_state = serialization::unserialization_state(the_event.serialized_event.buffer);
			if (network_interface_callbacks_nonuniform.size() > the_event.event_id)
			{
				network_interface_callbacks_nonuniform[the_event.event_id](the_event.event_client_id,unser_state);  
			}
			else
			{
				throw std::runtime_error("no network_interface_callbacks entry for this originating ID.");
			}
		}
		*/
	}
	

	//This code is here because of serialization v1 isn't capable of handling this data in a normal manner
	/*
	template<bool am_server>
	void send_queued_network_interface_events(serialization::serialization_state& state, std::vector<network_interface_event> const & events)
	{
		serialization::serialize(static_cast<serialization::size_tp>(events.size()),state);
		for (auto& the_event : events)
		{
			serialization::serialize(the_event.event_id,state); //the event id
			serialization::serialize(&(the_event.serialized_event),state);	//TODO serialization v1 cruft
			if constexpr(am_server)
			{
				serialization::serialize_container_sequence(&(the_event.event_client_id),state);//TODO serialization v1 cruft
			}
		}
	}
		*/
		/*
	template<bool am_server>
	std::vector<network_interface_event> receive_queued_network_interface_events(serialization::unserialization_state& state)
	{
		auto&& return_val = std::vector<network_interface_event>{};

		auto size = serialization::unserialize<serialization::size_tp>(state);

		return_val.reserve(static_cast<size_t>(size));

		for (size_t i =0; i < static_cast<size_t>(size); i++)
		{
			auto&& id = serialization::unserialize<interface_id_t>(state); //the event id
			serialization::serialization_state* serialized_event = serialization::unserialize<serialization::serialization_state*>(state);
			
			auto&& client_ids = std::vector<client_id_t>();
			if constexpr(!am_server)
			{
				client_ids = serialization::unserialize<std::vector<client_id_t>>(state);
			}

			return_val.push_back(network_interface_event{id,std::move(*serialized_event),std::move(client_ids)});
			delete serialized_event;
		}
		return std::move(return_val);
	}
		*/
	/////
	void translate_queued_network_interface_events_ids(std::vector<network_interface_event> & events, id_translate_map_t const & translation_map)
	{
		for (auto& the_event : events)
		{
			auto new_id_it = translation_map.find(the_event.event_id);
			if (new_id_it != translation_map.end())
			{
				the_event.event_id = new_id_it->second;
			}
		}
	}



	/*!
	 * Write to the socket a serialization_state of constant size (12 bytes) which contains a 64-bit int that is the size of the following
	 * serialization state (data) to be sent, then send the data in data.
	 */
		/*
	template<network::type network_type>
	void send_serialization_data(serialization::serialization_state const & data, network::socket<network_type> & socket)
	{
		serialization::serialization_state size_state(1);
		serialization::serialize(static_cast<serialization::size_tp>(data.bufferUsed),size_state); //serialize the size of the first buffer (will always come out as 12 bytes)

		socket.write(std::string(size_state.buffer,12)); // inform them of the size of the next one. 
		socket.write(std::string(data.buffer,data.bufferUsed)); //send the data over.
	}
		*/

	template <network::type network_type>
	std::string receive_serialization_state(network::socket<network_type>& socket)
	{
		/*
		std::string unser_size_str = socket.read(12);
		auto unser_size = serialization::unserialization_state(unser_size_str.data());
		serialization::size_tp next_size = serialization::unserialize<serialization::size_tp>(unser_size);

		//printf("next size %llu\n",next_size);

		return socket.read(next_size);
		*/
	}

	void send_initial_packet_to_client(client_info & client)
	{
		/*
		serialization::serialization_state state(0); 

		serialization::serialize(client.id, state); //Tell them their ID

		tmp::serialize_container_associative_1(&client_names,state); //Tell them everyone's (including theirs) names
		
		//TODO this should not be a pointer once the serialization library has been updated
		tmp::serialize_container_associative_0(&global_my_network_interface_name_to_id, state); //Give them a list of our event name string -> our ID for it

		GE_SerializedTrackedObjects(state); //Give them the state of the entire simulation

		send_serialization_data(state,client.socket);
		*/
	}
	bool name_is_in_use(std::string test_name)
	{
		for(auto&& [id,used_name] : client_names)
		{
			if (test_name == used_name) [[unlikely]]
			{
				return true;
			}
		}
		return false;
	}
	void initialize_connected_client(network::socket<network::type::server_view_of_client> && new_client_socket)
	{
		/*
		//get their requested name
		auto state_string = receive_serialization_state(new_client_socket);
		auto state = serialization::unserialization_state(state_string.data());

		std::string current_nickname = serialization::unserialize<std::string>(state);
		if (current_nickname.empty())
		{
			current_nickname = "Shyguy";
		}
		while(name_is_in_use(current_nickname))
		{
			current_nickname += '?';	
		}

		new_name_event::interface.push_event(new_name_event::net_data_t{new_name_event::event_data_t{next_client_id, current_nickname}, my_client_id});
		new_name_event::interface.send_events();
		new_name_event::interface.dispatch_events();
		new_name_event::netif.send_events();

		clients.push_back_at(clients.begin().move_by_id(next_client_id-clients.begin().get_id()), {next_client_id,std::move(new_client_socket)});	
		printf("<<< Assigning client id %llu\n",next_client_id);


		++next_client_id;

		//send them the initial state of the world
		send_initial_packet_to_client(clients.back());
		*/
	}

	void server_post_callback()
	{
		/*
		serialization::serialization_state state(0);
		GE_PartialSerializeTrackedObjects(state);
		send_queued_network_interface_events<true>(state,queued_network_interface_events);
		queued_network_interface_events.clear();
		*/

		for (auto it = std::begin(clients); it != std::end(clients); it++)
		{
			auto& client = *it;
			try
			{
				/*
				send_serialization_data(state,client.socket);
				auto state_string = receive_serialization_state(client.socket);
				auto unser_state = serialization::unserialization_state(state_string.data());
				std::vector<network_interface_event> network_events = receive_queued_network_interface_events<true>(unser_state);
				process_queued_network_interface_events_uniform(network_events, it.get_id());
				*/
			}
			//catch (network::broken_pipe const & e)
			catch (std::exception const & e)
			{
				/*printf("Client ID %llu disconnected\n",client.id);
				it = clients.erase(it);
				client_names.erase(client.id);
				if (it == std::end(clients))
				{
					break;
				}*/
			}
			/*
			{
			}*/
		}

		if (new_client.has_value())
		{
			if (new_client->wait_for(std::chrono::milliseconds(0)) == std::future_status::ready)
			{
				initialize_connected_client(new_client->get());
				new_client = {};
			}
		}
		if (!new_client.has_value())
		{
			new_client = {server_sock->listen_for_client_async()};
		}
	}

	
	unsigned long times= 0;
	void client_pre_callback()
	{
		/*
		read_thread_mutex.lock();
		if (client_unserialization_states.size() >= 2)
		{
			//printf("Multiple serialization states to read this time (lagging?)\n");
		}
		for (auto& state : client_unserialization_states)
		{
			GE_UpdateTrackedObjects(state);
			std::vector<network_interface_event> network_events = receive_queued_network_interface_events<false>(state);
			translate_queued_network_interface_events_ids(network_events, server_id_to_my_id_translation_map);
			process_queued_network_interface_events_nonuniform(network_events);
		}
		client_unserialization_states.clear();
		client_unserialization_strings.clear();
		*/
		
		/*!
		 * Receive all new connected client names
		 *
		 * This needs to happen before any other networked events are processed in case names are looked up.
		 */
		new_name_event::netif.dispatch_events();
		new_name_event::interface.dispatch_events();



		//send our networked events////
		translate_queued_network_interface_events_ids(queued_network_interface_events,my_id_to_server_id_translation_map);

		/*
		serialization::serialization_state state(0);
		send_queued_network_interface_events<false>(state,queued_network_interface_events);
		send_serialization_data(state,remote_server_sock.value());
		*/

		queued_network_interface_events.clear();
		/////////
		
		//send_queued_network_interface_events
		read_thread_mutex.unlock();
	}
	void client_read_thread_main()
	{
		try
		{
			//TODO phys
			while(true)//!physics_engine_thread_shutdown)
			{
				/*
				auto state_string = receive_serialization_state(remote_server_sock.value());
				read_thread_mutex.lock();
				client_unserialization_strings.push_back(state_string);
				client_unserialization_states.push_back(serialization::unserialization_state(client_unserialization_strings.back().data()));
				read_thread_mutex.unlock();
				*/
			}
		}
		catch (std::exception const & e)
		{
			printf("Client read thread caught exception \"%s\" and will die now\n",e.what());
		}
	}
	void start_server(unsigned short port)
	{
		//assign ourselves a nickname. First dibs!
		client_names.insert(std::make_pair(my_client_id,nickname));

		net_state = state::server;
		my_client_id = 0;
		server_sock = {network::socket<network::type::IPv6>(port)};
		server_sock->bind_as_server();
	}
	//Block until the server sends us our basic client info (i.e. our ID) and the current state of things
	void initialize_connection_to_server()
	{
		//Request our nickname
		/*{
			serialization::serialization_state state(0);
			serialization::serialize(&nickname,state);

			send_serialization_data(state,remote_server_sock.value());
		}

		auto state_string = receive_serialization_state(remote_server_sock.value());
		auto state = serialization::unserialization_state(state_string.data());

		my_client_id = serialization::unserialize<client_id_t>(state);
		printf(">>>Server assigned me ID %llu\n",my_client_id);
		client_names = tmp::unserialize_container_associative_1<decltype(client_names)>(state);

		printf(">>>Client list:\n");
		for (auto&& [id, name] : client_names)
		{
			printf("%llu | %s\n",id,name.c_str());
		}

		//TODO this should not be a pointer once the serialization library has been updated
		auto ptr_to_their_name_to_id = tmp::unserialize_container_associative_0<interface_name_to_id_t*>(state); //Give them a list of our event name string -> our ID for it
		std::tie(server_id_to_my_id_translation_map,my_id_to_server_id_translation_map) = generate_translate_map(global_my_network_interface_name_to_id,*ptr_to_their_name_to_id);
		delete ptr_to_their_name_to_id;

		GE_ClientReceiveTrackedObjects(state);
		*/
	}
	void join_server(unsigned short port, std::string hostname)
	{
		//GE_ResetPhysicsEngine(); //TODO phys
		net_state = state::client;

		printf("make socket\n");
		remote_server_sock = {network::socket<network::type::client_view_of_server>(port)};
		printf("ok, now connect\n");
		remote_server_sock->connect_to_server(hostname);
		printf("now do the physics engine\n");


		//Have to block until the initial state is receieved.
		initialize_connection_to_server();

		//Spin a new thread up to constantly listen for messages.
		client_read_thread = std::thread(client_read_thread_main);
	}
	void stop_connections()
	{
		clients.clear();
		server_sock = {};
		new_client = {};
		net_state = state::offline;

		client_names.clear();

		remote_server_sock = {};
		client_unserialization_strings.clear();
		//client_unserialization_states.clear();
		server_id_to_my_id_translation_map.clear();
		my_id_to_server_id_translation_map.clear();
	}

	void begin_offline_mode()
	{
		net_state = state::offline;
		clients.clear();

		my_client_id = 0;
		next_client_id =1;
		new_name_event::interface.push_event(new_name_event::net_data_t{new_name_event::event_data_t{my_client_id, nickname}, my_client_id});
		new_name_event::interface.send_events();
		new_name_event::interface.dispatch_events();

		new_name_event::netplay_thread_interface->dispatch_events();
	}

	void _physics_pre_callback()
	{
		if (remote_server_sock.has_value())
		{
			client_pre_callback();
		}
	}
	void _physics_post_callback()
	{
		if (server_sock.has_value())
		{
			server_post_callback();
		}
	}

	std::optional<event::listener<new_name_event::interface_t>> _persistent_new_name_listener;
	
	void init()
	{
		new_name_event::netplay_thread_interface = & new_name_event::interface;
		_persistent_new_name_listener = event::listener(new_name_event::interface, [](auto in)
		{
			printf("New client connected ID: %llu Name: %s\n",in.event_data.id,in.event_data.name.c_str());
			client_names[in.event_data.id] = in.event_data.name;
		},{});
	}
	void shutdown()
	{
		_persistent_new_name_listener.reset();
	}
}
