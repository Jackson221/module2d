#include "collision_handler.hpp"

namespace sim
{
	std::unordered_map<rect_id_t, mm::rectr> const & collision_rect_editor::get_rects()
	{
		return host->rectangles;
	}
	rect_id_t collision_rect_editor::add_rect(mm::rectr rect)
	{
		host->rectangles.insert(std::make_pair(host->next_id, rect));
		return host->next_id++;
	}
	void collision_rect_editor::remove_rect(rect_id_t id)
	{
		host->rectangles.erase(host->rectangles.find(id));
	}
	collision_rect_editor::~collision_rect_editor()
	{
		host->update();
	}
}
