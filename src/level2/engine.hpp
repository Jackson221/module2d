/*! 
 * @file
 * @author Jackson McNeill
 *
 * General engine things that don't really belong anywhere else
 *
 * Copyright 2017-2019 Jackson Reed McNeill. Licensed under the GNU AGPL version 3 or any later version.
 */
#pragma once

#include <string>
#include <atomic>
#include <memory>
#include <functional>
#include "render/state.hpp"
#include "UI/UI.hpp"
#include "cli/flag_interpreter.hpp"
#include "input/interfaces.hpp"
#include "options/options.hpp"

struct engine_state
{
	bool is_headless;
	std::unique_ptr<render::state> rstate;
	ui::state uistate;
	ui::window_manager* wm;
	input::interfaces* input_if;
	flag_interpreter::results_t flags;

	options::options options;


	inline ~engine_state();
};
struct _game_main_wrapper;

class game_base
{
	protected:
		friend class _game_main_wrapper;


		game_base(std::string title) :
			game_title(title)
		{
		}


		//This contains your game's thread (probably the render thread)
		virtual int game_main(int argc, char* argv[], engine_state & estate) = 0;
	public:
		std::atomic<bool> running = true;

		//Any flags you'd like to interpret
		flag_interpreter::shorthands_t shorthands;

		//This string will be the game window's initial title.
		std::string game_title;
};

/*!
 * NOT DEFINED INSIDE ENGINE.CPP
 *
 * Your game should create a singleton derived from game_base, and then set
 * game to point to it.
 *
 * For example:
 *
 * class mygame : public game_base {...} //implements game_main
 * mygame game_inst;
 * game_base* game = &game_inst;
 */
extern game_base* game;
