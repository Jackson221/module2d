/*!
 * Include defines for GLM, etc.
 *
 * Note: There is a known issue where GLM has faulty c++11 detection on some relatively older (though still very new) versions of gcc. I face this issue on one of my computers but cannot replicate it on others
 */
#pragma once



#define GLM_ENABLE_EXPERIMENTAL
#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#define GLM_FORCE_CXX14
//#define GLM_FORCE_INLINE
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/hash.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>


#include "string_manip/sprintf.hpp"
//Define string converters for GLM types - TODO complete this
namespace test
{
	constexpr char _internal_vec3_string[] = "{%d%,%d%,%d%}";
	inline std::string convert_to_string(glm::vec3 data) 
	{
		return "";//string_manip::sprintf<_internal_vec3_string>(data.x,data.y,data.z);	

	}
}	
