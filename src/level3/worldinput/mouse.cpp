#include "mouse.hpp"

#include "input/computer.hpp"

#include <tuple>

namespace worldinput
{
	namespace mouse
	{
		namespace mouse_motion
		{
			manager_t manager("worldinput_mouse_motion");
			thread_local event::endpoint_interface<event_configuration_types> interface(manager);
		}
		namespace mouse_button
		{
			manager_t manager("worldinput_mouse_button");
			thread_local event::endpoint_interface<event_configuration_types> interface(manager);
		}
		thread_local std::tuple<mouse::mouse_motion::interface_t*, mouse::mouse_button::interface_t*> interfaces = 
		{
			&mouse::mouse_motion::interface,
			&mouse::mouse_button::interface,
		};
		mm::vec2 get_cursor_world(render::camera* camera)
		{
			mm::vec2r click_position_screen = {static_cast<double>(input::computer::mouse_motion::current_cursor_x), static_cast<double>(input::computer::mouse_motion::current_cursor_y),0.0};
			mm::vec2r click_position_world = camera->negate_camera_offset(click_position_screen);
			return static_cast<mm::vec2>(click_position_world);
		}
		mouse_event_creator::mouse_event_creator(render::camera* camera, input::interfaces & input_if) :
			mouse_motion_listner(input_if.mouse_motion, [camera](auto in)
			{
				mm::vec2 click_position_world = get_cursor_world(camera);
				mouse::mouse_motion::interface.push_event(mouse::mouse_motion::event_data_t{in.delta_x,in.delta_y,in.delta_wheel_x,in.delta_wheel_y, click_position_world});
				mouse::mouse_motion::interface.send_events();
				mouse::mouse_motion::interface.dispatch_events();
			}, {}),
			mouse_button_listner(input_if.mouse_button, [camera](auto in)
			{
				mm::vec2 click_position_world = get_cursor_world(camera);
				mouse::mouse_button::interface.push_event(mouse::mouse_button::event_data_t{in.is_down,in.button,click_position_world});
				mouse::mouse_button::interface.send_events();
				mouse::mouse_button::interface.dispatch_events();
			}, {})
		{
		}


	}
}
