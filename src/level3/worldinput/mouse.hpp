/*!
 * Gives the location in the world where the user clicked
 */
#pragma once 

#include "event/event.hpp"

#include "input/computer.hpp"
#include "input/interfaces.hpp"

#include "mathematics/primitives.hpp"

#include "render/camera.hpp"

namespace worldinput
{
	namespace mouse
	{
		//TODO give real-world mouse delta's (in a vector2 of course) instead of screen delta's 
		namespace mouse_motion
		{
			struct event_data_t
			{
				int32_t delta_x;
				int32_t delta_y;

				int32_t delta_wheel_x;
				int32_t delta_wheel_y;

				mm::vec2 location;
			};
			using event_configuration_types = event::event_configuration_types_with_default_interfaces<event::event_configuration_types<event_data_t>>;
			using manager_t = event_configuration_types::manager_t;
			extern manager_t manager;
			using interface_t = event::endpoint_interface<event_configuration_types>;
			extern thread_local interface_t interface;
		}
		namespace mouse_button
		{
			struct event_data_t
			{
				bool is_down;
				input::computer::mouse_button::button_t button;

				mm::vec2 location;
			};
			using event_configuration_types = event::event_configuration_types_with_default_interfaces<event::event_configuration_types<event_data_t>>;
			using manager_t = event_configuration_types::manager_t;
			extern manager_t manager;
			using interface_t = event::endpoint_interface<event_configuration_types>;
			extern thread_local interface_t interface;
		}
		extern thread_local std::tuple<mouse::mouse_motion::interface_t*, mouse::mouse_button::interface_t*> interfaces;

		mm::vec2 get_cursor_world(render::camera* camera);
		/*!
		 * One of these should be created (if you want world mouse events) and should live no longer than the camera object passed in by pointer
		 */
		struct mouse_event_creator
		{
			event::listener<input::computer::mouse_motion::interface_t> mouse_motion_listner;
			event::listener<input::computer::mouse_button::interface_t> mouse_button_listner;
			mouse_event_creator(render::camera* camera, input::interfaces & input_if);
		};
	}
}
