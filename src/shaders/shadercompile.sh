#!/bin/bash

mkdir ../../../spirv

glslangValidator -V shader.vert
glslangValidator -V shader.frag

mv vert.spv ../../../spirv/
mv frag.spv ../../../spirv/

glslangValidator -V shader_alt.frag

mv frag.spv ../../../spirv/frag_alt.spv

glslangValidator -V 2dshader.vert
glslangValidator -V 2dshader.frag

mv vert.spv ../../../spirv/2dvert.spv
mv frag.spv ../../../spirv/2dfrag.spv

glslangValidator -V transient.vert
glslangValidator -V transient.frag

mv vert.spv ../../../spirv/transientvert.spv
mv frag.spv ../../../spirv/transientfrag.spv
