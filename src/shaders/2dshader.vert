#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(binding = 0) uniform UniformBufferObject 
{
    mat4 model;
    mat4 view;
    mat4 proj;
    vec3 global_lighting_direction;
} ubo;


layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inColor;
layout(location = 2) in vec2 inTexCoord;
layout(location = 3) in vec3 inNormal;


out gl_PerVertex {
    vec4 gl_Position;
};

layout(location = 0) out vec3 fragColor;
layout(location = 1) out vec2 fragTexCoord;


vec3 lighting_color = vec3(204.f/256.f,204.f/256.f,166.f/256.f);
vec3 lighting_direction = ubo.global_lighting_direction;


float ambient_light = 0.3f;



void main() 
{
    gl_Position = (ubo.proj * ubo.view * ubo.model * vec4(inPosition, 1.0) );

	fragTexCoord=inTexCoord;
}
