#include "idmap.hpp"

#include <map>
#include <random>
#include <iostream>

#include "string_manip/sprintf.hpp"
#include "test/test.hpp"

namespace container
{
	void perf_test(test::instance & inst, size_t num_random_ids, size_t max_id, bool ids_are_random)
	{
		std::vector<size_t> random_ids;

		if (ids_are_random)
		{
			for (int i = 0; i < num_random_ids; i++)
			{
				random_ids.push_back(rand() % max_id);
			}
			std::sort(random_ids.begin(), random_ids.end());

			auto last = std::unique(random_ids.begin(), random_ids.end());

			random_ids.erase(last, random_ids.end());
			inst.inform("using " + std::to_string(random_ids.size()) + " ids. from 0-"+std::to_string(max_id));
		}
		else
		{
			for (int i = 0; i < num_random_ids; i++)
			{
				random_ids.push_back(i);
			}
			inst.inform("using " + std::to_string(random_ids.size()) + " ids in direct sequential order.");
		}


		idmap<int> idmap_version;
		std::map<size_t, int> map_version;
		std::unordered_map<size_t, int> unordered_map_version;
		/*

		inst.inform("idmap random insertion time:              " + std::to_string(test::benchmark([&]()
		{
			for( size_t id : random_ids)
			{
				idmap_version.push_back_at(id, 1);
			}
		})));
		inst.inform("std::map random insertion time:           " + std::to_string(test::benchmark([&]()
		{
			for( size_t id : random_ids)
			{
				map_version.insert(std::make_pair(id,1));
			}
		})));
		inst.inform("std::unordered_map random insertion time: " + std::to_string(test::benchmark([&]()
		{
			for( size_t id : random_ids)
			{
				unordered_map_version.insert(std::make_pair(id,1));
			}
		})));

		size_t sum;

		inst.inform("idmap linear traverse time:               " + std::to_string(test::benchmark([&]()
		{
			for( size_t val : idmap_version)
			{
				sum += val;
			}
		})));
		inst.inform("std::map linear traverse time:            " + std::to_string(test::benchmark([&]()
		{
			for( auto [key, val] : map_version)
			{
				sum += val;
			}
		})));
		inst.inform("std::unordered_map linear traverse time:  " + std::to_string(test::benchmark([&]()
		{
			for( auto [key, val] : unordered_map_version)
			{
				sum += val;
			}
		})));

		std::vector<size_t> subset_of_ids;
		std::sample(random_ids.begin(), random_ids.end(), std::back_inserter(subset_of_ids), 1000, std::mt19937{std::random_device{}()});

		inst.inform("idmap random access time:                 " + std::to_string(test::benchmark([&]()
		{
			for( size_t id : subset_of_ids)
			{
				sum += idmap_version[id];
			}
		})));
		inst.inform("std::map random access time:              " + std::to_string(test::benchmark([&]()
		{
			for( size_t id : subset_of_ids)
			{
				sum += map_version[id];
			}
		})));
		inst.inform("std::unordered_map random access time:    " + std::to_string(test::benchmark([&]()
		{
			for( size_t id : subset_of_ids)
			{
				sum += unordered_map_version[id];
			}
		})));


		inst.inform("idmap random erase time:                  " + std::to_string(test::benchmark([&]()
		{
			for( size_t id : subset_of_ids)
			{
				idmap_version.erase(idmap_version.find(id));
			}
		})));
		inst.inform("std::map random erase time:               " + std::to_string(test::benchmark([&]()
		{
			for( size_t id : subset_of_ids)
			{
				map_version.erase(map_version.find(id));
			}
		})));
		inst.inform("std::unordered_map random erase time:     " + std::to_string(test::benchmark([&]()
		{
			for( size_t id : subset_of_ids)
			{
				unordered_map_version.erase(unordered_map_version.find(id));
			}
		})));

		volatile size_t force_sum_to_exist = sum;
		*/
	}
	void unit_test_idmap(test::instance & inst)
	{
		/*
		inst.set_name("container-idmap");
		{
			idmap<int> mym;

			mym.push_back(27);

			inst.test(TEST_EX(mym.front(), ==, 27));
			inst.test(TEST_EX(mym.begin().get_id(), ==, 0));

			mym.push_back_at(420, 69);

			inst.test(TEST_EX(mym.front(), ==, 27));
			inst.test(TEST_EX(mym.begin().get_id(), ==, 0));
			inst.test(TEST_EX(mym[420], ==, 69));

			mym.push_back_at(421, 1337);
			mym.push_back(13);

			{
				std::vector<int> contents;
				std::vector<size_t> iterators;
				for (auto it = mym.begin(); it != mym.end(); it++)
				{
					contents.push_back(*it);
					iterators.push_back(it.get_id());
				}
				auto expected_contents = std::vector{27, 69, 1337, 13};
				auto expected_iterators = std::vector<size_t>{0, 420, 421, 422};
				inst.test(TEST_EX(contents, ==, expected_contents));
				inst.test(TEST_EX(iterators, ==, expected_iterators));
			}

			mym.erase(mym.find(421));

			inst.test(TEST_EX(mym.at(420), ==, 69));
			inst.test(TEST_EX(mym[422], ==, 13));
			inst.test(TEST_EX(*(++mym.find(420)), !=, 1337));

		}

		perf_test(inst, 100000, 1000000, true);
		perf_test(inst, 100000, 10000000, true);
		perf_test(inst, 100000, -1, false);
		perf_test(inst, 1000, -1, false);


		//TODO: convert to real tests
		container::type_monarch monarch;

		container::type_lease<bool> lease(monarch, "a cool type");
		container::type_lease<bool> lease2(monarch, "a second type");
		container::type_lease<bool> lease3(monarch, "a third type");

		monarch.id_classification.push_back(std::vector{false,true});

		std::cout << monarch.db_get_classifications(0) << std::endl;

		container::type_mixture mixture(monarch, std::vector<container::lease_info const *>{&lease, &lease2});

		auto et = mixture.create_entity();
		std::cout << monarch.db_get_classifications(et) << std::endl;

		container::type_mixture mixture2(monarch, std::vector<container::lease_info const *>{&lease3},mixture);

		auto et2 = mixture2.create_entity();
		std::cout << monarch.db_get_classifications(et2) << std::endl;

		container::event_type<bool> my_et(monarch, "eventtype");

		my_et.add_listener([](size_t id, bool in)
		{
			printf("ev %lu %d\n", id, in);
		});

		monarch.event_callbacks[0][0](27, std::any(false));
		
		my_et.trigger(999, true);
		*/
	}
}
