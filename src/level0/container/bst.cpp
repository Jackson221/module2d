//Copyright 2017-2019 Jackson Reed McNeill. Licensed under the GNU AGPL version 3 or any later version.
#include "bst.h"

#include "test/test.hpp"


namespace container
{
	constexpr char bst_test_msg[] = "Key: %d% data: %d%";
	void unit_test_bst(test::instance& inst)
	{
		inst.set_name("Container");
		{
			binary_search_tree<int,int> mybst;	

			mybst.insert(5,0);
			mybst.insert(6,1);
			mybst.insert(7,2);
			mybst.insert(8,3);

			mybst.remove(8);

			inst.inform("Ensure deletion of childless node worked");
			inst.test(TEST_1(mybst.root->right->right->right, mybst.root->right->right->right==NULL,true));

			mybst.remove(6);

			inst.inform("Ensure deletion of 1 child node worked");
			inst.test(TEST_1( mybst.root->right->key, mybst.root->right->key==7,true));
		}
		{

			binary_search_tree<int,int> mybst;	

			mybst.insert(50,0);
			mybst.insert(30,1);
			mybst.insert(20,2);
			mybst.insert(40,3);
			mybst.insert(70,4);
			mybst.insert(60,5);
			mybst.insert(80,6);
			mybst.insert(79,7);

			mybst.remove(70);

			inst.inform("Ensure deletion of 2 child node worked");
			inst.test(TEST_1( mybst.root->right->key, mybst.root->right->key==79,true));

		}
		{

			binary_search_tree<int,int> mybst;	

			mybst.insert(50,0);
			mybst.insert(30,1);
			mybst.insert(20,2);
			mybst.insert(40,3);
			mybst.insert(70,4);
			mybst.insert(60,5);
			mybst.insert(80,6);
			mybst.insert(79,7);

			int result = mybst.search(80).value();
			int result2 = mybst.search(50).value();
			int result3 = mybst.search(20).value();

			inst.inform("Ensure search works");
			inst.test(TEST_1( result, result==6,true));
			inst.test(TEST_1( result2, result2==0,true));
			inst.test(TEST_1( result3, result3==2,true));
			inst.inform("Ensure inorder traverse works");

		}
		{
			auto v = vector2d<int>(100,200);
			v[0][0] = 7;
			v[100-1][200-1] = 200;

			inst.inform("Ensure vector2d works");
			inst.test(TEST_1(v[0][0],v[0][0]==7,true));
			inst.test(TEST_1(v[100-1][200-1],v[100-1][200-1]==200,true));


		}
	}
}
