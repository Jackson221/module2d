/*!
 * Provides a "vector"-like class that preserves the ID of all elements
 *
 * Copyright 2017-2019 Jackson Reed McNeill. Licensed under the GNU AGPL version 3 or any later version.
 */
#pragma once

#include <iterator>
#include <vector>
#include <memory>
#include <exception>
#include <stdexcept>
#include <string>
#include <any>
#include <functional>

#include "test/forward.hpp"
#include "synchro/synchro.hpp"

namespace container
{
	/*!
	 * A container which provides linear traverse times as fast as a std::vector, and random-access times only
	 * twice as slow as a std::vector. In many ways, it acts as a fast std::map<size_t, T>
	 *
	 * The drawbacks are that IDs must be assigned in ascending order, and random erasure is very slow.
	 * Additionally, the memory overhead grows as the maximum-used-ID grows, and as sparsity increases
	 * (i.e. there are unused IDs in the container) the container becomes less effecient for all except
	 * insertion and linear traversal.
	 *
	 * The memory overhead of storing 1 element at ID 100,000 would be 1MB, for example. This is more of a 
	 * concern for data locality than RAM usage.
	 *
	 * Useful for ECS-style systems.
	 */
	template<typename _value_type, typename _allocator_type = std::allocator<_value_type>>
	class idmap
	{
		protected:
			using storage_t = std::vector<_value_type>;
			storage_t storage;
			using _size_type = typename storage_t::size_type;
			std::vector<_size_type> storage_spot_to_id;
			std::vector<_size_type> id_to_storage_spot;

			size_t next_id = 0;


		public:
			SYNCHRO(idmap,storage,storage_spot_to_id,id_to_storage_spot,next_id)

			using value_type = _value_type;
			using allocator_type = _allocator_type;
			using size_type = typename storage_t::size_type;
			using difference_type = typename storage_t::difference_type;
			using reference = value_type &;
			using const_reference = value_type const &;
			using pointer = typename std::allocator_traits<allocator_type>::pointer;
			using const_pointer = typename std::allocator_traits<allocator_type>::const_pointer;

			class iterator : std::iterator
			<
				std::bidirectional_iterator_tag,//random_access_iterator_tag,
				size_type,
				difference_type
			>
			{
				friend class idmap;
				friend struct debug_access_iterator_host;
				idmap * host;
				template<bool backward>
				constexpr inline void move()
				{
					if constexpr(backward)
					{
						it_in_storage--;
					}
					else
					{
						if (it_in_storage == host->storage.end())
						{
								throw std::runtime_error("Cannot increment a past-the-end iterator");
						}
						it_in_storage++;
					}
				}
				protected:
					storage_t::iterator it_in_storage;
					constexpr explicit iterator(idmap* _host, storage_t::iterator _it) { host = _host; it_in_storage = _it; }	
				public:
					constexpr iterator(iterator const & other) :
						host(other.host),
						it_in_storage(other.it_in_storage)
					{
					}
					constexpr iterator(iterator && other) :
						host(std::move(other.host)),
						it_in_storage(std::move(other.it_in_storage))
					{
					}
					constexpr iterator& operator=(iterator const & other)
					{
						host = other.host;
						it_in_storage = other.it_in_storage;
						return *this;
					}
					constexpr iterator& operator=(iterator && other)
					{
						host = other.host;
						it_in_storage = other.it_in_storage;
						return *this;
					}
					constexpr iterator() { host = nullptr;}
					constexpr iterator& operator++()
					{
						move<false>();
						return *this;
					}
					constexpr iterator operator++(int)
					{
						iterator return_val = *this;
						move<false>();
						return return_val;
					}
					constexpr iterator& operator--()
					{
						move<true>();
						return *this;
					}
					constexpr iterator operator--(int)
					{
						iterator return_val = *this;
						move<true>();
						return return_val;
					}
					constexpr bool operator==(iterator other)
					{
						return other.it_in_storage == it_in_storage;
					}
					constexpr bool operator!=(iterator other)
					{
						return other.it_in_storage != it_in_storage;
					}
					constexpr reference operator*()
					{
						return *it_in_storage;
					}
					constexpr pointer operator->()
					{
						return &(**this);
					}
					//Gets the ID of the iterator. Only valid for a valid iterator, or a one-past-the-end iterator.
					//Complexity: O(1)
					constexpr size_type get_id()
					{
						if (it_in_storage == host->storage.end())
						{
							return host->next_id;
						}
						return host->storage_spot_to_id.at(std::distance(host->storage.begin(), it_in_storage));
					}
			};
			friend class iterator;


			using reverse_iterator = std::reverse_iterator<iterator>;

			constexpr idmap() {}

			//O(1)
			constexpr iterator begin()
			{
				return iterator(this,storage.begin());
			}
			//O(1)
			constexpr reference front()
			{
				return *begin();
			}
			//O(1)
			constexpr iterator end()
			{
				return iterator(this,storage.end());
			}
			//O(1)
			constexpr reference back()
			{
				return *(--end());
			}
			//O(1)
			constexpr bool empty()
			{
				return storage.empty();
			}
			//O(1)
			constexpr size_type size()
			{
				return storage.size();
			}
			//O(1)
			constexpr size_type max_size()
			{
				return storage.max_size();
			}
			//O(1)
			constexpr size_type real_size()
			{
				return storage.size();
			}
			/*!
			 * Reserves memory, with the assumption that you will insert new IDs in a sequential fashion.
			 * 
			 * Otherwise, this can reserve more memory than you will use.
			 */
			constexpr void reserve(size_type number)
			{
				storage.reserve(number);
				storage_spot_to_id.reserve(number);
				id_to_storage_spot.reserve(number);
			}
			constexpr size_type capacity()
			{
				return storage.capacity();
			}
			constexpr void shrink_to_fit()
			{
				storage.shrink_to_fit();
				storage_spot_to_id.shrink_to_fit();
				id_to_storage_spot.shrink_to_fit();
			}


			//Modifiers
			constexpr void clear()
			{
				storage.clear();
				storage_spot_to_id.clear();
				id_to_storage_spot.clear();
			}

			/*!
			 * Similar to insert, but can only insert at a past-the-end iterator
			 *
			 * Where k is id-next_id, O(k + 1)
			 */
			constexpr void push_back_at(size_t id, value_type const & val)
			{
				if (id >= next_id)
				{
					id_to_storage_spot.insert(id_to_storage_spot.end(), (id-next_id)+1,std::numeric_limits<size_type>::max());
					id_to_storage_spot.back() = storage.size();
					storage.push_back(val);
					storage_spot_to_id.push_back(id);
					next_id = id+1;
				}
				else
				{
					throw std::runtime_error("cannot insert id below the highest previously-used id"); //todo better error or give support for this operation
				}
			}
			constexpr void push_back_at(size_t id, value_type && val)
			{
				if (id >= next_id)
				{
					id_to_storage_spot.insert(id_to_storage_spot.end(), (id-next_id)+1,std::numeric_limits<size_type>::max());
					id_to_storage_spot.back() = storage.size();
					storage.push_back(std::move(val));
					storage_spot_to_id.push_back(id);
					next_id = id+1;
				}
				else
				{
					throw std::runtime_error("cannot insert id below the highest previously-used id"); //todo better error or give support for this operation
				}
			}
			
			//O(1), assuming there is capacity.
			constexpr size_t push_back(value_type const & val)
			{
				id_to_storage_spot.push_back(storage.size());
				storage.push_back(val);
				storage_spot_to_id.push_back(next_id);
				return next_id++;
			}
			constexpr size_t push_back(value_type && val)
			{
				id_to_storage_spot.push_back(storage.size());
				storage.push_back(std::move(val));
				storage_spot_to_id.push_back(next_id);
				return next_id++;
			}
			//Where k is the number of items to the left of the provided iterator, O(k + 1)
			//
			//This tends to be the worst performing operation of the container. It's no less than 2 orders of magnitude slower than
			//erasing on a std::unordered_map
			constexpr iterator erase(iterator next)
			{
				iterator pos = next++;

				//shift all id-to-storage-spots which occur after the iterator down by 1 to account for the erasure.
				for (auto it = id_to_storage_spot.begin()+pos.get_id()+1;it < id_to_storage_spot.end(); it++)
				{
					--(*it);
				}
				storage_spot_to_id.erase(storage_spot_to_id.begin() + std::distance(storage.begin(), pos.it_in_storage));
				storage.erase(pos.it_in_storage);
				
				return next;
			}
			//O(1)
			constexpr void pop_back()
			{
				remove(--end());
			}
			//O(1), the cost of two vector indexes.
			constexpr reference operator[](size_type id)
			{
				return storage[id_to_storage_spot[id]];
			}
			//O(1), the cost of two vector indexes.
			constexpr reference at(size_type id)
			{
				return storage.at(id_to_storage_spot.at(id));
			}
			constexpr const_reference operator[](size_type id) const 
			{
				return storage[id_to_storage_spot[id]];
			}
			constexpr const_reference at(size_type id) const 
			{
				return storage.at(id_to_storage_spot.at(id));
			}
			//O(1), the cost of a single vector index at most.
			constexpr iterator find(size_type const & id)
			{
				if (id < id_to_storage_spot.size())
				{
					size_t it_in_storage = id_to_storage_spot.at(id);
					if (it_in_storage != std::numeric_limits<size_type>::max())
					{
						return iterator(this, storage.begin() + it_in_storage);
					}
				}
				return end();
			}
	};

	


	//TODO: possibly make type_lease specify a ctor and dtor for the type? and other fns?

	template<typename entity_t>
	class type_lease;
	class type_mixture;
	template<typename arg_type_t>
	class event_type;
	using event_callback_t = std::function<void(size_t, std::any)>;

	class type_monarch
	{
		idmap<std::string> types;

		public:
			

			std::string db_get_classifications(size_t id)
			{
				std::string rs = "";
				auto v_of_bool = id_classification[id];
				for (int i = 0; i < v_of_bool.size(); i++)
				{
					if (v_of_bool[i])
					{
						rs += types[i] + ", ";
					}
				}
				return rs.empty()? "" : rs.substr(0, rs.size()-2);
			}

			bool is_in_mixture(type_mixture const & mix, size_t id);

		public://protected:
			template<typename entity_t>
			friend class type_lease;
			template<typename arg_type_t>
			friend class event_type;

			inline size_t add_type(std::string name)
			{
				return types.push_back(name);
			}

			idmap<std::string> event_types;
			idmap<std::vector<event_callback_t>> event_callbacks;
			inline size_t add_event_type(std::string name)
			{
				size_t id = event_types.push_back(name);
				event_callbacks.push_back_at(id, {});
				return id;
			}

			//for a given id, get its classifications
			idmap<std::vector<bool>> id_classification;

			//for a given classification, get all objects of such a type.
			//TODO: a vector is a terrible choice here; takes a long-ass time to search and erase.
			//Could be mitigated by making it into a heap (via sorting?).
			//idmap<std::vector<size_t>> all_ids_of_given_type;


			//TODO: Do the above but for all type_mixtures which actually exist?
			//
			//NOTE: actually, the above is dumb, cause each type already has its own idmap that can be
			//iterated through at high effeciency. So only do it for mixtures.
	};

	
	template<typename arg_type_t>
	class event_type
	{
		size_t id;
		type_monarch * monarch_ptr;

		using arg_type_or_placeholder = std::conditional_t<std::is_same_v<arg_type_t, void>,char,arg_type_t>;

		//either select void(size_t, arg_type_t> or if arg_type_t is void, then select void(size_t)
		using callback_t = std::conditional_t
		<
			/*if  */ std::is_same_v<arg_type_t, void>,
			/*then*/ std::function<void(size_t)>,
			/*else*/ std::function<void(size_t, arg_type_or_placeholder)>
		>;
		public:
			inline event_type(type_monarch & monarch, std::string name) :
				id(monarch.add_event_type(name)),
				monarch_ptr(&monarch)
			{
			}
			inline void add_listener(callback_t callback)
			{
				monarch_ptr->event_callbacks.at(id).push_back([&](size_t subject_id, std::any in)
				{
					if constexpr(std::is_same_v<arg_type_t, void>)
					{
						callback(subject_id);
					}
					else
					{
						callback(subject_id, std::any_cast<arg_type_t>(in));
					}
				});
			}
			inline void trigger(size_t triggered_id, arg_type_or_placeholder && arg)
			{
				for(auto& callback : monarch_ptr->event_callbacks.at(id))
				{
					callback(triggered_id, std::any(arg));
				}
			}
			inline void trigger(size_t triggered_id)
				requires(std::is_same_v<arg_type_t,void>)
			{
				trigger(triggered_id, char{});
			}
	};

	class lease_info
	{
		public:
			size_t id;
			std::string name;
		protected:
			lease_info(type_monarch& monarch, std::string in_name) :
				id(monarch.add_type(in_name)),
				name(in_name)
			{
			}

	};

	
	/*!
	 * A type lease defines a type.
	 *
	 * Typically, these will just be global variables. There's no shame in it; 
	 * if it where possible these would be done at compile time (but this would
	 * cause modules to be too interconnected).
	 */
	template<typename entity_t>
	class type_lease : public lease_info
	{
		public:
			idmap<entity_t> component_map;
			inline type_lease(type_monarch& monarch, std::string in_name) : 
				lease_info(monarch, in_name)
			{
			}
	};

	/*!
	 * A mixture type which can create entities of its given mixture.
	 * 
	 * Again, probably instantiated as a global variable.
	 */
	class type_mixture
	{
		std::vector<size_t> type_ids;
		type_monarch* monarch_ptr;

		inline type_mixture() :
			monarch_ptr(nullptr)
		{
		}
		
		type_mixture( type_mixture const &) = delete;
		type_mixture( type_mixture && other) :
			type_ids(std::move(other.type_ids)),
			monarch_ptr(other.monarch_ptr),
			flags(other.flags)
		{
			other.monarch_ptr = nullptr;
		}

		public:
			std::vector<size_t> owned_ids;
			inline type_mixture(type_monarch& monarch, std::vector<lease_info const *> leases, type_mixture const & inherit_from = type_mixture()) : 
				monarch_ptr(&monarch)
			{
				if (inherit_from.monarch_ptr != nullptr) //if they passed something to inherit from...
				{
					flags = inherit_from.flags;
				}
				for (auto const * lease : leases)
				{
					flags.resize(std::max(leases.size(), lease->id+1));
					flags[lease->id] = true;
					type_ids.push_back(lease->id);
				}
			}
			constexpr inline size_t create_entity()
			{
				size_t id = monarch_ptr->id_classification.push_back(flags);

				/*for (auto type_id : type_ids)
				{
					all_ids_of_given_type[type_id].push_back(id);
				}*/
				return id;
			}
		protected:
			friend class type_monarch;
			std::vector<bool> flags;
	};

	inline bool type_monarch::is_in_mixture(type_mixture const & mix, size_t id)
	{
		for (size_t i = 0; i < mix.flags.size(); i++)
		{
			if (mix.flags[i] && !id_classification[id][i])
			{
				return false;
			}
		}
		return true;
	}

	void unit_test_idmap(test::instance & inst);	
}
