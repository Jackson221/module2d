/*!
 * @file
 *
 * @author Jackson McNeill
 *
 * Numerical Binary Search Tree
 *
 * Candidate for deletion (use a sorted vector)
 *
 * Copyright 2017-2019 Jackson Reed McNeill. Licensed under the GNU AGPL version 3 or any later version.
 */
#pragma once

#include <iterator>
#include <optional>
#include <functional>

#include "test/forward.hpp"

namespace container
{
	void unit_test_bst(test::instance& inst);

	template<class key_type, class value_type>
	class bst_node
	{
		public:
			bst_node(key_type key, value_type data)
			{
				this->key = key;
				this->data = data;

				left = NULL;
				right=NULL;
			}
			inline char child_count()
			{
				char count = 0;
				if (left != NULL)
				{
					count++;
				}
				if (right != NULL)
				{
					count++;
				}
				return count;
			}
			void copyto(bst_node<key_type,value_type>* node)
			{	
				node->key = key;
				node->data = data;
			}
			void copyfrom(bst_node<key_type,value_type>* node)
			{
				key = node->key;
				data = node->data;
			}

			bst_node* left;
			bst_node* right;
			key_type key;
			value_type data;
	};


	template<class T>
	class optional_reference
	{
		T* data;
		bool exists;
		public:
			optional_reference(T& data)
			{
				exists = true;
				this->data = &(data);
			}
			optional_reference()
			{
				exists = false;
			}
			bool has_value()
			{
				return exists;
			}
			T& value()
			{
				return *data;
			}
	};
	/*!
	 * key_type must be an arithmatic type
	 *
	 * value_type may be anything
	 *
	 * Iterators aren't yet supported.
	 */
	template<class key_type, class value_type>
	class binary_search_tree
	{
		friend void unit_test_bst(test::instance& inst);
		using node = bst_node<key_type,value_type>;

		bool prevdel = false;
		public:
			binary_search_tree()
			{
				root = nullptr;
			}
			~binary_search_tree()
			{
				if (prevdel)
				{
					printf("PREV DEL!!!!\n");
					exit(1);
				}
				prevdel= true;
				internal_postorder_traverse(root,[](node* current) -> bool
				{
					delete current;
					return false;
				});
			}
			void insert(key_type key, value_type data)
			{
				internal_insert(root,&root,key,data);
			}
			optional_reference<value_type> search(key_type key)
			{
				auto result = internal_search(root,nullptr,key);
				if (result.has_value())
				{
					return {result.value().current->data};
				}
				else
				{
					return {};
				}
			}
			value_type& operator[](key_type key)
			{
				return search(key).value();
			}
			void remove(key_type key)
			{
				auto found_node = internal_search(root,nullptr,key);
				if (found_node.has_value())
				{
					node* current = found_node.value().current;
					node* parent = found_node.value().parent;
					node** current_pointer_from_parent = found_node.value().current_pointer_from_parent;

					if (current->left != nullptr && current->right != nullptr)
					{
						node** leaf = minValueLeaf(&current->right);	
						(*leaf)->copyto(current);
						delete *leaf;
						*leaf = nullptr;
					}
					else
					{
						if (current->left != nullptr)
						{
							*current_pointer_from_parent = current->left;
						}
						else if (current->right != nullptr)
						{
							*current_pointer_from_parent = current->right;
						}
						else
						{
							*current_pointer_from_parent = nullptr;
						}
						
						delete current;
					}
				}
			}
			using traverse_function_t = std::function<bool(key_type,value_type)>;
			void preorder_traverse(traverse_function_t func)
			{
				internal_preorder_traverse(root,[func](node* current)
				{
					return func(current->key,current->data);
				});
			}
			void inorder_traverse(traverse_function_t func)
			{
				internal_inorder_traverse(root,[func](node* current)
				{
					return func(current->key,current->data);
				});
			}
			void postorder_traverse(traverse_function_t func)
			{
				internal_postorder_traverse(root,[func](node* current)
				{
					return func(current->key,current->data);
				});
			}
		protected:
			node* root;
		private:
			using internal_traverse_function_t = std::function<bool(node* data)>;
			inline bool internal_preorder_traverse(node* current, internal_traverse_function_t func)
			{
				if (current != nullptr)
				{
					if (func(current))
					{
						return true;
					}
					if (internal_preorder_traverse(current->left,func))
					{
						return true;
					}
					if (internal_preorder_traverse(current->right,func))
					{
						return true;
					}
				}
				return false;
			}
			inline bool internal_inorder_traverse(node* current, internal_traverse_function_t func)
			{
				if (current != nullptr)
				{
					if (internal_inorder_traverse(current->left,func))
					{
						return true;
					}
					if (func(current))
					{
						return true;
					}
					if (internal_inorder_traverse(current->right,func))
					{
						return true;
					}
				}
				return false;
			}
			inline bool internal_postorder_traverse(node* current, internal_traverse_function_t func)
			{
				if (current != nullptr)
				{
					if(internal_postorder_traverse(current->left,func))
					{
						return true;
					}
					if (internal_postorder_traverse(current->right,func))
					{
						return true;
					}
					if(func(current))
					{
						return true;
					}
				}
				return false;
			}
			inline void internal_insert(node* current, node** current_ptr, key_type key, value_type data)
			{
				if(current==nullptr)
				{
					*current_ptr = new node(key,data);
				}
				else if (current->key == key)
				{
					current->data = data;
				}
				//NULL left/right are handled
				else if (key > current->key)
				{
					internal_insert(current->right,&current->right,key,data);
				}
				else //key < current->key
				{
					internal_insert(current->left,&current->left,key,data);
				}
			}
			struct internal_search_return
			{
				node* current;
				node* parent;

				node** current_pointer_from_parent;
			};
			inline std::optional<internal_search_return> internal_search(node* current,node* parent, key_type key)
			{
				if (current==nullptr)
				{
					return {};
				}
				else if (current->key == key)
				{
					/*if (parent == nullptr)
					{
						printf("par is nullptr\n");
					}
					else printf("parent is %p\n",parent);*/
					return {{
						current,
						parent,
						(parent == nullptr)? 
							&root : //if there is no parent then it must be the original root. 
							(parent->left == current? &parent->left : &parent->right)
					}};
				}
				else if (key > current->key)
				{
					return internal_search(current->right,current,key);
				}
				else//key < current->key
				{
					return internal_search(current->left,current,key);
				}
			}
			inline node** minValueLeaf(node** current_pointer)
			{
				node* current = *current_pointer; 
				if (current->left != nullptr)
				{
					return minValueLeaf(&current->left);
				}
				else
				{
					return current_pointer;
				}
			}
	};

	/*!
	 * A minimalist expandable 2d array.
	 *
	 * It doesn't quite behave like a vector, and it doesn't quite behave like an array because it is resizable. 
	 *
	 * Iterators aren't yet supported;
	 */
	template<class value_type>
	class vector2d
	{
		using vector_t = std::vector<value_type>;
		public: 
			struct index_result
			{
				size_t sizex;
				vector_t* vector;	
				size_t index_x;
				inline value_type& operator[](size_t index_y)
				{
					return at(index_y);
				}
				inline value_type& at(size_t index_y)
				{
					return vector->at((index_y*sizex) + index_x);	
				}
			};
			vector2d(size_t sizex, size_t sizey)
			{
				resize(sizex,sizey);
			}
			void resize(size_t sizex, size_t sizey)
			{
				this->sizex = sizex;
				this->sizey = sizey;
				vector.resize(sizex*sizey);
			}
			struct size_return
			{
				size_t sizex;
				size_t sizey;
			};
			size_return size()
			{
				return {sizex,sizey};
			}
			inline index_result at(size_t index_x)
			{
				return {sizex,&vector,index_x};
			}
			inline index_result operator[](size_t index_x)
			{
				return at(index_x);
			}
			~vector2d()
			{

			}
		private:
			vector_t vector;

			size_t sizex;
			size_t sizey;



	};



}
