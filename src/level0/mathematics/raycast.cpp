//Copyright 2017-2019 Jackson Reed McNeill. Licensed under the GNU AGPL version 3 or any later version.
#include "raycast.h"

#include "render/debug.hpp"
#include "simulation/physics.hpp"
#include "mathematics/common_ops.hpp"





std::optional<mm::vec2> GE_Raycast(mm::vec2 start, mm::vec2 end, GE_ShapePointsVector obstacles)
{
	std::optional<mm::vec2> currentShortest = {};
	for (auto shape : obstacles)
	{
		if (shape.begin() == shape.end())
		{
			//printf("error - shape end equals begin\n");
			break;
		}
		GE_Line rayLine = GE_GetLine(start,end);
		auto lastSpot = shape.begin();
		for (auto spot = std::begin(shape);spot!= std::end(shape);spot++)
		{
			GE_Line shapeLine = GE_GetLine(*lastSpot,*spot);	
			std::optional<mm::vec2> runnerUp = GE_LineIntersectionVector(rayLine,shapeLine);

			//ensure collision is in range of ray, and shape
			if (runnerUp.has_value() && mm::is_in_range(runnerUp.value().x,start.x,end.x) && mm::is_in_range(runnerUp.value().x,lastSpot->x,spot->x))
			{
				if (mm::is_in_range(runnerUp.value().y,start.y,end.y) && mm::is_in_range(runnerUp.value().y,lastSpot->y,spot->y))
				{
					if(!currentShortest.has_value())
					{
						currentShortest = runnerUp;
					}
					else
					{
						currentShortest = {mm::closest_vec(start,runnerUp.value(),currentShortest.value())};
					}
				}
			}

			lastSpot = spot;
		}
	}
	return currentShortest; //return the last found shortest value


}


ObjectRaycastReturn GE_Raycast(sim::physen& physen, mm::vec2 start, mm::vec2 end, std::vector<sim::object*> objects)
{
	objects = physen.sort_by_closest_centroid(start,objects); //Sorts by position, but 'inaccurate' because it doesn't care about collision rectangles.
	mm::vec2 currentShortest = end;
	sim::object* currentShortestObj = NULL;
	for (sim::object* obj : objects)
	{
		GE_ShapePointsVector shapes;
		obj->collision_info.collisionRectangles.inorder_traverse([&](unsigned int i, mm::rectr rectangle)
		{
			mm::vec2 points[4];
			//GE_RectangleToPoints(rectangle,obj->collision_info.bounding_box,points,obj->position); //populate points
			GE_RectangleToPoints(rectangle,obj->collision_info.bounding_box/2.0,points,obj->position()-(obj->collision_info.bounding_box/2.0));
			//debug::drawRect(y);
			GE_ShapePoints shapeLines;
			//connect every point on the rectangle with lines
			shapeLines.insert(shapeLines.end(),points[0]);
			shapeLines.insert(shapeLines.end(),points[1]);
			shapeLines.insert(shapeLines.end(),points[3]);
			shapeLines.insert(shapeLines.end(),points[2]);
			shapeLines.insert(shapeLines.end(),points[0]); 
			shapes.insert(shapes.end(),shapeLines);
			return false;
		});
		auto result = GE_Raycast(start,end,shapes);
		if (!result.has_value())
		{
			continue;
		}
#if DEBUG_RENDERS_ENABLED()
		debug::text_at<debug::physics_position_modifier>(debug::transient_object_pusher{},std::to_string(mm::distance(start,result.value()).get()),result.value());
#endif
		if (mm::distance(start,result.value()) <= mm::distance(start,currentShortest))
		{
			currentShortest = result.value();
			currentShortestObj = obj;
		}
		else if(currentShortestObj != NULL)
		{
			break;
		}

	}
	return {currentShortestObj,currentShortest};
}


	/*
	std::vector<sim::object*> collisionRectangleToOwner;
	GE_ShapePointsVector shapes;	
	int ammountShapes = 0;
	for (sim::object* obj : objects)
	{
		for (int i=0;i<obj->numCollisionRectangles;i++)
		{
			mm::vec2 points[4];
			GE_RectangleToPoints(obj->collisionRectangles[i],obj->grid,points,obj->position);
			GE_ShapePoints shapeLines;
			shapeLines.insert(shapeLines.end(),points[0]);
			shapeLines.insert(shapeLines.end(),points[1]);
			shapeLines.insert(shapeLines.end(),points[3]);
			shapeLines.insert(shapeLines.end(),points[2]);
			shapeLines.insert(shapeLines.end(),points[0]);
			shapes.insert(shapes.end(),shapeLines);
			ammountShapes++;
			collisionRectangleToOwner.insert(collisionRectangleToOwner.end(),obj);
		}

	}
	int shapenumber = GE_Raycast(start,end,shapes).identity;
	return (shapenumber>ammountShapes)? NULL : collisionRectangleToOwner[shapenumber];
	
}

*/
