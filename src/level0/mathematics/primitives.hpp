/*!
 * Common math class defintions
 */
#pragma once
#include <cmath>
#include <type_traits>
#include <vector>

#include "synchro/enumeration.hpp"

#ifndef M_PI
#define M_PI 3.1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348253421170679
#endif

#define PRIMITIVES_GENERATE_OPEQUALS(op1,op2)\
template<typename T>\
inline constexpr auto & operator op1 ( T const & in)\
{\
	*this = *this op2 in;\
	return *this;\
}

namespace mm
{
	constexpr double RAD_TO_DEG = 180.0 / M_PI;
	constexpr double TWO_PI = 2 * M_PI;

	struct vec2r;
	/*!
	 * A point in 2D space with no rotation. 
	 */
	struct vec2 
	{
		double x;
		double y;
		inline explicit operator vec2r();

		SYNCHRO_STRUCT_CONSTEXPR(vec2,x, y)
		template<typename T>
		inline constexpr std::enable_if_t<std::is_arithmetic_v<T>,vec2> operator+(T const other) const 
		{
			vec2 newVector = {this->x+other,this->y+other};
			return newVector;
		}
		template<typename T>
		inline constexpr std::enable_if_t<std::is_arithmetic_v<T>,vec2> operator-(T const other) const
		{
			vec2 newVector = {this->x-other,this->y-other};
			return newVector;
		}
		template<typename T>
		inline constexpr std::enable_if_t<std::is_arithmetic_v<T>,vec2> operator*(T const other) const
		{
			vec2 newVector = {this->x*other,this->y*other};
			return newVector;
		}
		template<typename T>
		inline constexpr std::enable_if_t<std::is_arithmetic_v<T>,vec2> operator/(T const other) const
		{
			vec2 newVector = {this->x/other,this->y/other};
			return newVector;
		}
		template<class XY>
		inline constexpr std::enable_if_t<!std::is_arithmetic_v<XY>,vec2> operator+(XY const other) const
		{
			vec2 newVector = {this->x+other.x,this->y+other.y};
			return newVector;
		}
		template<class XY>
		inline constexpr std::enable_if_t<!std::is_arithmetic_v<XY>,vec2> operator-(XY const other) const
		{
			vec2 newVector = {this->x-other.x,this->y-other.y};
			return newVector;
		}
		template<class XY>
		inline constexpr std::enable_if_t<!std::is_arithmetic_v<XY>,vec2> operator*(XY const other) const
		{	
			vec2 newVector = {this->x*other.x,this->y*other.y};
			return newVector;
		}
		template<class XY>
		inline constexpr std::enable_if_t<!std::is_arithmetic_v<XY>,vec2> operator/(XY const other) const
		{	
			vec2 newVector = {this->x/other.x,this->y/other.y};
			return newVector;
		}
		inline vec2& abs()
		{
			x = std::abs(x);
			y = std::abs(y);

			return *this;
		}
		inline vec2& round()
		{
			x = std::floor(x+0.5);
			y = std::floor(y+0.5);

			return *this;
		}
		inline vec2& floor()
		{
			x = std::floor(x);
			y = std::floor(y);

			return *this;
		}
		inline vec2 min_between(vec2 const & other)
		{
			x = std::min(x, other.x);
			y = std::min(y, other.y);

			return *this;
		}
		inline vec2 max_between(vec2 const & other)
		{
			x = std::max(x, other.x);
			y = std::max(y, other.y);

			return *this;
		}
		inline double magnitude()
		{
			return std::sqrt(std::pow(y,2)+std::pow(x,2));
		}
		template<class XY>
		inline constexpr bool operator>(XY const other) const
		{
			return ( (x > other.x) && (y > other.y) );
		}
		template<class XY>
		inline constexpr bool operator<(XY const other) const
		{
			return ( (x < other.x) && (y < other.y) );
		}
		template<class XY>
		inline constexpr bool operator>=(XY const other) const
		{
			return ( (x >= other.x) && (y >= other.y) );
		}
		template<class XY>
		inline constexpr bool operator<=(XY const other) const
		{
			return ( (x <= other.x) && (y <= other.y) );
		}
		template <class XY>
		inline constexpr vec2 operator=(XY const other) const
		{
			std::swap(x,other.x);
			std::swap(y,other.y);
			return *this;

		}
		template<class XY>
		inline constexpr bool operator==(XY const other) const
		{
			return ( (x == other.x) && (y == other.y) );
		}
		inline vec2 operator- ()
		{
			vec2 other = *this;
			other.x *= -1;
			other.y *= -1;
			return other;
		}
		inline std::string to_string()
		{
			return "{"+std::to_string(x)+", "+std::to_string(y)+"}";
		}

		PRIMITIVES_GENERATE_OPEQUALS(+=,+)
		PRIMITIVES_GENERATE_OPEQUALS(-=,-)
		PRIMITIVES_GENERATE_OPEQUALS(*=,*)
		PRIMITIVES_GENERATE_OPEQUALS(/=,/)
	};
	/*!
	 * A point in 2D space with rotation
	 */
	struct vec2r
	{
		double x;
		double y;
		double r;


		SYNCHRO_STRUCT_CONSTEXPR(vec2r,x, y, r)

		inline explicit operator vec2()
		{
			return vec2{x,y};
		}

		/*
		vec2r(double x, double y,double r)
		{
			this->x=x;
			this->y=y;
			this->r=r;
		}
		vec2r() {} //allow creation of empty vector
		
		*/
		inline double magnitude()
		{
			return std::sqrt(std::pow(y,2)+std::pow(x,2));
		}
		inline constexpr vec2r operator+(vec2r other) const
		{
			vec2r newVector = {this->x+other.x,this->y+other.y,this->r+other.r};
			//GE_CanonicalizeRotation(&newVector.r);

			return newVector;
		}
		inline constexpr vec2r operator-(vec2r other) const
		{
			vec2r newVector = {this->x-other.x,this->y-other.y,this->r-other.r};
			//GE_CanonicalizeRotation(&newVector.r);

			return newVector;
		}
		inline constexpr vec2r operator*(vec2r other) const
		{	
			vec2r newVector = {this->x*other.x,this->y*other.y,this->r*other.r};
			//GE_CanonicalizeRotation(&newVector.r);

			return newVector;
		}
		inline constexpr vec2r operator/(vec2r other) const
		{	
			vec2r newVector = {this->x/other.x,this->y/other.y,this->r/other.r};
			//GE_CanonicalizeRotation(&newVector.r);

			return newVector;
		}
		inline constexpr vec2r operator*(double other) const
		{
			vec2r newVector = {this->x*other,this->y*other,this->r*other};
			//GE_CanonicalizeRotation(&newVector.r);

			return newVector;
		}
		inline constexpr vec2r operator/(double other) const
		{
			vec2r newVector = {this->x/other,this->y/other,this->r/other};
			//GE_CanonicalizeRotation(&newVector.r);

			return newVector;
		}
		//TODO: Broken
		/*
		inline vec2r operator+=(vec2r other)
		{
			return operator+(other);
		}
		inline vec2r operator-=(vec2r other)
		{
			return operator-(other);
		}
		*/
		inline vec2r& abs()
		{
			x = std::abs(x);
			y = std::abs(y);
			r = std::abs(r);

			return *this;
		}
		inline vec2r& round()
		{
			x = std::floor(x+0.5);
			y = std::floor(y+0.5);
			r = std::floor(r+0.5);

			return *this;
		}
		inline vec2r& floor()
		{
			x = std::floor(x);
			y = std::floor(y);
			r = std::floor(r);

			return *this;
		}

		//vec2 operations

		inline constexpr vec2r operator+(vec2 other) const
		{
			vec2r newVector = {this->x+other.x,this->y+other.y,this->r};
			return newVector;
		}
		inline constexpr vec2r operator-(vec2 other) const
		{
			vec2r newVector = {this->x-other.x,this->y-other.y,this->r};
			return newVector;
		}
		inline constexpr vec2r operator*(vec2 other) const
		{	
			vec2r newVector = {this->x*other.x,this->y*other.y,this->r};
			return newVector;
		}
		template <class XY>
		inline constexpr vec2r operator=(XY other) const
		{
			/*if constexpr(std::is_pointer<XY>::value) //TODO: this doesn't work?
			{
				x = other->x;
				y = other->y;
				r = other->r;
			}
			else
			{
			*/
			
			std::swap(x,other.x);
			std::swap(y,other.y);
			std::swap(r,other.r);
			return *this;

		}
		template<class XYR>
		inline constexpr bool operator==(XYR other) const
		{
			return ( (x == other.x) && (y == other.y) && (r == other.r) );
		}
		inline vec2r operator- ()
		{
			vec2r new_vector = *this;
			new_vector.x *= -1;
			new_vector.y *= -1;
			new_vector.r *= -1;
			return new_vector;
		}
		inline std::string to_string()
		{
			return "{"+std::to_string(x)+", "+std::to_string(y)+", "+std::to_string(r)+"}";
		}
			
		PRIMITIVES_GENERATE_OPEQUALS(+=,+)
		PRIMITIVES_GENERATE_OPEQUALS(-=,-)
		PRIMITIVES_GENERATE_OPEQUALS(*=,*)
		PRIMITIVES_GENERATE_OPEQUALS(/=,/)
	};

	vec2::operator vec2r()
	{
		return vec2r{x,y,0};
	}

	/*
		x=0;y=0;w=10;h=10;

		   width
		x,y-----|
		|		|
		|		|
	  h |		|
	  e |		|
	  i |		| 
	  g |		|
	  h |		|
	  t |		|
		|-------|<-- 10,10 (0+10,0+10)

	*/

	struct rectr;

	/*! 
	 *	A 2D Rectangle. No additional operators provided.
	 */
	struct rect 
	{
		double x;
		double y;
		double w;
		double h;

		SYNCHRO_STRUCT_CONSTEXPR(rect,x, y,w, h)

		inline explicit operator rectr();

		inline rect& round()
		{
			x = std::floor(x+0.5);
			y = std::floor(y+0.5);
			w = std::floor(w+0.5);
			h = std::floor(h+0.5);

			return *this;
		}
		inline rect& floor()
		{
			x = std::floor(x);
			y = std::floor(y);
			w = std::floor(w);
			h = std::floor(h);

			return *this;
		}


	};
	struct rectr 
	{
		
		double x;
		double y;
		double r;
		double w;
		double h;

		SYNCHRO_STRUCT_CONSTEXPR(rectr,x, y, r, w, h)

		inline explicit operator rect()
		{
			return {x,y,w,h};
		}
		
	};

	rect::operator rectr()
	{
		return rectr{x,y,0.0,w,h};	
	}

	struct rect_points
	{
		vec2 top_left;
		vec2 top_right;
		vec2 bottom_left;
		vec2 bottom_right;

		SYNCHRO_STRUCT_CONSTEXPR(rect_points,top_left,top_right,bottom_left,bottom_right)
	};

	using simple_polygon_points = std::vector<vec2>;
}
#undef PRIMITIVES_GENERATE_OPEQUALS
