#pragma once

#include <tuple>

#include "primitives.hpp"
#include "common_ops.hpp"
#include "render/debug.hpp"
#include "synchro/enumeration.hpp"

#include "filesystem/filesystem.hpp"

namespace mm
{
	//CCW winding order
	struct closed_polygon
	{
		std::vector<mm::vec2> vertices;

		/*!
		 * Access vertices, but wrap -1 to the last vert and a past-the-end index to the first vert.
		 */
		mm::vec2& operator[](signed int i)
		{
			if (i == -1)
			{
				return vertices.back();
			}
			else if (i == vertices.size())
			{
				return vertices.front();
			}
			return vertices[i];
		}

		mm::vec2 const & operator[](signed int i) const
		{
			return (*const_cast<closed_polygon*>(this))[i];
		}
		//Gets the normal for a line ending at point i. 
		//It will not be a unit length vector, it will be arbitrary length.
		mm::vec2 get_normal(unsigned int i) const
		{
			return mm::normal_vector((*this)[i-1],(*this)[i]);
		}

		SYNCHRO(closed_polygon,vertices)
	};

	closed_polygon load_poly(std::string path)
	{
		auto tape = synchro::serialization_tape{};
		tape.data = filesystem::read_all_from_file(path);
		auto snapshot = synchro::initial_snapshot<closed_polygon>(tape);
		closed_polygon result;
		result = snapshot;
		return result;
	}
	void save_poly(std::string path, closed_polygon poly)
	{
		auto snapshot = synchro::initial_snapshot<closed_polygon>{poly};
		auto tape = synchro::serialization_tape{snapshot};
		filesystem::write_to_file(path, tape.data);
	}
	//May return a past-the-end point if the last line is picked
	std::optional<std::tuple<mm::vec2, size_t>> vertical_raycast(closed_polygon const & poly, mm::vec2 origin)
	{
		if (poly.vertices.size() < 3)
		{
			return std::nullopt; //TODO throw an error
		}
		auto last_point = poly[0];

		bool found = false;
		mm::vec2 current_closest_collision;
		size_t current_closest_line;
		mm::distance_squared current_distance;
		
		//intentionally run one-off the array due to custom closed_polygon operator[] function which 
		//wraps-around.
		for (auto line_it = 1; line_it != poly.vertices.size()+1; line_it++)
		{
			auto& line = poly[line_it];
			//optimization to avoid ray casting in most cases
			if (mm::is_in_range(origin.x, last_point.x, line.x))
			{
				auto shape_line = (line- last_point);
				double slope = mm::slope(last_point, line);
				double start_y = last_point.y-(slope*last_point.x);
				mm::vec2 raycast = {origin.x, slope*origin.x+start_y};
				mm::distance_squared new_distance = mm::distance(raycast,origin);

				//The vertices of the shape are points of discontinuity. They can cause miscalculations.
				//So, the patch for this here is to simply move the sample point slightly if it is right
				//over a vertex. 
				//
				//By using the value squared, there is a small amount of error here, but as the two variables
				//approach the same value, the error becomes smaller. Thus, since I am comparing for less than .1,
				//the error is minimal and a square root is not worth it.
				if (found && std::abs(new_distance.value_squared - current_distance.value_squared) < .1)
				{
					return vertical_raycast(poly, origin+mm::vec2{.5,0});
				}
				if (mm::is_in_range(raycast.x,last_point.x,line.x) && (!found || new_distance < current_distance))
				{
					/*if (i != 0 && mm::distance(raycast, last_point) < 1)
					{
						current_normal = mm::unit_vector(line.normal + poly.points[i-1].normal);
					}
					else if (i+2 != poly.points.size() && mm::distance(raycast, line) < 1)
					{
						current_normal = mm::unit_vector(line.normal + poly.points[i+1].normal);
					}
					else
					{
					}*/
					current_closest_collision = raycast;
					current_closest_line = line_it;
					current_distance = new_distance;
					found = true;
				}
			}
			last_point = line;
		}
		if (found)
		{
			return std::make_optional(std::make_tuple(current_closest_collision, current_closest_line));
		}
		return std::nullopt;
	}
	bool is_in_interior(closed_polygon const & poly, mm::vec2 test_point, mm::vec2 collision_spot, size_t line_it)
	{
		mm::vec2 last_point = poly[line_it-1];
		mm::vec2 line = poly[line_it];

		//take the dot product between the line created by the point and the collision point, and the normal vector of the collided line.
		//if it's negative, then we're in the interior.
		return mm::dot(test_point-collision_spot, poly.get_normal(line_it)) < 0;
	}
	bool point_in_rect(closed_polygon const & poly, mm::vec2 test_point)
	{
		auto result = vertical_raycast(poly, test_point);
		if (result)
		{
			auto [collision_spot, line_it] = *result;
			if (is_in_interior(poly, test_point, collision_spot, line_it))
			{
				return true;
			}
		}
		return false;
	}

#if DEBUG_RENDERS_ENABLED()
	template<typename position_modifier = debug::null_position_modifier>
	void debug_render_poly(closed_polygon const & poly, bool show_normals, mm::vec2 offset = {0,0})
	{
		if (poly.vertices.size() > 2) //more lenient with invalid polygons
		{
			auto last_point = poly[0];
			for (auto line_it = 1; line_it != poly.vertices.size()+1; line_it++)
			{
				auto l = poly[line_it];
				debug::draw_line<position_modifier>(debug::transient_object_pusher{},last_point+offset, l+offset,{25,100,25,255});

				if (show_normals)
				{
					auto midpoint = last_point + (l- last_point)/2.0;
					debug::draw_line<position_modifier>(debug::transient_object_pusher{},midpoint+offset, midpoint+(mm::unit_vector(poly.get_normal(line_it))*50.0+offset),{255,255,0,255});
				}

				last_point = l;
			}
		}
	}
#endif
};
