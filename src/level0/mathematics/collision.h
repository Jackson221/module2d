//Copyright 2017-2019 Jackson Reed McNeill. Licensed under the GNU AGPL version 3 or any later version.
#pragma once

#include <vector>

#include "mathematics/line.h"


namespace collision
{
	struct collision_point_t
	{
		mm::vec2 point;
		GE_Line line;
	};
	std::vector<collision_point_t> shape_collision(GE_ShapePoints shape1, GE_ShapePoints shape2);
	mm::vec2 collision_normal(std::vector<collision_point_t> collision_points);
}
