/*!
 * @file
 * @author Jackson McNeill
 *
 * Common math operations
 *
 * Copyright 2017-2019 Jackson Reed McNeill. Licensed under the GNU AGPL version 3 or any later version.
 */
#pragma once

#include <cmath>

#include "primitives.hpp"
/*!
 * "Math Module" namespace
 */
namespace mm
{

	template<class number>
	constexpr bool is_in_range(number const value, number const range1, number const range2)
	{
		if (value >= range1 && value <= range2)
		{
			return true;
		}
		if (value <= range1 && value >= range2)
		{
			return true;
		}
		return false;

	}
	//tested to be faster for doubles on intel x64
	template<typename number_type>
	constexpr number_type fast_fmod(number_type number,number_type maxval)
	{
		number_type maybe_floored_number = number/maxval;
		if constexpr(std::is_floating_point<number_type>::value)
		{
			maybe_floored_number = std::floor(maybe_floored_number);
		}
		return number-((maybe_floored_number)*maxval);
	}
	/*!
	  * Keeps rotation greater than 0, and less than two pi, without altering direction.
	  */
	inline constexpr double canonicalize_rotation(double rotation)
	{
		rotation /= TWO_PI;
		rotation += std::abs(floor(rotation)); //note the order: when a number is negative, floor will make the number more negative (e.x. -1.1 -> -2)
		rotation -= floor(rotation);
		rotation *= TWO_PI;

		return rotation;
	}

	template<typename T>
	concept has_xy = requires(T t)
	{
		{ t.x };
		{ t.y };
	};
	
	inline constexpr void rotation_cw(double& x, double& y, double rotation)
	{
		double sin_rotation = sin(rotation);
		double cos_rotation = cos(rotation);

		double new_x = (x*cos_rotation)-(y*sin_rotation);
		y =	(x*sin_rotation)+(y*cos_rotation);

		x = new_x;
	}
	inline constexpr has_xy auto rotation_cw(has_xy auto in, double rotation)
	{
		rotation_cw(in.x,in.y,rotation);
		return in;
	}
	inline constexpr vec2r rotation_cw(vec2r in)
	{
		rotation_cw(in.x,in.y,in.r);
		return in;
	}
	inline constexpr void rotation_ccw(double& x, double& y, double rotation)
	{
		double sin_rotation = sin(rotation);
		double cos_rotation = cos(rotation);

		double new_x = (x*cos_rotation)+(y*sin_rotation); 
		y =	-(x*sin_rotation)+(y*cos_rotation);

		x = new_x;
	}
	inline constexpr has_xy auto rotation_ccw(has_xy auto in, double rotation)
	{
		rotation_ccw(in.x,in.y,rotation);
		return in;
	}
	inline constexpr vec2r rotation_ccw(vec2r in)
	{
		rotation_ccw(in.x,in.y,in.r);
		return in;
	}
	/*!
	* Returns the result of a dot product with subject and subject2
	*/
	inline constexpr double dot(has_xy auto subject, has_xy auto subject2)
	{
		return (subject.x*subject2.x)+(subject.y*subject2.y);
	}
	
	inline constexpr double slope(has_xy auto start, has_xy auto end)
	{
		return (end.y-start.y)/(end.x-start.x);
	}
	
	struct distance_squared
	{
		double value_squared;

		inline constexpr double get()
		{
			return std::sqrt(value_squared);
		}
		inline constexpr double operator*()
		{
			return get();
		}
		inline constexpr std::partial_ordering operator<=> (distance_squared const & rhs)
		{
			return value_squared <=> rhs.value_squared;
		}
		inline constexpr std::partial_ordering operator<=> (double const & rhs)
		{
			return value_squared <=> (rhs*rhs);
		}
	};

	inline constexpr auto square(auto in)
	{
		return in * in;
	}

	inline constexpr distance_squared length(has_xy auto subject)
	{
		return distance_squared{square(subject.x) + square(subject.y)};
	}


	/*!
	 * Distance between x1, y1 and x2, y2
	 * @return The distance between x1, y1 and x2, y2, in a double
	 */
	inline constexpr distance_squared distance(double x1, double y1, double x2, double y2)
	{
		return distance_squared{(square(x2-x1))+(square(y2-y1))};
	}

	/*!
	 * Distance between 2 vectors
	 * @return The distance between the 2 vectors, in a double
	 */
	inline constexpr distance_squared distance(has_xy auto subject, has_xy auto subject2)
	{
		return distance(subject.x,subject.y,subject2.x,subject2.y);
	}


	constexpr has_xy auto closest_vec(has_xy auto origin, has_xy auto point1, has_xy auto point2)
	{
		if (distance(origin,point1) < distance(origin,point2))
		{
			return point1;
		}
		return point2;
	}
	constexpr has_xy auto furthest_vec(has_xy auto origin, has_xy auto point1, has_xy auto point2)
	{
		if (distance(origin,point1) > distance(origin,point2))
		{
			return point1;
		}
		return point2;
	}

	inline vec2 normal_vector(has_xy auto first, has_xy auto second)
	{
		return vec2{first.y-second.y,second.x-first.x};
	}
	inline vec2 normal_vector(has_xy auto in)
	{
		return vec2{-in.y,in.x};
	}
	inline vec2 normal_vector(double slope)
	{
		if (slope == 0)
		{
			return {0,1};
		}
		double new_slope = -1.0/slope;
		return {1,new_slope};
	}
	inline vec2 unit_vector(has_xy auto in)
	{
		double inverse_coeffecient = std::sqrt(square(in.x)+square(in.y));
		return vec2{in.x/inverse_coeffecient, in.y/inverse_coeffecient};
	}
	inline constexpr vec2 transform_shape_point_to_world_coords(vec2 point, vec2 host_center_point, mm::vec2r host_position)
	{
		//rotate shape point arround center point
		point -= host_center_point;
		point = mm::rotation_ccw(point,host_position.r);
		point += host_center_point;
		
		//move to final, host cordinate space.
		point += host_position;

		return point;
	}
	inline constexpr vec2 transform_rectr_point_to_world_coords(rectr rect, vec2 point, vec2 host_center_point, mm::vec2r host_position)
	{
		vec2 half_rect_size = vec2{rect.w,rect.h}/2;

		//rotate rect point arround center
		point -= half_rect_size;
		point = mm::rotation_ccw(point,rect.r);
		point += half_rect_size;

		//move to shape points's cordinate space
		point += rect;

		//translate the rect around the object.
		return transform_shape_point_to_world_coords(point, host_center_point, host_position);
	}
	inline constexpr rect_points transform_rectr_to_rect_points_in_world_coords(rectr rect, vec2 host_center_point, mm::vec2r host_position)
	{
		rect_points points;
		points.top_left = {0,0};
		points.top_right = {rect.w,0};
		points.bottom_left = {0,rect.h};
		points.bottom_right = {rect.w,rect.h};

		points.top_left = transform_rectr_point_to_world_coords(rect, points.top_left, host_center_point, host_position);
		points.top_right = transform_rectr_point_to_world_coords(rect, points.top_right, host_center_point, host_position);
		points.bottom_left = transform_rectr_point_to_world_coords(rect, points.bottom_left, host_center_point, host_position);
		points.bottom_right = transform_rectr_point_to_world_coords(rect, points.bottom_right, host_center_point, host_position);

		return points;
	}
	constexpr mm::vec2 get_size_of_rectangle(mm::vec2 top_left, mm::vec2 bottom_right, double rotation)
	{
		//no matter what, dist from TL to BR is pythag. So why is this needed?
		bottom_right = bottom_right-top_left;
		top_left = {0,0};

		mm::vec2 center_point = (top_left+bottom_right)/2.0;

		top_left = top_left-(center_point/2.0);
		bottom_right = bottom_right-(center_point/2.0);
		top_left = mm::rotation_cw(top_left,rotation);
		bottom_right = mm::rotation_cw(bottom_right,rotation);
		top_left = top_left+(center_point/2.0);
		bottom_right = bottom_right+(center_point/2.0);

		return {bottom_right.x-top_left.x,bottom_right.y-top_left.y};
	}
	inline mm::simple_polygon_points convert_rect_points_to_simple_polygon_points(rect_points rect)
	{
		mm::simple_polygon_points points_vector;
		points_vector.reserve(5);
		points_vector.push_back(rect.top_left);
		points_vector.push_back(rect.top_right);
		points_vector.push_back(rect.bottom_right);
		points_vector.push_back(rect.bottom_left);
		points_vector.push_back(rect.top_left);
		return points_vector;
	}
};

/*!
 * Converts a rectangle to 4 points in 2D space. A point in 2D space has no rotation, so it is described by a mm::vec2.
 * @param rect The rectangle to be converted
 * @param points An array that can contain 4 mm::vec2* s. Will be filled in the following order: Top left, top right, bottom left, bottom right
 * @param hostPosition a mm::vec2r , note the r, that the GE_Rectangle rect belongs to. It is used to be added to the positions of points, and its rotation will translate them.
 */
constexpr inline void GE_RectangleToPoints(mm::rectr rect, mm::vec2 center_point, mm::vec2* points, mm::vec2r hostPosition) 
{
	points[0] = {0 , 0}; //top left
	points[1] = {rect.w , 0}; //top right
	points[2] = {0 , rect.h}; //bottom left
	points[3] = {rect.w , rect.h}; //bottom right

	for (int i =0; i < 4; i++)
	{
		//rotate rectangle arround center
		points[i].x -= rect.w/2;
		points[i].y -= rect.h/2;
		points[i] = mm::rotation_ccw(points[i],rect.r);
		points[i].x += rect.w/2;
		points[i].y += rect.h/2;
		//move to rectangle's cordinate space
		points[i].x += rect.x;
		points[i].y += rect.y;

		//rotate rectangle arround center point
		points[i].x -= center_point.x;
		points[i].y -= center_point.y;
		points[i] = mm::rotation_ccw(points[i],hostPosition.r);
		points[i].x += center_point.x;
		points[i].y += center_point.y;
		
		//move to final, host cordinate space.
		points[i].x += hostPosition.x;
		points[i].y += hostPosition.y;
	}
}
