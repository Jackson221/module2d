/*!
 * Provides a simple class for managing collision rectangles.
 *
 * Copyright 2017-2019 Jackson Reed McNeill. Licensed under the GNU AGPL version 3 or any later version.
 */
#include "mathematics/primitives.hpp"
#include "mathematics/common_ops.hpp"
#include "container/bst.h"


class collision_rectangle_manager
{
	public:
		inline collision_rectangle_manager()
		{
			numCollisionRectangles = 0;
		}
		/*!
		 * Adds a collision rectangle to this object and updates cached values
		 */
		inline unsigned int addCollisionRectangle(mm::rectr newRectangle)
		{
			unsigned int ID = addCollisionRectangle_no_refresh_cache(newRectangle);
			refresh_collision_rectangle_cache();
			return ID;
		}

		/*!
		 * Modifies a collision rectangle and updates cached values
		 */
		inline void changeCollisionRectangle(unsigned int num, mm::rectr newRectangle)
		{
			changeCollisionRectangle_no_refresh_cache(num,newRectangle);
			refresh_collision_rectangle_cache();
		}

		constexpr static inline double SQRT_OF_TWO = sqrt(2.f);
		/*!
		 * Refreshes cached values for collision rectangles -- you may use this after modifications or use add/changeCollisionRectangle
		 */
		inline void refresh_collision_rectangle_cache()
		{
			mm::vec2 myPoints[4] = {};
			mm::vec2& bounding_box = this->bounding_box;
			bounding_box = {0,0};
			collisionRectangles.inorder_traverse([&bounding_box,&myPoints](int,mm::rectr rectangle)
			{
				GE_RectangleToPoints(rectangle,mm::vec2{0,0},myPoints,mm::vec2r{0,0,0});
				for (int o=0;o<4;o++)
				{
					bounding_box.x = fmax(bounding_box.x,myPoints[o].x);
					bounding_box.y = fmax(bounding_box.y,myPoints[o].y);
				}
				return false;
			});
			//see doc. for diameter in definition of physicsobject
			diameter = fmax(bounding_box.x,bounding_box.y)*SQRT_OF_TWO;
			radius = diameter/2;
		}

		/*!
		 * Adds a collision rectangle to this object 
		 */
		inline unsigned int addCollisionRectangle_no_refresh_cache(mm::rectr newRectangle)
		{
			unsigned int ID  =numCollisionRectangles;
			collisionRectangles.insert(ID,newRectangle);	
			numCollisionRectangles++;
			return ID;
		}

		/*!
		 * Modifies a collision rectangle 
		 */
		inline void changeCollisionRectangle_no_refresh_cache(unsigned int num, mm::rectr newRectangle)
		{
			auto result = collisionRectangles.search(num);
			if (result.has_value())
			{
				result.value() = newRectangle;
			}
		}
		container::binary_search_tree<unsigned int, mm::rectr> collisionRectangles;
		unsigned int numCollisionRectangles = 0;
		mm::vec2 bounding_box;//the bounding box is the smallest box that can fit all collision rectangles
		double diameter; //the maximum space the object can take up; used for collision checking optimizations
						//so essentially, the "diameter" is the side-length for a square which, no matter what rotation the object is in, it will not exeed that square.
		double radius; //diameter/2 , for optimization & convienience



};
