//Copyright 2017-2019 Jackson Reed McNeill. Licensed under the GNU AGPL version 3 or any later version.
#include "collision.h"
#include <array>
#include <optional>
#include <stdexcept>

#include "mathematics/common_ops.hpp"

struct line_plus_range
{
	GE_Line line;
	mm::vec2 start;
	mm::vec2 end;
};
using shape_lines_vector = std::vector<line_plus_range>;
shape_lines_vector get_shape_lines(GE_ShapePoints input)
{
	shape_lines_vector result;
	result.reserve(input.size());
	mm::vec2 lastPoint = *std::begin(input);
	for (auto it=std::begin(input)+1;it != std::end(input);it++)
	{
		result.push_back(line_plus_range{GE_GetLine(lastPoint,*it),lastPoint,*it});
		lastPoint = *it;
	}
	return result;
}
constexpr bool point_in_range_of_line(mm::vec2 const point, line_plus_range const line)
{
	return mm::is_in_range(point.x,line.start.x,line.end.x) && mm::is_in_range(point.y,line.start.y,line.end.y);
}
constexpr bool collision_in_range_of_lines(mm::vec2 const collision,line_plus_range const first,line_plus_range const second)
{
	return point_in_range_of_line(collision,first) && point_in_range_of_line(collision,second);

}

//#include "render/debug.hpp"
namespace collision
{

	std::vector<collision_point_t> shape_collision(GE_ShapePoints shape1, GE_ShapePoints shape2)
	{
		shape_lines_vector shapelines1 = get_shape_lines(shape1);
		shape_lines_vector shapelines2 = get_shape_lines(shape2);
		/*
		for (auto line : shapelines1)
		{
			GE_DEBUG_DrawLine(line.line);
		}
		for (auto line : shapelines2)
		{
			GE_DEBUG_DrawLine(line.line);
		}
		*/
		std::vector<collision_point_t> collision_points = {};
		for (auto line1_plus_range = std::begin(shapelines1);line1_plus_range!= std::end(shapelines1);line1_plus_range++)
		{
			for (auto line2_plus_range = std::begin(shapelines2);line2_plus_range!= std::end(shapelines2);line2_plus_range++)
			{
				std::optional<mm::vec2> collision_point = GE_LineIntersectionVector(line1_plus_range->line,line2_plus_range->line);
				if (collision_point.has_value())
				{
					if (collision_in_range_of_lines(collision_point.value(),*line1_plus_range,*line2_plus_range))
					{
						collision_points.push_back({collision_point.value(), line2_plus_range->line});
					}
				}
			}
		}
		return collision_points;
	}
	mm::vec2 collision_normal(std::vector<collision_point_t> collision_points)
	{
		if (collision_points.size() == 1)
		{
			GE_Line collision_line = collision_points[0].line;
			return collision_line.vertical? mm::vec2{0,1} : mm::normal_vector(collision_line.m);
		}
		else if (collision_points.size() > 1)
		{
			//TODO algorithm for > 2
			return mm::normal_vector(collision_points[0].point, collision_points[1].point);	
		}
		else
		{
			throw std::runtime_error("must have at least 1 collision point");
		}
	}
}


