/*!
 * @file
 * @author Jackson McNeill
 * Performs raycasting
 *
 * Copyright 2017-2019 Jackson Reed McNeill. Licensed under the GNU AGPL version 3 or any later version.
 */
#pragma once

#include "mathematics/line.h"
#include "mathematics/primitives.hpp"
#include <vector>
#include "simulation/physics.hpp"

struct ObjectRaycastReturn
{
	sim::object* victim;
	mm::vec2 position;
};

/*!
 * Raycasts using a line and shapes and returns the collision point. 
 */
std::optional<mm::vec2> GE_Raycast(mm::vec2 start, mm::vec2 end, GE_ShapePointsVector obstacles);
/*!
 * Raycasts using a line and an non-order-specific vector list of physics objects and returns the physics object hit and the spot hit
 */
ObjectRaycastReturn GE_Raycast(sim::physen& physen, mm::vec2 start, mm::vec2 end, std::vector<sim::object*> objects);


//sim::object* GE_Raycast(mm::vec2 start, mm::vec2 end, std::vector<sim::object*> objects);

