/*!
 * @author Jackson McNeill
 *
 * Provides synchronization of variables between threads using the synchro library
 */
#pragma once

#include <vector>
#include <array>

#include "synchro/synchro.hpp"
#include "variadic_util/variadic_util.hpp"

namespace synchrothread
{
	/*!
	 * Used for when the user does not specify an address of a bool to set to false when the object is dead.
	 *
	 * Since it is never read, it really doesn't have to be thread_local, but I'd rather not invoke unneccesary errors in gcc's thread sanitizer
	 */
	static inline thread_local bool null_is_alive;


	//TODO: Refactor into an entrance->exit node thingy? (to facilitate data transfer through multiple threads and probably through the network)
	//Probably will want to get a thread-only version working first.
	template<typename object_tuple>
	class instance
	{

		
		//both id types are size_t but aliasing them makes code more readable
		using provider_id_t = size_t;
		using reader_id_t = size_t;

		template<typename object>
		struct object_containers_t
		{
			//stores the provider ID -> provider object ptr
			std::map<provider_id_t,std::add_pointer_t<object>> map_of_provider_object_ptrs;

			//stores the provider id -> its snapshot
			std::map<provider_id_t,synchro::resync_snapshot<object>> map_of_snapshots;
			
			//stores the next provider id to be used
			provider_id_t next_provider_id = 0;

			//stores all the readers of a given provider id
			std::map<provider_id_t, std::map<reader_id_t,std::add_pointer_t<object>>> readers_by_provider_id;

			//stores the next id of a reader of a given provider id
			std::map<provider_id_t, reader_id_t> next_reader_id_by_provider_id;

			//cleanup handling section:

			//stores the providerIDs which have recently been killed so that their
			//readers can be informed of their death
			std::vector<provider_id_t> recently_deleted_providers;

			//contains pointers to a bool that will be set to false upon (the provider that a reader is readering)'s death.
			std::map<provider_id_t, std::map<reader_id_t, bool*>> is_alive_ptrs_by_provider_id;
		};

		variadic_util::make_variadic_of_template_applied_to_variadic_contents_t<object_containers_t, object_tuple>
			tuple_of_object_containers;



		template<typename object_t>
		constexpr auto& get_containers()
		{
			constexpr std::optional<size_t> object_iterator = variadic_util::index_of_type_in_variadic<object_tuple, object_t>();

			static_assert(object_iterator.has_value(), "the object registered must be within this `instance`'s `object_tuple` template arguments.");

			return std::get<object_iterator.value()>(tuple_of_object_containers);
		}

		public:

			/*!
			 * Adds an object, with the desired sync_type to be used, to this instances records.
			 *
			 * The object will then by synced on every sync tick (TODO: work on wording)
			 *
			 * The sync type is needed since a single object can be registed multiple times under different
			 * sync_types, and this function needs to know which one to use
			 *
			 * Returns the ID of the object, which is unique __but only for this sync_type/object_t pair__
			 */
			template<typename object_t>
			size_t register_data_provider_object(object_t& object)
			{
				auto& containers = get_containers<object_t>();

				//insert the ptr
				containers.map_of_provider_object_ptrs.insert(containers.map_of_provider_object_ptrs.end(), 
						std::make_pair( containers.next_provider_id, &object));

				//create the map for reader IDs
				using reader_id_map = typename decltype(containers.readers_by_provider_id)::mapped_type;
				containers.readers_by_provider_id.insert(containers.readers_by_provider_id.end(),
						std::make_pair( containers.next_provider_id , reader_id_map{} ));

				//create the next reader ID, which starts at 0
				containers.next_reader_id_by_provider_id.insert(containers.next_reader_id_by_provider_id.end(), 
						std::pair( containers.next_provider_id, 0));

				//create the map for is_alive pointers
				using reader_id_map_for_is_alive = typename decltype(containers.is_alive_ptrs_by_provider_id)::mapped_type;
				containers.is_alive_ptrs_by_provider_id.insert(containers.is_alive_ptrs_by_provider_id.end(),
						std::make_pair( containers.next_provider_id , reader_id_map_for_is_alive{} ));


				return containers.next_provider_id++;
			}
			template<typename object_t>
			void remove_data_provider_object(size_t id)
			{
				auto& containers = get_containers<object_t>();

				containers.map_of_provider_object_ptrs.erase(id);
				//TODO handling of removing the other side (i.e. reader maps) and notifying users of the removing
				containers.recently_deleted_providers.push_back(id);
			}

			template<typename object_t>
			size_t register_data_reader_object(size_t id_to_sync_to, object_t& object, bool& is_alive_ref = null_is_alive)
			{
				auto& containers = get_containers<object_t>();

				size_t& id = containers.next_reader_id_by_provider_id[id_to_sync_to];

				containers.readers_by_provider_id[id_to_sync_to][id] = &object;

				containers.is_alive_ptrs_by_provider_id[id_to_sync_to][id] = &is_alive_ref;
				is_alive_ref = true;

				return id++;
			}

			template<typename object_t>
			void remove_data_reader_object(size_t id_to_sync_to, size_t id_of_reader)
			{
				auto& containers = get_containers<object_t>();

				containers.readers_by_provider_id[id_to_sync_to].erase(id_of_reader);

				*containers.is_alive_ptrs_by_provider_id[id_to_sync_to][id_of_reader] = false;
				containers.is_alive_ptrs_by_provider_id[id_to_sync_to].erase(id_of_reader);
			}

			void write_providers()
			{
				variadic_util::for_each_iterator<variadic_util::variadic_size_v<object_tuple>>([&]<size_t i>()
				{
					using object_type = variadic_util::variadic_element_t<i,object_tuple>;

					auto& containers = std::get<i>(tuple_of_object_containers);

					for(auto& [provider_id, object_ptr] : containers.map_of_provider_object_ptrs)
					{
						containers.map_of_snapshots.insert_or_assign(provider_id, synchro::resync_snapshot<object_type>(*object_ptr));
					}
				});

			}
			void write_readers()
			{
				variadic_util::for_each_iterator<variadic_util::variadic_size_v<object_tuple>>([&]<size_t i>()
				{
					auto& containers = std::get<i>(tuple_of_object_containers);

					//cleanup readers on dead providers:
					for (provider_id_t provider_id : containers.recently_deleted_providers)
					{
						containers.readers_by_provider_id.erase(provider_id);
						containers.next_reader_id_by_provider_id.erase(provider_id);

						for (auto [reader_id, is_alive_ptr] : containers.is_alive_ptrs_by_provider_id[provider_id])
						{
							*is_alive_ptr = false;
						}	
						containers.is_alive_ptrs_by_provider_id.erase(provider_id);

					}
					containers.recently_deleted_providers.clear();

					//write all the readers
					for (auto& [provider_id, map_of_reader_ptrs] : containers.readers_by_provider_id)
					{
						for(auto& [reader_id, object_ptr] : map_of_reader_ptrs)
						{
							*object_ptr = containers.map_of_snapshots.at(provider_id); 
						}
					}
				});
			}
	};
	template<typename object_t, typename instance_t>
	struct synced_object
	{
		object_t owned_object;
		//TODO why was this here? it appears to be useless
		bool alive;

		size_t id_to_sync_to;
		size_t id;

		instance_t* instance;

		synced_object(instance_t& _instance, size_t id_to_sync_to) :
			id_to_sync_to(id_to_sync_to),
			id(_instance.template register_data_reader_object(id_to_sync_to, owned_object, alive)),
			instance(&_instance)
		{
		}
		~synced_object()
		{
			instance->template remove_data_reader_object<object_t>(id_to_sync_to,id);
		}

		object_t& operator*() noexcept
		{
			return owned_object;
		}
		object_t* operator->() noexcept
		{
			return &owned_object;
		}

		bool expired()
		{
			return !alive;
		}
	};
}
