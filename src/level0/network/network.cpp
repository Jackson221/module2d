//Copyright 2017-2019 Jackson Reed McNeill. Licensed under the GNU AGPL version 3 or any later version.
#include "network.hpp"

#include "test/test.hpp"



#include <string>
#include <stdexcept>
#include <chrono>

#include <cerrno>

namespace network
{

#ifdef PLATFORM_WINDOWS
	//call initialization functions for winsock
	//since there is no UNIX equivalent, there is no way to report errors if startup fails
	class compat_start
	{
		public:
			compat_start()
			{    
				WSADATA wsa_data;
				if(WSAStartup(0x202, &wsa_data) != 0)
				{
					printf("[WARNING] Could not initialize winsock. Networking will be unavailable.\n");
				}
				printf("Initialized Winsock\n");
			}
			~compat_start()
			{
				WSACleanup();
			}
	} _compat_start;
#endif

	int _to_berkeley_format(type in)
	{
		switch(in)
		{
			case type::IPv6:
				return AF_INET6;
				break;
			case type::IPv4:
				return AF_INET;
				break;
			case type::UNIX:
				return AF_UNIX;
				break;
			case type::client_view_of_server:
				return AF_UNSPEC;
				break;
			default:
				throw std::runtime_error("socket type not supported");
		}
	}


	std::tuple<unsigned short, network::socket<type::IPv6>> get_socket_at_port_starting_at(unsigned short port)
	{
		for (unsigned short this_port = port; this_port < std::numeric_limits<unsigned short>::max(); this_port++)
		{
			try
			{
				auto sock = network::socket<type::IPv6>(this_port);
				sock.bind_as_server();
				return {this_port, std::move(sock)};
			}
			catch (std::exception const & e)
			{

			}
		}
		throw std::runtime_error("Couldn't assign a port.");
	}

	constexpr char port_inform_str[] = "Running on port %d%";
	void unit_test(test::instance& inst)
	{
		using namespace std::string_literals;
		inst.set_name("Network");

		auto [port, server_sock] = get_socket_at_port_starting_at(5000);
		inst.inform<port_inform_str>(std::to_string(port));

		auto future_servers_view_of_client_sock = server_sock.listen_for_client_async();
		auto client_sock = network::socket<type::client_view_of_server>(port);

		std::chrono::steady_clock::time_point start_time = std::chrono::steady_clock::now();
		std::chrono::steady_clock::duration try_time = std::chrono::seconds(5);
		while(true)
		{
			if(std::chrono::steady_clock::now() - start_time > try_time)
			{
				printf("fail\n");
				throw std::runtime_error(std::string("Couldn't connect in set time limit of "));//+std::to_string(try_time.count())+std::string(" seconds"));
			}
			try
			{
				client_sock.connect_to_server("localhost");
				break;
			}
			catch (std::exception const & e)
			{
				puts(e.what());
			}
		}
		future_servers_view_of_client_sock.wait();
		network::socket servers_view_of_client_sock = std::move(future_servers_view_of_client_sock.get());
		{
			std::string msg="test \0 data"s;
			servers_view_of_client_sock.write(msg);

			auto back_msg = client_sock.read(msg.size());

			inst.inform("Ensure TCP server->client works");
			inst.test(TEST_2(msg,back_msg,msg==back_msg,true));
		}
		{
			std::string msg = "test \0 data"s;
			client_sock.write(msg);

			auto back_msg = servers_view_of_client_sock.read(msg.size());

			inst.inform("Ensure TCP client->server works");
			inst.test(TEST_2(msg,back_msg,msg==back_msg,true));
		}
	}
}
