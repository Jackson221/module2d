/*!
 * @file
 * @author Jackson McNeill
 *
 * A small network abstraction layer, using UNIX sockets.
 *
 * I'm not very happy with this file, I think it consists mostly of failed experiments.
 *
 * Copyright 2017-2019 Jackson Reed McNeill. Licensed under the GNU AGPL version 3 or any later version.
 */
#pragma once 

#include <string>
#include <memory>
#include <future> 
#include <cstring>   
#include <exception>
#include <stdexcept>

//inline int platform_get_errno();
//inline std::string platform_get_strerror(int);

#ifdef PLATFORM_WINDOWS

#define WIN32_LEAN_AND_MEAN
#include <winsock2.h>
#include <ws2tcpip.h>
#include <windows.h>
#define CLOSE(a) closesocket(a)
#define MSG_DONTWAIT 0 //TODO
#define MSG_NOSIGNAL 0
#define SHUT_RDWR 0

#undef interface

#define recv(a,b,c,d) recv(a, reinterpret_cast<char*>(b),c,d)


inline int platform_get_errno()
{
	return WSAGetLastError();
}
inline std::string platform_get_strerror(int error)
{
	//Based on https://stackoverflow.com/a/46104456
	char msgbuf [2048];
	msgbuf [0] = '\0';

	FormatMessage
	(
		FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,   // flags
		NULL,                // lpsource
		error,                 // message id
		MAKELANGID (LANG_NEUTRAL, SUBLANG_DEFAULT),    // languageid
		msgbuf,              // output buffer
		sizeof (msgbuf),     // size of msgbuf, bytes
		NULL               // va_list of arguments
	);
	return std::string(msgbuf);
}
#else
#include <netinet/in.h> 
#include <unistd.h>  
#include <netdb.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/tcp.h>
#define CLOSE(a) ::close(a)


inline int platform_get_errno()
{
	return errno;
}
inline std::string platform_get_strerror(int error)
{
	return std::string(strerror(error));
}
#endif //PLATFORM_WINDOWS

#include "test/forward.hpp"
#include "variadic_util/variadic_util.hpp"



namespace network
{
	class connection_refused : public std::exception
	{
		inline const char* what() const noexcept override
		{
			return "Connection refused";
		}
	};
	class connection_reset : public std::exception
	{
		inline const char* what() const noexcept override
		{
			return "Connection reset";
		}
	};
	class bad_file_descriptor : public std::exception
	{
		inline const char* what() const noexcept override
		{
			return "Bad file descriptor (potentially trying to read on a closed socket)";
		}
	};
	class broken_pipe : public std::exception
	{
		inline const char* what() const noexcept override
		{
			return "Broken pipe (other machine probably disconnected)";
		}
	};

	enum class type
	{
		IPv6,
		IPv4,
		UNIX,
		client_view_of_server,
		server_view_of_client,
		SIZE
	};
	int _to_berkeley_format(type in);

	class socket_interface
	{
		protected:
			int socketfd = -1;
		public:
			std::string read(ssize_t data_to_read)
			{
				std::string buffer;
				buffer.resize(data_to_read);
				ssize_t read_so_far = 0;
				while(read_so_far != data_to_read)
				{
					ssize_t data_read_this_time = recv(socketfd, reinterpret_cast<void*>(buffer.data()+read_so_far), data_to_read-read_so_far,0);
					if (data_read_this_time == -1)
					{
						if (platform_get_errno() == ECONNRESET)
						{
							throw connection_reset();
						}
						else if (platform_get_errno() == EBADF)
						{
							throw bad_file_descriptor();
						}
						else
						{
							throw std::runtime_error("Socket read unknown error "+platform_get_strerror(platform_get_errno()));
						}
					}
					if (data_read_this_time != data_to_read)
					{
						printf("TRY READ %lu BUT GOT %ld\n",data_to_read,data_read_this_time);
					}
					read_so_far += data_read_this_time;
				}
				return buffer;
			}
			void read_nonblocking(std::string & buffer)
			{
				ssize_t data_read_this_time = recv(socketfd, reinterpret_cast<void*>(buffer.data()+buffer.size()), buffer.capacity() - buffer.size(),MSG_DONTWAIT);
				if (data_read_this_time == -1)
				{
					if (platform_get_errno() == ECONNRESET)
					{
						throw connection_reset();
					}
					else if (platform_get_errno() == EBADF)
					{
						throw bad_file_descriptor();
					}
					else if(platform_get_errno() == EAGAIN)
					{
					}
					else
					{
						throw std::runtime_error("Socket read unknown error "+platform_get_strerror(platform_get_errno()));
					}
				}
			}
			void write(std::string data)
			{
				ssize_t data_wrote = send(socketfd,data.c_str(),data.size(),MSG_NOSIGNAL);
				if (data_wrote == -1)
				{
					if (platform_get_errno() == ECONNREFUSED)
					{
						throw connection_refused();
					}
					else if (platform_get_errno() == EBADF)
					{
						throw bad_file_descriptor();
					}
					else if (platform_get_errno() == EPIPE)
					{
						throw broken_pipe();
					}
					else
					{
						throw std::runtime_error("Socket write unknown error "+platform_get_strerror(platform_get_errno()));
					}
				}
				if (data_wrote != static_cast<ssize_t>(data.size()))
				{
					printf("FAILED TO WRITE ALL OF DATA TRIED TO WRITE %lu WROTE %ld\n",data.size(),data_wrote);
				}
			}
	};

	template<type network_type>
	class socket : public socket_interface
	{
		friend class socket<type::IPv4>;
		friend class socket<type::IPv6>;
		friend class socket<type::UNIX>;
		struct noaddr_t {};
		using addr_t = std::conditional_t
		<
			/*if*/ network_type == type::IPv6,
			/*then*/ sockaddr_in6,
			/*elseif*/ std::conditional_t<network_type == type::IPv4,
			/*then*/ sockaddr_in,
			/*else then*/ noaddr_t>
		>;
		addr_t addr;

		unsigned short port;

		void _create_socket(int _type)
		{
			socketfd = ::socket(_type, SOCK_STREAM, IPPROTO_TCP); //A UNIX call, NOT OUR CUSTOM SOCKET CLASS.
			printf("socketfd %d\n",socketfd);

			if (socketfd < 0 )
			{
				throw std::runtime_error("couldn't open socket, error code : "+ std::to_string(platform_get_errno())+ "(" +  platform_get_strerror(platform_get_errno())+")");
			}
			
			{
				//set tcp to no delay https://stackoverflow.com/a/17843292
				int flag = 1;
				int result = ::setsockopt
				(
					socketfd,            /* socket affected */
					IPPROTO_TCP,     /* set option at TCP level */
					TCP_NODELAY,     /* name of option */
					reinterpret_cast<char*>(&flag),  /* the cast is historical cruft */
					sizeof(int)    /* length of option value */
				);
				if (result < 0)
				{
					throw std::runtime_error("Couldn't set tcp no delay");
				}
			}
#ifdef PLATFORM_WINDOWS
			{
				u_long iMode=0;
				int result = ioctlsocket(socketfd,FIONBIO,&iMode);
				if (result != 0)
				{
					throw std::runtime_error("Failed to set blocking mode on socket "+platform_get_strerror(platform_get_errno()));
				}
			}
#endif
		}

		public:
			socket(unsigned short port) :
				port(port)
			{
			}

			inline socket(socket&& other) :
				addr(std::move(other.addr)),
				port(std::move(other.port))
			{
				socketfd = std::exchange(other.socketfd,-1);
			}
			~socket()
			{
				if (socketfd != -1)
				{
					if (shutdown(socketfd, SHUT_RDWR) < 0)
					{
						if (platform_get_errno() != ENOTCONN && platform_get_errno() != EINVAL)
						{
							printf("Note: Trying to close a socket that has already been invalidated. Ignoring.\n");
						}
					}
					CLOSE(socketfd);
				}
			}
			inline socket& operator=(socket&& other)
			{
				socketfd = (std::exchange(other.socketfd,-1));
				addr = (std::move(other.addr));
				port = std::move(other.port);
				return *this;
			}

			socket(socket const & other) = delete;
			socket& operator=(socket const & other) = delete;

			void bind_as_server()
			{
				_create_socket(_to_berkeley_format(network_type));
				int bindError;
				if constexpr(network_type == type::IPv6)
				{
					std::memset(&(addr),0,sizeof(addr));
					addr.sin6_flowinfo = 0;
					addr.sin6_family = _to_berkeley_format(network_type);
					addr.sin6_addr = in6addr_any;
					addr.sin6_port = htons(port);
					bindError = ::bind(socketfd, reinterpret_cast<sockaddr *>( &addr), sizeof(addr));
				}
				else if (network_type == type::IPv4)
				{
					std::memset(&(addr),0,sizeof(addr));
					addr.sin_family = _to_berkeley_format(network_type);
					addr.sin_addr.s_addr = INADDR_ANY;
					addr.sin_port = htons(port);
					bindError = ::bind(socketfd, reinterpret_cast<sockaddr *>( &addr), sizeof(addr));
				}
				else
				{
					throw std::runtime_error("socket type not supported");
				}
				if (bindError < 0)
				{
					throw std::runtime_error("failed to bind as server error code : " +  platform_get_strerror(platform_get_errno()));
				}
			}
			void connect_to_server(std::string hostname)
			{
				printf("hn %s\n",hostname.c_str());

				::addrinfo* info_orig;
				::addrinfo hints = {};
				std::memset(&hints, 0, sizeof(hints));
				hints.ai_family = _to_berkeley_format(network_type);
				hints.ai_socktype = SOCK_STREAM;
				hints.ai_protocol = IPPROTO_TCP;

				auto base10_port_format = std::to_string(port);
				::getaddrinfo(hostname.c_str(),base10_port_format.c_str(),&hints,&info_orig);

				int connectError;
				bool got_connection = false;
				for (::addrinfo* info = info_orig; info != nullptr; info = info->ai_next)
				{
					char human_hostname[1025];                         
					//TODO: handle errors
					/*int error = */getnameinfo(info->ai_addr, info->ai_addrlen, human_hostname, sizeof(human_hostname), NULL, 0, 0);
					printf("name %s\n",human_hostname);
					printf("AF_INET %d | SOCK_STREAM %d | IPPROTO_TCP %d\n", info->ai_family == hints.ai_family, info->ai_socktype == hints.ai_socktype, info->ai_protocol == hints.ai_protocol);

					_create_socket(info->ai_family);

					connectError = ::connect(socketfd,info->ai_addr, info->ai_addrlen);// reinterpret_cast<::sockaddr*>(&addr), sizeof(addr));
					if (connectError >= 0)
					{
						printf("choose a %s connection\n",info->ai_family == AF_INET6? "IPv6" : "IPv4/other");
						got_connection = true;
						break;
					}
					//failed
					CLOSE(socketfd);
					printf("connect error %d, %s| %s\n",connectError, strerror(connectError),strerror(platform_get_errno()));
				}
				
				::freeaddrinfo(info_orig);

				if (!got_connection)
				{
					/*if (connectError < 0 )
					{
						printf("connect err %d %d\n", connectError, platform_get_errno());
						printf(strerror(connectError));
						throw std::runtime_error("failed to connect to server error code: " +  std::string(strerror(platform_get_errno())));
					}*/
					throw std::runtime_error("failed to connect to server");
				}
			}

			/*!
			 * Connects (listens for) a client to your server. Blocks. 
			 *
			 * Returns a socket that is for the client; i.e. calling write on it will write to the client connected
			 */
			socket<type::server_view_of_client> listen_for_client()
			{
				int listensucc = ::listen(socketfd,1);
				if (listensucc < 0)
				{
					throw std::runtime_error(platform_get_strerror(platform_get_errno()));
				}



				socket<type::server_view_of_client>&& client(port); //our custom socket object, NOT A UNIX CALL.
				::socklen_t size = sizeof(client.addr);
				client.socketfd = ::accept(socketfd,reinterpret_cast<::sockaddr*>(&client.addr), &size); 
				if (socketfd == -1 )
				{
					throw std::runtime_error(platform_get_strerror(platform_get_errno()));
				}
				return std::move(client);
			}

			std::future<socket<type::server_view_of_client>> listen_for_client_async()
			{
				return std::async(std::launch::async, [this](){return std::move(this->listen_for_client());});
			}
	};

	/*!
	 * Manages a tuple of server sockets (probably of different network families like IPv6 and IPv4)
	 */
	template<typename socket_tuple_t>
	class multi_protocol_stack_server_manager
	{
		using _client_future = std::future<socket<type::server_view_of_client>>;
		template<typename>
		using _future_of_client_sock_template = _client_future;

		socket_tuple_t server_socks;
		//make a tuple of client_futures using the template above; doesn't actually consider what type the socket is.
		variadic_util::make_variadic_of_template_applied_to_variadic_contents_t<_future_of_client_sock_template,socket_tuple_t> new_clients;

		template<size_t originating_server>
		void _fill_new_client_future(std::future<socket<type::server_view_of_client>> & future)
		{
			future = std::get<originating_server>(server_socks).listen_for_client_async();
		}
		template<typename callable_t>
		struct _each_element_in_tuple_fn
		{
			multi_protocol_stack_server_manager & host;
			callable_t & function;

			template<size_t originating_server>
			void operator()(_client_future& future)
			{
				if (future.wait_for(std::chrono::milliseconds(0)) == std::future_status::ready)
				{
					function(future.get());
					host._fill_new_client_future<originating_server>(future);
				}
			}
		};
		public:
			template<typename callable_t>
			inline void process_all_new_clients(callable_t function)
			{
				auto fn = _each_element_in_tuple_fn<callable_t>(*this,function);
				variadic_util::for_each_element_in_variadic(new_clients, fn);
			}
	};
	void unit_test(test::instance& inst);
}
