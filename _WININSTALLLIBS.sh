#!/bin/bash

orig=$PWD/../build_windows

mkdir ${orig}

echo "NOTE: Requires Wine to install Vulkan. If you have already installed Vulkan in your local Wine, please uninstall it and re-start this script."

mkdir windows


####SDL
cd /tmp
rm -rf SDLTMP
mkdir SDLTMP 
cd SDLTMP

wget https://www.libsdl.org/release/SDL2-devel-2.0.22-mingw.tar.gz

tar -xvf SDL2-devel*

cd SDL2-*/

cd x86_64-w64-mingw32/

sudo cp ./* /usr/x86_64-w64-mingw32/ -rf --verbose

cp bin/* ${orig} --verbose



###SDL_IMAGE

cd /tmp
rm -rf IMGTMP
mkdir IMGTMP
cd IMGTMP

wget https://www.libsdl.org/projects/SDL_image/release/SDL2_image-devel-2.0.5-mingw.tar.gz

tar -xvf SDL2_image-devel-*

cd SDL2_image-*/

cd x86_64-w64-mingw32/

sudo cp ./* /usr/x86_64-w64-mingw32/ -rf --verbose

cp bin/* ${orig} --verbose


####SDL_TTF

cd /tmp
rm -rf TTFTMP
mkdir TTFTMP
cd TTFTMP/

wget https://www.libsdl.org/projects/SDL_ttf/release/SDL2_ttf-devel-2.0.18-mingw.tar.gz

tar -xvf SDL2_ttf-devel-*

cd SDL2_ttf-*/

cd x86_64-w64-mingw32/

sudo cp ./* /usr/x86_64-w64-mingw32/ -rf --verbose

cp bin/* ${orig} --verbose

####VULKAN INSTALL

cd /tmp/
rm -rf VKTMP
mkdir VKTMP
cd VKTMP

wget https://sdk.lunarg.com/sdk/download/1.3.211.0/windows/VulkanSDK-1.3.211.0-Installer.exe?Human=true;u=

mv Vulkan* vulkan.exe

echo "NOTE: If you have previously installed the Vulkan SDK on your Wine install, you will need to un-install it and re-run this script."

echo "If the installer window appears, mind the jank and install to: Z:\tmp\VKTMP"

wine vulkan.exe /NCRC /S /D=Z:\\tmp\\VKTMP\\

sudo cp Lib/vulkan-1.lib /usr/x86_64-w64-mingw32/lib/libvulkan.a

sudo cp Include/vulkan/ /usr/x86_64-w64-mingw32/include/ -rf

echo "Uninstalling, to prevent pollution of your Wine."

wine Uninstall.exe /S


####END

#The following files are already provided by the host OS, and are valid to use.

#(GLM is header-only and platform independant)
sudo ln -s /usr/include/glm/ /usr/x86_64-w64-mingw32/include/glm


cp /usr/x86_64-w64-mingw32/lib/libwinpthread-1.dll ${orig}

echo Done.

